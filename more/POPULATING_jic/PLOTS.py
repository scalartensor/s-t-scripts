#!/usr/bin/env python -u
import numpy as np
import pylab as py
import os,sys
from time import sleep

#Input data.
flight=True #"True" to plot on the flight as points are created. "False" to plot a finalised file.

#Import data.
FEA_file1='../../../data/FEA12_alpha0beta0_1141.txt'
FEA_file2='../../../data/FEA12_alpha0beta0_1738.txt'
ifile='../../../data/ANALYSIS/alpha0beta0_consistent.txt'
py.ion()
while True:
	if flight:
		data=np.genfromtxt(ifile, delimiter=',', skip_footer=1)
	else:
		data=np.loadtxt(ifile, delimiter=',')
	if len(data)==0:
		sys.stdout.write('No overlap found yet. \r')
		continue

	FEA1=np.loadtxt(FEA_file1)
	FEA2=np.loadtxt(FEA_file2)
	alpha0=data[:,0]
	beta0=data[:,1]
	perc=data[:,2]

	#Plot.
	py.clf()
	ymin,ymax=1e-4, 1.
	xmin,xmax=-6., 6.
	py.plot(FEA1[:,0], FEA1[:,1], color='purple', linewidth=4)
	py.plot(FEA2[:,0], FEA2[:,1], color='blue', linewidth=4)
	selecti=(perc<5) #Select the ones where alphaA and alphaA_check coincide within 5 percent.
	#Find envelope.
	beta0_env=np.unique(beta0[selecti])
	alpha0_env=np.zeros(len(beta0_env))
	beta0_env_all=np.unique(beta0)
	alpha0_env_all=np.zeros(len(beta0_env_all))
	for i in xrange(len(beta0_env)):
		alpha0_env[i]=max(abs(alpha0[selecti][beta0[selecti]==beta0_env[i]]))
	for i in xrange(len(beta0_env_all)):
		alpha0_env_all[i]=max(abs(alpha0[beta0==beta0_env_all[i]]))

	py.plot(beta0, abs(alpha0), '.', color='red', alpha=0.1)
	py.plot(beta0[selecti], abs(alpha0[selecti]), '.', color='green', alpha=0.1)
	py.plot(beta0_env_all, abs(alpha0_env_all), color='red', linewidth=4)
	py.plot(beta0_env, abs(alpha0_env), color='green', linewidth=4)
	py.axvline(0, ymin, ymax, color='black')
	py.axhline(3.2e-3, xmin, xmax, color='black')
	py.yscale('log')
	py.xlim(xmin, xmax)
	py.ylim(ymin, ymax)
	py.ylabel('|alpha0|')
	py.xlabel('beta0')
	py.draw()
	if flight:
		sleep(5)
	else:
		raw_input('enter')
