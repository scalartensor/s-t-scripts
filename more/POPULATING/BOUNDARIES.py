#!/usr/bin/env python
import numpy as np
import pylab as py
import os
from scipy import interpolate as ip
from scipy.interpolate import LinearNDInterpolator as LNDI
from time import sleep
import COMMON as CM
from scipy.ndimage import gaussian_filter

#DESCRIPTION.
#This script finds what are the best initial conditions to cover the interesting output parameter space.
#(A) The central pressure must be in a range that the NS baryonic mass is always within a specific range, for all values of beta0.
#(B) The central scalar field must be in a range such that |alpha0| is always within a specific range, for all values of beta0.

#INPUT PARAMETERS:
abs_alpha0_max=1. #Maximum allowed |alpha0|.
abs_alpha0_min=1e-4 #Minimum allowed |alpha0|.
mbA_min=0.5 #Minimum allowed baryonic mass.
mbA_max=3.5 #Maximum allowed baryonic mass.
#beta0vec=np.linspace(-6.,6.,20)
beta0vec=np.array([-6.,-5.,-4.,-3.,-2.,-1.,0.,1.,2.,3.,4.,5.,6.]) #All values of beta0 considered.
#beta0vec=np.array([-3.])

#Fixed parameters for the Mathematica calculation.
sp = 1e34 #Typical maximum pressure at the centre of the NS in Pa (the parameters pNminT and pNmaxT multiplied by this will give the range of central pressure considered).
sM = 1e4 #Typical radius of the NS in m.
pNsurT = 1e-15 #Pressure at the surface of the NS (in theory zero).
rhoNcenT = 1e-2 #Radial coordinate at the centre. In theory should be zero, but given that at rho=0 there is a singularity, it should be a small number compared to 1. If it is smaller than 1e-4, singularities start to appear in the interesting output space.
#Variable parameters for the Mathematica calculation.
pbins = 40 #Number of bins in values of central pressure (log spaced).
phibins = 40 #Number of bins in values of central scalar field (log spaced).
plotdir='./plots/'
inputfile='input.txt' #Input file for the Mathematica script.
outputfile='output.txt' #Output file for the Mathematica script.
pNminT = 1e-3 #Minimum pressure at the centre of the NS in rescaled units (so the real value is that multiplied by sp).
pNmaxT = 100 #Maximum pressure at the centre of the NS in rescaled units (so the real value is that multiplied by sp).
phiNminT = 5e-5 #Minimum value of the scalar field at the centre of the NS.
phiNmaxT = 1 #Maximum value of the scalar field at the centre of the NS.

#Other parameters specific for this script.
mbAbins=20 #Number of points in the vector of baryonic mass.
phi0_bins=50 #Number of bins in phi0 to search for the maximum baryonic mass.
PLOTTING=False #True to show plots as they are obtained. Otherwise they will just be saved.
SAVING=True #True to save plots.

##########################################
#MAIN CALCULATION.
#Create folder for plots.
if SAVING:
	filenum=[]
	for fili in os.listdir(plotdir): filenum.append(int(fili[1:]) if fili[0]=='B' else 0)
	odir=plotdir+'B%i' %(max(filenum)+1)
	os.makedirs(odir)

mbAvec=np.linspace(mbA_min, mbA_max, mbAbins) #Vector of baryonic mass to see lines in the plots.
if PLOTTING:
	py.ion()
for beta0i in xrange(len(beta0vec)):
	beta0=beta0vec[beta0i]
	print 'Beta0=%.3f' %beta0
	tempdir='./Eq_solver/TEMP/'
	
	CM.input_par_math_file(sp=sp, sM=sM, pNsurT=pNsurT, rhoNcenT=rhoNcenT, beta0=beta0)

	#Initial conditions.
	pNcenT_vec=np.logspace(np.log10(pNminT), np.log10(pNmaxT), pbins)
	phiNcenT_vec=np.logspace(np.log10(phiNminT), np.log10(phiNmaxT), phibins)
	P_vec, Phi_vec=np.meshgrid(pNcenT_vec, phiNcenT_vec)
	input=np.vstack((P_vec.flatten(),Phi_vec.flatten())).T
	#Create input file with initial conditions.
	np.savetxt(tempdir+'input.txt', input, delimiter=',', newline='\n', header='pNcenT, phiNcenT')

	#Solve equations.
	print 'Mathematica running...'
	CM.eq_solver(inputfile, outputfile)
	
	#Load solutions.
	raw_data=np.loadtxt(tempdir+'output.txt', dtype='str')
	c0=(raw_data[:,8]!='Indeterminate') #This condition will eliminate "Indeterminate" solutions.
	#Convert data to floats (first get rid of "Indeterminate" rows).
	raw_data[raw_data=='Indeterminate']='999999'
	sols=raw_data.astype(float)
	pcen=sols[:,0]*sp #Pressure at the centre of the NS in Pa.
	phicen=sols[:,1] #Value of the scalar field at the centre of the NS.
	Rns=sols[:,2] #Radius of the NS in m.
	alphaA=sols[:,3] #Effective coupling (dimensionless).
	phi0=sols[:,4] #Scalar field at infinity (dimensionless).
	mA=sols[:,5] #Mass in solar masses.
	mbA=sols[:,6] #Mass (baryonic) in solar masses.
	IA=sols[:,7] #Moment of inertia in kg m^2.
	alpha0=sols[:,8] #Matter coupling in spatial infinity (dimensionless).

	#Physical conditions.
	c1=(phicen*phi0>=0) #As explained in DamourEspositoFarese1998 (referring to DamourEspositoFarese1996), configurations where that product is negative are energetically disfavored.
	c2=(Rns>0.)&(mA>0.)&(mbA>0.) #NS radius must be positive.
	#c3=(abs(alpha0)<1) #|alpha0| cannot be larger than 1.
	sel=c0&c1&c2

	#Plot of input parameters with stripes of constant baryonic mass.
	py.figure(1)
	py.clf()
	plottitle=(str('%.3f' %beta0)).replace('.','p')
	#py.loglog(pcen, phicen,'.', color='blue', alpha=0.5, label='All')
	py.loglog(pcen[-c0], phicen[-c0], 's', color='red', alpha=0.5, label='Singularities')
	py.loglog(pcen[-c1], phicen[-c1], '^', color='magenta', alpha=0.5, label='Energetically disfavoured')
	py.loglog(pcen[-c2], phicen[-c2], '^', color='yellow', alpha=0.5, label='Negative mass or radius')
	#py.loglog(pcen[-c3], phicen[-c3], '*', color='brown', alpha=0.5, label='|alpha0|>1')
	py.loglog(pcen[sel], phicen[sel], '.', color='green', alpha=1, label='Good solutions')
	for mbAi in xrange(len(mbAvec)):
		mbAsel=(abs(mbA-mbAvec[mbAi])<0.01)
		py.loglog(pcen[mbAsel], phicen[mbAsel], 'o', alpha=0.8)
	py.title('Beta0=%.3f' %beta0)
	py.legend(loc='lower right')
	py.xlabel('Pressure at the centre of the NS/Pa')
	py.ylabel('Scalar field at the centre of the NS')
	if SAVING:
		py.savefig('%s/inputs_%i_beta0_%s.png' %(odir,beta0i,plottitle))

	#Plot of |alpha0| versus mbA.
	py.figure(2)
	py.clf()
	py.plot(mbA, abs(alpha0),'.', color='blue', label='All')
	py.plot(mbA[sel], abs(alpha0[sel]),'.', color='green', label='Good solutions')
	py.hlines(abs_alpha0_max, mbA_min, mbA_max, color='black')
	py.hlines(abs_alpha0_min, mbA_min, mbA_max, color='black')
	py.vlines(mbA_min, abs_alpha0_min, abs_alpha0_max, color='black')
	py.vlines(mbA_max, abs_alpha0_min, abs_alpha0_max, color='black')
	py.yscale('log')
	py.legend(loc='lower right')
	py.xlim(0.,5.)
	py.ylim(1e-5, 10)
	py.xlabel('Baryonic mass / msun')
	py.ylabel('|alpha0|')
	if SAVING:
		py.savefig('%s/absalpha0_vs_mbA_%i_beta0_%s.png' %(odir,beta0i,plottitle))

	#Plot of central pressure versus mbA.
	py.figure(3)
	py.clf()
	py.plot(mbA[sel], pcen[sel],'.', alpha=0.2)
	sel2=(abs(alpha0[sel])>1e-4)&(abs(alpha0[sel])<1)
	phi0_good=phi0[sel][sel2]
	phi0vec=np.logspace(np.log10(min(phi0_good)), np.log10(max(phi0_good)), phi0_bins)
	for phi0i in xrange(len(phi0vec)):
		phi0sel=(abs(phi0-phi0vec[phi0i])<0.01)
		py.loglog(mbA[phi0sel], pcen[phi0sel], 'o', alpha=0.2, markersize=4)
		if len(mbA[phi0sel])>0:
			indimax=mbA[phi0sel].argmax()
			py.loglog(mbA[phi0sel][indimax], pcen[phi0sel][indimax], '^', markersize=10, color='black')
	py.xlabel('Baryonic mass / msun')
	py.ylabel('Central pressure / Pa')
	py.yscale('log')
	py.xlim(0.,5.)
	if SAVING:
		py.savefig('%s/pcen_vs_mbA_%i_beta0_%s.png' %(odir,beta0i,plottitle))

	#Plot of central pressure versus mass.
	py.figure(4)
	py.clf()
	py.plot(mA[sel], pcen[sel],'.', alpha=0.2)
	for phi0i in xrange(len(phi0vec)):
		phi0sel=(abs(phi0-phi0vec[phi0i])<0.01)
		py.loglog(mA[phi0sel], pcen[phi0sel], 'o', alpha=0.2, markersize=4)
		if len(mA[phi0sel])>0:
			indimax=mA[phi0sel].argmax()
			py.loglog(mA[phi0sel][indimax], pcen[phi0sel][indimax], '^', markersize=10, color='black')
	py.xlabel('Mass / msun')
	py.ylabel('Central pressure / Pa')
	py.yscale('log')
	py.xlim(0.,5.)
	if SAVING:
		py.savefig('%s/pcen_vs_mA_%i_beta0_%s.png' %(odir,beta0i,plottitle))

	if PLOTTING:
		raw_input('enter')
