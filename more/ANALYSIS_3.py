#!/usr/bin/python
##############
#This script should:
#(1) Load the data produced by mathematica and apply some physical checks on it.
#(2) Export some plots to see that things are as expected.
##############

import numpy as np
import pylab as py
import os

inputdir='../../data/ANALYSIS/'
solnum='all' #Number of solutions to import ('all' to select all together).
outputplotdir='../../plots/' #Output directory for plots.

#Define physical conditions.
absalphaA_max=1. #Maximum value of |alphaA|.
Rns_min=1000. #Minimum value of the radius of the NS in m.
Rns_max=15000. #Maximum value of the radius of the NS in m.
mbA_max=4. #Maximum value of the baryonic mass of the NS in msun.
mA_max=4. #Maximum value of the mass of the NS in msun.
#Further selection conditions have to be chosen manually from below.

#Define constants.
grav=6.673e-11
light=2.99792458e8
msun=1.9891e30

##############
#Load data (solutions of the equations by Mathematica).
#Fixed parameters.
#AppendTo[params, {sp 1., sM 1., pNsurT 1., \[Rho]NcenT 1., mtb 1.,  nt0 1., \[CapitalGamma] 1., kns 1., \[Beta]0 1.}]
if solnum=='all':
	par_to_load=[]
	sol_to_load=[]
	for fili in os.listdir(inputdir):
		if fili[0:11]=='parameters_':
			par_to_load.append(inputdir+fili)
			sol_to_load.append(inputdir+'solutions_'+fili[11:14]+'.txt')
	for fili in xrange(len(par_to_load)):
		if fili==0:
			params=np.loadtxt(par_to_load[fili])
			sols=np.loadtxt(sol_to_load[fili])
		else:
			new_params=np.loadtxt(par_to_load[fili])
			new_sols=np.loadtxt(sol_to_load[fili])
			if not np.allclose(params,new_params):
				print 'Parameters are not equal! Exiting...'
				exit()
			else:
				sols=np.vstack((sols,new_sols))
else:
	params=np.loadtxt(inputdir+'parameters_'+solnum+'.txt')
	sols=np.loadtxt(inputdir+'solutions_'+solnum+'.txt')

sp=params[0] #Typical maximum pressure at the centre of the NS in Pa.
sM=params[1] #Typical radius of the NS in m.
psur=params[2]*sp #Pressure at the surface of the NS (in theory should be zero, but that leads to singularities).
rhocen=params[3]*sM #Radial coordinate at the centre (in theory should be zero).
mtb=params[4] #Baryonic mass (kg).
nt0=params[5] #Baryon number density (m^-3).
Gamma=params[6] #Dimensionless parameter of the polytrope EoS.
kns=params[7] #Dimensionless constant of the polytrope EoS.
beta0=params[8] #Dimensionless constant : quadratic coupling of the scalar field.
#Variable parameters.
#AppendTo[sols, {pNcenT 1., \[CurlyPhi]NcenT 1., Rns 1., \[Alpha]A 1., \[CurlyPhi]0 1., mA 1./Msun, mbA 1./Msun, IA 1.}]
pcen=sols[:,0]*sp #Pressure at the centre of the NS in Pa.
phicen=sols[:,1] #Value of the scalar field at the centre of the NS.
Rns=sols[:,2] #Radius of the NS in m.
alphaA=sols[:,3] #Effective coupling (dimensionless).
phi0=sols[:,4] #Scalar field at infinity (dimensionless).
mA=sols[:,5] #Mass in solar masses.
mbA=sols[:,6] #Mass (bar) in solar masses.
IA=sols[:,7] #Moment of inertia in kg m^2.
alpha0=sols[:,8]
############################
#Take only physical solutions.
check=(mbA>0)&(mbA<mbA_max)&(mA>0)&(mA<mA_max)&(Rns>Rns_min)&(Rns<Rns_max)&(abs(alphaA)<absalphaA_max)
############################
#Select only those solutions that are consistent with Solar System experiments.
#phi0lim=0.0043 #Limit on phi0 from Solar System experiments (from DEF93).
phi0lim=0.0024 #Limit on phi0 from Solar System experiments (from DEF96).
#sel=(abs(phi0-phi0lim)<phi0lim*0.05) #This selects only one stripe at the maximum value.
sel=check&(phi0<phi0lim) #This selects all values consistent with the limit.
############################
#Select only stable solutions, where mass decreases for increasing pressure.
#sel=check&(pcen<6.8e34) #This pressure is chosen (by eye) at the point where mass starts to decrease for increasing pressure.
############################

#Different plots to check that everything works as expected.
py.ion()
msel=(mbA>0.97)&(mbA<1.) #Select a stripe of NS baryonic mass.
mAsel=mA[msel]
phi0sel=phi0[msel]
alphaAsel=alphaA[msel]

sel2=phi0sel.argsort()
phi0vec=np.linspace(min(phi0sel[sel2]),max(phi0sel[sel2]),20)
logmAvec=np.interp(phi0vec, phi0sel[sel2], np.log(mAsel[sel2]))

alphaAvec=np.interp(phi0vec,phi0sel[sel2], alphaAsel[sel2])
alphaAcalc=np.diff(logmAvec)*1./np.diff(phi0vec)
betaA=np.diff(alphaAvec)*1./np.diff(phi0vec)
#perc=abs((alphaAvec[1:]-alphaAcalc)*1./alphaAvec[1:])*100.
py.plot(phi0vec[1:], -alphaAcalc,'.',color='red',label='Derived')
py.plot(phi0vec[1:], -alphaAvec[1:],'.',color='blue',label='Theoretical')
#py.plot(perc,'.')
py.xlabel('Scalar field at infinity')
py.ylabel('-alphaA')
py.legend(loc='best')
py.savefig(outputplotdir+'alphaA_comparison.png')
raw_input('enter')
#py.plot(betaA)
#raw_input('enter')
#exit()

print 'By looking at the following plot I can choose a better parameter space of p and phi at the centre. There are many points in uninteresting areas!'
print 'I could output points in the accepted region, and use them as inputs for ANALYSIS, to calculate the equations in those areas.'
py.clf()
py.loglog(pcen,phicen,'.',color='green',label='All')
py.loglog(pcen[check],phicen[check],'.',color='blue',label='Physical')
py.loglog(pcen[sel],phicen[sel],'.',color='red',label='Unconstrained')
py.xlabel('Pressure at the centre of the NS/Pa')
py.ylabel('Scalar field at the centre of the NS')
py.legend(loc='upper left')
py.savefig(outputplotdir+'phicen_vs_pcen.png')
raw_input('enter')

#Apply physical conditions to all quantities.
#pcen=pcen[check]
#phicen=phicen[check]
#Rns=Rns[check]
#alphaA=alphaA[check]
#phi0=phi0[check]
#mA=mA[check]
#mbA=mbA[check]
#IA=IA[check]
#alpha0=alpha0[check]

#Some plots.
py.clf()
py.plot(mA[sel],-alphaA[sel],'.')
py.plot(mbA[sel],-alphaA[sel],'.')
py.ylabel('-alphaA')
py.xlabel('mass/msun')
py.xlim(0.,3.)
py.ylim(0.,0.7)
py.savefig(outputplotdir+'alphaA_vs_mass.png')
raw_input('enter')

py.clf()
py.plot(mA[sel],IA[sel]/(msun*(grav*msun/light**2.)**2.),'.')
py.plot(mbA[sel],IA[sel]/(msun*(grav*msun/light**2.)**2.),'.')
py.ylabel('IA/msun(G msun/c2)2')
py.xlabel('mbA/msun')
py.xlim(0.,3.5)
py.ylim(0.,100)
py.savefig(outputplotdir+'momin_vs_mass.png')
raw_input('enter')

py.clf()
py.plot(Rns[sel],mA[sel],'.')
py.plot(Rns[sel],mbA[sel],'.',color='red')
py.ylim(0.,5.)
py.xlim(0.,16000)
py.xlabel('Radius/m')
py.ylabel('Mass/msun')
py.savefig(outputplotdir+'mass_vs_rad.png')
raw_input('enter')

print 'I could get rid of unstable solutions, where the mass decreases for increasing pressure.'
py.clf()
py.plot(pcen[sel],mA[sel],'.')
py.plot(pcen[sel],mbA[sel],'.',color='red')
py.xscale('log')
py.xlabel('Pressure at the centre of the NS/Pa')
py.ylabel('Mass/msun')
py.savefig(outputplotdir+'mass_vs_pcen.png')
raw_input('enter')

py.clf()
py.plot(pcen[sel],abs(alphaA[sel]),'.')
py.xscale('log')
py.yscale('log')
py.xlabel('Pressure at the centre of the NS/Pa')
py.ylabel('abs(alphaA)')
py.savefig(outputplotdir+'alphaA_vs_pcen.png')
raw_input('enter')

