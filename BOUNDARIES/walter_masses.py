import numpy as np
import Eq_solver.DEFAULT_PARAMETERS as DEF

#Define constants.
grav=DEF.Gn
light=DEF.c
msun=DEF.msun
rsun=DEF.rsun
day=24.*3600.
yr=365.*day

#Input data.
walterfile='../../../data/walter/posterior_samples.dat'
#GLF1_1	OM	DM	ECC	DECJ	JUMP1	PMRA	GLPH_1	RAJ	F0_1141-6545	T0	PB	DM1	PMDEC	GAMMA_1141-6545	JUMP2	F1_1141-6545	GLF0_1	PBDOT	OMDOT	A1_1141-6545	logL

d=np.loadtxt(walterfile,skiprows=1,usecols=(3, 11, 14, 18, 19, 20), delimiter='\t') 
ecc=d[:,0] 
pb=d[:,1]*24.*3600. #Orbital period in s.
gamma=d[:,2]
pbdot=d[:,3]
omdot=d[:,4]*np.pi/180.*1./yr
a1=d[:,5]*light

def mtot(pb, ecc, omdot):
	return ((omdot*(1.-ecc**2.)/3.)**(3./2.)*(light**3.*pb**(5./2.)/(grav*(2.*np.pi)**(5./2.))))/msun

mtoti=mtot(pb[0],ecc[0],omdot[0])
a=gamma[0]*(mtoti*msun)**(4./3.)*light**2.*(2.*np.pi)**(1./3.)/(ecc[0]*grav**(2./3.)*pb[0]**(1./3.))

def mb(pb, ecc, gamma, mtot):
	A=gamma*(mtot*msun)**(4./3.)*light**2.*(2.*np.pi)**(1./3.)/(ecc*grav**(2./3.)*pb**(1./3.))
	return (-mtot*msun + np.sqrt((mtot*msun)**2.+4.*A)*0.5)/msun

mtotvec=mtot(pb,ecc,omdot)
mbvec=mb(pb, ecc, gamma, mtotvec)
mavec=mtotvec-mbvec
print mavec
print mbvec
