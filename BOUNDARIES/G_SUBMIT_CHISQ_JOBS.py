#!/usr/local/python-2.7.2/bin/python
import numpy as np
import pylab as py
import os, sys
import COMMON as CM

##############################################
#INPUT PARAMETERS:
run_number=4

##############################################
#run_dir='/lustre/projects/p002_swin/prosado/scalartensor/data/ANALYSIS/BOUNDARIES/RUN%.3i/ANALYSIS/' %int(run_number)
run_dir='/lustre/projects/p002_swin/prosado/scalartensor/data/ANALYSIS/BOUNDARIES/RUN%.3i/ANALYSIS_uni/' %int(run_number)
qsub_dir='/home/prosado/projects/scalartensor/scalar-tensor-scripts/ANALYSIS/BOUNDARIES/G_qsub_CHISQ_RUN%.3i/'  %int(run_number)
#outputdir='/lustre/projects/p002_swin/prosado/scalartensor/data/ANALYSIS/BOUNDARIES/RUN%.3i/CHISQ/' %int(run_number)
#outputdir='/lustre/projects/p002_swin/prosado/scalartensor/data/ANALYSIS/BOUNDARIES/RUN%.3i/CHISQ_poster/' %int(run_number)
outputdir='/lustre/projects/p002_swin/prosado/scalartensor/data/ANALYSIS/BOUNDARIES/RUN%.3i/CHISQ_poster_uni/' %int(run_number)
parfile='/home/prosado/projects/scalartensor/scalar-tensor-scripts/ANALYSIS/BOUNDARIES/poster.par'
#parfile='/lustre/projects/p002_swin/vvenkatr/data/cpsr2/calibrated/J1141-6545/20cm/metm/3min_nonoise.par'

#Create or relpace output directory.
CM.create_or_replace(qsub_dir, cluster=True)
CM.create_or_replace(outputdir, cluster=True)

#Locate files.
inputfiles=np.sort([file for file in os.listdir(run_dir) if file[0:9]=='analysis_'])

#Analyse files.

for job_i in xrange(len(inputfiles)):
	gfilename=qsub_dir+'job_%.3i.gstar' %(job_i+1)
	analysis_filename=run_dir+inputfiles[job_i]

	#Content of the gstar file:
	filetext=['#!/bin/csh \n\
#PBS -q sstar \n\
#PBS -l nodes=1:ppn=6 \n\
#PBS -l pmem=500mb \n\
#PBS -l walltime=10:00:00 \n\
#PBS -N %sO_job_%s \n\
#PBS -o %sO_output_%s \n\
#PBS -e %sO_error_%s \n\
\n\
echo Deploying job to CPUs ... \n\
cat $PBS_NODEFILE \n\
\n\
python /home/prosado/projects/scalartensor/scalar-tensor-scripts/ANALYSIS/BOUNDARIES/G_CHISQ.py %s %s %s' %(qsub_dir, job_i+1, qsub_dir, job_i+1, qsub_dir, job_i+1, analysis_filename, outputdir, parfile)]

	np.savetxt(gfilename, filetext, fmt='%s', newline='\n')

	task='qsub '+gfilename
	print task
	os.system(task)
