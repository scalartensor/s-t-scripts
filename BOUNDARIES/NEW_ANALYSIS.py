#!/usr/bin/env python -u
import numpy as np
import pylab as py
import os, sys
import Eq_solver.DEFAULT_PARAMETERS as DEF
from scipy import interpolate as ip
from scipy.interpolate import LinearNDInterpolator as LNDI
from time import sleep
from USEFUL import time_estimate
import COMMON as CM

#INPUT PARAMETERS:
inputdir=sys.argv[1]
outputdir=sys.argv[2]
#inputdir='../../../data/ANALYSIS/BOUNDARIES/RUN3/COMBINED/'
#outputdir='../../../data/ANALYSIS/BOUNDARIES/RUN3/OLD_ANALYSIS/'

#run_number=2 #Number of the current run, from which data will be taken.

#Physical conditions.
mbA_min=0.5 #Minimum value of NS baryonic mass (in msun).
mbA_max=3.5 #Maximum value of NS baryonic mass (in msun).
mA_min=0. #Minimum value of NS mass (in msun).
mA_max=3.5 #Maximum value of NS mass (in msun).
Rns_min=1000. #Minimum value of NS radius (in m).

#Characteristics of output vectors.
mbA_bins=1000 #Number of points in the vector of baryonic mass.
abs_alpha0_bins=1000 #Number of points to output in |alpha0| and all other quantities.

mbA_low=1.
mbA_upp=3.
abs_alpha0_min=1e-4 #Minimum value of |alpha0|.
abs_alpha0_max=1 #Maximum value of |alpha0|.

###########################
#Create or relpace output directory.
CM.create_or_replace(outputdir)

##############
#Load data.
inputfiles=np.sort([file for file in os.listdir(inputdir) if file[0:5]=='comb_'])

t=time_estimate(len(inputfiles)) #A class that prints estimated computation time.

for fili in xrange(len(inputfiles)):
	t.display() #Shows the remaining computation time.
	t.increase() #Needed to calculate the remaining computation time.

	ifile=inputdir+inputfiles[fili]
	data=np.load(ifile)[()]

	beta0, sp, sM, mtb, nt0, Gamma, kns, o_data=CM.new_analysis(data, mbA_min=mbA_min, mbA_max=mbA_max, mA_min=mA_min, mA_max=mA_max, Rns_min=Rns_min, abs_alpha0_min=abs_alpha0_min, abs_alpha0_max=abs_alpha0_max, mbA_low=mbA_low, mbA_upp=mbA_upp, abs_alpha0_bins=abs_alpha0_bins, mbA_bins=mbA_bins)
	
	#Save data.
	ofile=outputdir+'new_%.3i.txt' %fili
	#np.savetxt(ofile, o_data, delimiter=', ', newline='\n', header=' Parameters: \n beta0=%e \n sp=%e \n sM=%e \n mtb=%e \n nt0=%e \n Gamma=%e \n kns=%e \n Columns: \n mA/solar_mass, alpha0, alphaA_true, alphaA_check, betaA, kA, pcen/Pa, phicen.' %(beta0, sp, sM, mtb, nt0, Gamma, kns))
	np.savetxt(ofile, o_data, delimiter=' ', newline='\n', header=' Parameters: \n beta0=%e \n sp=%e \n sM=%e \n mtb=%e \n nt0=%e \n Gamma=%e \n kns=%e \n Columns: \n mA/solar_mass, alpha0, alphaA_true, alphaA_check, betaA, kA, pcen/Pa, phicen.' %(beta0, sp, sM, mtb, nt0, Gamma, kns))


