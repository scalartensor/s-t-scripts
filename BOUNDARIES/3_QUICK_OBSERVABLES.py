#!/usr/bin/env python -u
import numpy as np
import pylab as py
import os
import COMMON as CM
from scipy import interpolate as ip
from scipy.interpolate import LinearNDInterpolator as LNDI
from USEFUL import time_estimate

#Input parameters.
#inputfile='./TEMP/ofile.npy'
#inputfile='./new_058_1141_massive_temp.txt'
inputfile='./new_039.txt'
#inputdir='../../../data/ANALYSIS/BOUNDARIES/RUN002/NEW_ANALYSIS/'
#outputfile='../../../data/ANALYSIS/BOUNDARIES/alpha0beta0_NEW.txt'
#maxnum=1000 #Number of random points to take from every beta0 file.
mpmin, mpmax=1.3, 1.35 #Minimum and maximum pulsar mass.
mcmin, mcmax = 0.97, 0.98 #Minimum and maximum companion mass.
#Around the region where there is overlap in GR.
#mpmin, mpmax=1.28, 1.38 #Minimum and maximum pulsar mass.
#mcmin, mcmax = 0.9, 1.05 #Minimum and maximum companion mass.
#A region outside the GR one.
#mpmin, mpmax=1.68, 1.88 #Minimum and maximum pulsar mass.
#mcmin, mcmax = 1.1, 1.2 #Minimum and maximum companion mass.
#mpbins=1000 #Number of pixels in mp.
mcbins=500 #Number of pixels in mc.
Pbdot_err_factor=1. #Factor by which the error of Pbdot is multiplied.
gamma_err_factor=1. #Same thing for gamma.
omdot_err_factor=1. #Same thing for omegadot.
plot_mm=False #"True" to plot mass mass diagram.

#Binary observable parameters, from '../../data/poster.par'.
Pb=0.19765096245435756787*CM.day
Pbdot=-3.8756162690082992223e-13+5.05e-15-7.96e-16
Pbdot_err=3.0493044461725964325e-15
omdot=5.3102844274576932438*np.pi/180.*1./CM.yr
omdot_err=0.00006147865970216246*np.pi/180.*1./CM.yr
gamma=0.00073204316533203326844
gamma_err=0.00000196110316567236
ecc=0.17188249375774956839
mf=0.171706 #Mass function in solar mass.

FEA_file1='../../../data/FEA12_alpha0beta0_1141.txt'
FEA_file2='../../../data/FEA12_alpha0beta0_1738.txt'
FEA1=np.loadtxt(FEA_file1)
FEA2=np.loadtxt(FEA_file2)

#Mass-mass diagram.
#mpvec=np.linspace(mpmin, mpmax, mpbins)
mcvec=np.linspace(mcmin, mcmax, mcbins)
#MP,MC=np.meshgrid(mpvec, mcvec)

'''
data=np.load(inputfile)[()]
pcen=data['pcen']
phicen=data['phicen']
#Rns=data['Rns']
alphaA=data['alphaA']
alphaA_check=data['alphaA_check']
#phi0=data['phi0']
mA=data['mA']
#mbA=data['mbA']
#IA=data['IA']
alpha0=data['alpha0']
beta0=data['beta0']
betaA=data['betaA']
kA=data['kA']
'''
#  mA/solar_mass, alpha0, alphaA_true, alphaA_check, betaA, kA, pcen/Pa, phicen.
data=np.loadtxt(inputfile, skiprows=9)
mA=data[:,0]
alpha0=data[:,1]
alphaA=data[:,2]
alphaA_check=data[:,3]
betaA=data[:,4]
kA=data[:,5]
pcen=data[:,6]
phicen=data[:,7]

dfile=open(inputfile)
for ii,line in enumerate(dfile):
	if ii==1:
		beta0=eval(line.split('=')[1])
		break
dfile.close()

'''
maxnum=min(maxnum,len(pcen))
indis=np.arange(len(pcen))
np.random.shuffle(indis)
indis=indis[0:maxnum]

pcen=pcen[indis]
phicen=phicen[indis]
#Rns=Rns[indis]
alphaA=alphaA[indis]
alphaA_check=alphaA_check[indis]
#phi0=phi0[indis]
mA=mA[indis]
#mbA=mbA[indis]
#IA=IA[indis]
alphaB=alphaB[indis]
betaB=betaB[indis]
betaA=betaA[indis]
kA=kA[indis]
'''
sel=(mA>mpmin)&(mA<mpmax)
mA=mA[sel]
alpha0=alpha0[sel]
alphaA=alphaA[sel]
alphaA_check=alphaA_check[sel]
betaA=betaA[sel]
kA=kA[sel]
pcen=pcen[sel]
phicen=phicen[sel]

alphaB=alpha0

betaB=beta0*np.ones(len(alpha0))

py.ion()
fig = py.figure()
ax  = fig.add_subplot(3, 1, 1)
bx= fig.add_subplot(3,1,2)
mx=fig.add_subplot(3,1,3)
b1=bx.plot([],[],'^', markersize=10).pop(0)
bx.plot(FEA1[:,0], FEA1[:,1], color='purple', linewidth=1, alpha=0.1)
bx.plot(FEA2[:,0], FEA2[:,1], color='blue', linewidth=1, alpha=0.1)

#t=time_estimate(len(alphaA)) #A class that prints estimated computation time.
t=time_estimate(len(mcvec)) #A class that prints estimated computation time.

for ii, mB_i in enumerate(mcvec):
	t.display() #Shows the remaining computation time.
	t.increase() #Needed to calculate the remaining computation time.
	
	#print alphaA_i, alphaB_i, betaA_i, betaB_i, kA_i
	monopole, dipole, quadrupole_phi, quadrupole_g=CM.Pbdot_f(mA, mB_i, Pb, ecc, alphaA, alphaB, betaA, betaB)
	Pbdot_mat=monopole+dipole+quadrupole_phi+quadrupole_g
	gamma_mat=CM.gamma_f(mA, mB_i, Pb, ecc, alphaA, alphaB, kA)
	omdot_mat=CM.omdot_f(mA, mB_i, Pb, ecc, alphaA, alphaB, betaA, betaB)

	#Selections.
	Pbdot_sel=(abs(Pbdot_mat-Pbdot)<Pbdot_err*Pbdot_err_factor)
	gamma_sel=(abs(gamma_mat-gamma)<gamma_err*gamma_err_factor)
	omdot_sel=(abs(omdot_mat-omdot)<omdot_err*omdot_err_factor)
	mf_sel=(mB_i**3.>(mf*(mA+mB_i)**2)) #Mass function requirement.
	#all_sel=(Pbdot_sel&gamma_sel&omdot_sel)
	all_sel=(Pbdot_sel&gamma_sel&omdot_sel&mf_sel)

	#overlap=len(all_sel[all_sel])
	#if overlap>0:
	#	perc=abs((alphaA_i-alphaA_check_i)*100./alphaA_i) #Percentage of difference between alphaA and alphaA_check.
	#	alpha0beta0_consistent.append((alphaB_i, beta0, perc))
		#of.write('%f, %f, %f \n' %(alphaB_i, beta0, perc))
	#	of.write('%f, %f, %f, %e \n' %(alphaB_i, beta0, perc, pcen_i))

	#numlevels=30
	#levels=np.logspace(np.log10(np.amin(-Pbdot_mat)), np.log10(np.amax(-Pbdot_mat)), numlevels)
	'''
	Pbdot_yes=np.zeros(np.shape(Pbdot_mat))
	Pbdot_yes[Pbdot_sel]=1
	gamma_yes=np.zeros(np.shape(gamma_mat))
	gamma_yes[gamma_sel]=1
	omdot_yes=np.zeros(np.shape(omdot_mat))
	omdot_yes[omdot_sel]=1
	mf_yes=np.zeros(np.shape(gamma_mat))
	mf_yes[mf_sel]=1
	'''
	if len(all_sel[all_sel])>0:
		print mA[all_sel][0], alpha0[all_sel][0], alphaA[all_sel][0], alphaA_check[all_sel][0], betaA[all_sel][0], kA[all_sel][0], pcen[all_sel][0], phicen[all_sel][0]
		print mB_i
		print
		
		
		ax.cla()
		ax.plot(mA[all_sel], mB_i*np.ones(len(all_sel[all_sel])),'.')
		ax.set_xlabel('mA')
		ax.set_ylabel('mB')
		#ax.set_xlim(mpmin, mpmax)

		#b1.remove()
		b1=bx.plot(betaB[all_sel],abs(alphaB[all_sel]),'^', markersize=8, alpha=0.2).pop(0)
		mx.plot(mA[all_sel], pcen[all_sel],'^', markersize=10)

	##bx.cla()
	#bx.plot(betaB,abs(alphaB),'.')
	bx.set_yscale('log')
	bx.set_ylabel('|alpha0|')
	bx.set_xlabel('beta0')
	bx.set_xlim(-6.,6.)
	bx.set_ylim(1e-4,1e0)

	#mx.plot(mA_i, pcen_i,'.')
	#mx.vlines(mpmin, min(pcen), max(pcen))
	#mx.vlines(mpmax, min(pcen), max(pcen))
	#mx.set_yscale('log')
	#mx.set_ylim(min(pcen), max(pcen))
	py.pause(0.01)
raw_input('enter')





