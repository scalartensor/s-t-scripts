#!/usr/bin/env python -u
import numpy as np
import COMMON as CM
import pylab as py
from RUNS import run_fun

#INPUT PARAMETERS:
ifile='1141_posterold_chisq1.npy'
inputdir='../../../data/ANALYSIS/BOUNDARIES/RUN004/ALL_CHISQ/'
outputplot='../../../plots/EXCLUSION_PLOT.png' #Output directory for plots.
mB_good=0.8 #mB must be larger than this value, to avoid unphysical solutions.
factor=2 #The contour of excluded |alpha0| values will be 'factor' higher than the original exclusion contour.
run_number=4
##################################

print 'This script will export a vector beta0-alpha0 that is above the exclusion contour. This line will be used to cancel jobs above this line, to save computation time.'

data=np.load(inputdir+ifile)[()]
aalpha0=abs(data['alpha0'])
beta0=data['beta0']
mA=data['mA']
mB=data['mB']
chisq=data['chisq']

sel=(mB>mB_good)

#beta0_vec=np.unique(beta0)
split_bins, pbins, phibins, beta0_min, beta0_max, beta0_bins=run_fun(run_number)
beta0_vec=np.linspace(beta0_min, beta0_max, beta0_bins)
aalpha0_contour=np.zeros(len(beta0_vec))

for betai in xrange(len(beta0_vec)):
	#selected=(beta0[sel]==beta0_vec[betai])
	selected=np.isclose(beta0[sel],beta0_vec[betai])
	if len(selected[selected])>0:
		lim=max(aalpha0[sel][selected])*factor
	else:
		lim=0.3 #If there were no points, the limit will be fixed to this number.
	if beta0_vec[betai]<-4.6:
		lim=2e-4 #For low beta0, there are no points, I will fix the limit to this number.

	if (beta0_vec[betai]>-2.)&(beta0_vec[betai]<-0.5): #Around the peak, I increase the limit.
		lim=lim*1.8
	aalpha0_contour[betai]=lim

py.ion()
py.figure(1)
py.semilogy(beta0[sel], aalpha0[sel],'.')
py.semilogy(beta0_vec, aalpha0_contour,'.')

raw_input('enter')
exit()
py.figure(2)
py.plot(mA[sel],mB[sel],'.')
py.show()

