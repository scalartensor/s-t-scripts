#!/usr/bin/env python -u
import numpy as np
import pylab as py
import scipy
import os
import Eq_solver.DEFAULT_PARAMETERS as DEF
from RUNS import run_fun
from scipy import interpolate as ip
from scipy.interpolate import LinearNDInterpolator as LNDI
from time import sleep
from USEFUL import time_estimate
import COMMON as CM

#INPUT PARAMETERS:
run_number=2 #Number of the current run, from which data will be taken.

ploti=True #True to show plots. Otherwise False.

#Physical conditions.
mbA_min=0.5 #Minimum value of NS baryonic mass (in msun).
mbA_max=3.5 #Maximum value of NS baryonic mass (in msun).
mA_min=0. #Minimum value of NS mass (in msun).
Rns_min=1000. #Minimum value of NS radius (in m).
pcen_lim=1e35 #A typical number is of 1e35####################!!!!!!!!!!!!!!!!!!!!!!
abs_alpha0_min=1e-4 #Minimum value of |alpha0|.
abs_alpha0_max=1 #Maximum value of |alpha0|.
abs_alpha0_steps=10 #Number of steps in |alpha0| to find matching solutions.

#Characteristics of output vectors.
mbA_low=1.
mbA_upp=3.
mbA_bins=50 #Number of points in the vector of baryonic mass.
abs_alpha0_low=1e-4
abs_alpha0_upp=1
abs_alpha0_bins=100 #Number of points to output in |alpha0| and all other quantities. NOT YET IMPLEMENTED!

#Other parameters.
polyord=4 #Order of the polynomial fit on alphaA.
init_mtol=0.5 #Initial percentage of tolerance in mbA.
mtol_factor=0.9 #Multiply tolerance in baryonic mass selection by this factor.
mtol_factor_up=2. #Multiply tolerance in baryonic mass selection by this factor initially, to have enough points before any criterion on alphaA is applied.
alphaA_tol=5. #Tolerance in alphaA (in DamourEspositoFarese1998 it is 5%).
minpoints=10 #Both for the alphaA selection and the mass selection.

###########################
#Define some other quantities.
mbAvec=np.linspace(mbA_low, mbA_upp, mbA_bins)
abs_alpha0_vec=np.logspace(np.log10(abs_alpha0_min), np.log10(abs_alpha0_max), abs_alpha0_steps)

final_abs_alpha0_vec=np.logspace(np.log10(abs_alpha0_low), np.log10(abs_alpha0_upp), abs_alpha0_bins) #NOT YET USED!!!!!!

split_bins, pbins, phibins, beta0_min, beta0_max, beta0_bins=run_fun(run_number)
more_pcen_bins=split_bins*pbins #Number of pcen bins for the histogram of missing points.
more_phicen_bins=phibins #Number of phicen bins for the histogram of missing points.

#Create output folder if not existing.
inputdir='../../../data/ANALYSIS/BOUNDARIES/RUN%.3i/COMBINED/' %int(run_number)
outputdir='../../../data/ANALYSIS/BOUNDARIES/RUN%.3i/NEW_ANALYSIS_try/' %int(run_number)

CM.create_or_replace(outputdir)

if ploti:
	#Start interactive plots.
	py.ion()
	fig = py.figure()
	ax  = fig.add_subplot(3, 1, 1)
	bx= fig.add_subplot(3,1,2)
	kx=fig.add_subplot(3,1,3)

#CREATE ANOTHER ALPHA0 VEC, LOGSPACED, THAT WILL BE THE FITS OF ALPHAA, KA AND BETAA EVALUATED AT EACH ALPHA0 IN THAT VECTOR!

##############
#Load data.
inputfiles=np.sort([file for file in os.listdir(inputdir) if file[0:5]=='comb_'])

t=time_estimate(len(inputfiles)*len(mbAvec)) #A class that prints estimated computation time.

for fili in xrange(len(inputfiles)):
	ifile=inputdir+inputfiles[fili]
	ofile=outputdir+'new_%.3i.txt' %fili
	mfile=outputdir+'missing_%.3i' %fili
	
	#Vectors of inputs (pcen, phicen) where more points are needed.
	more_pcen=[]
	more_phicen=[]
	o_data=np.array([[],[],[],[],[],[],[],[]]).T

	data=np.load(ifile)[()]
	pcen=data['pcen']
	phicen=data['phicen']
	Rns=data['Rns']
	alphaA=data['alphaA']
	phi0=data['phi0']
	mA=data['mA']
	mbA=data['mbA']
	IA=data['IA']
	alpha0=data['alpha0']
	beta0=data['beta0']

	#Load other parameters (not used in the calculations, but will be included in the output).
	sp=DEF.sp #Typical maximum pressure at the centre of the NS in Pa.
	sM=DEF.sM #Typical radius of the NS in m.
	mtb=DEF.mtb #Baryonic mass (kg).
	nt0=DEF.nt0 #Baryon number density (m^-3).
	Gamma=DEF.Gamma #Dimensionless parameter of the polytrope EoS.
	kns=DEF.kns #Dimensionless constant of the polytrope EoS.

	#CHECK THAT ALPHA0=BETA0 PHI0, OTHERWISE THIS WILL NOT WORK!
	#phi0_min=abs_alpha0_min*1./abs(beta0)
	#phi0_max=abs_alpha0_max*1./abs(beta0)
	#lphi0bins=abs_alpha0_bins
	#lphi0vec=np.logspace(np.log10(phi0_min), np.log10(phi0_max), lphi0bins)
	phi0_vec=final_abs_alpha0_vec*1./abs(beta0)
	lphi0_vec=np.log10(phi0_vec)
	#mbAvec=np.linspace(min(mbA), max(mbA), mbAbins)
	mbAvec=np.linspace(mbA_low, mbA_upp, mbA_bins)

	#Take only physical solutions.
	c1=(mbA>mbA_min)&(mbA<mbA_max)&(mA>mA_min)&(Rns>Rns_min)
	c2=phicen*phi0>0 #Condition explained in DamourEsp.Far.1996-98.
	c3=pcen<pcen_lim #Take only stable solutions (also explained in DamourEsp.Far.1996-98).
	c4=(abs(alpha0)<=abs_alpha0_max)&(abs(alpha0)>=abs_alpha0_min)
	check=c1&c2&c3&c4
	pcen=pcen[check]
	phicen=phicen[check]
	Rns=Rns[check]
	alphaA=alphaA[check]
	phi0=phi0[check]
	mA=mA[check]
	mbA=mbA[check]
	IA=IA[check]
	alpha0=alpha0[check]

	#Interpolate all quantities on the log10(phi0)-mb plane.
	if len(phi0[phi0<0])>0:
		print 'Phi0 is negative! Exiting.'
		exit()
	else:
		lphi0=np.log10(phi0)
	points=np.vstack((lphi0, mbA)).T
	mA_f2=ip.LinearNDInterpolator(points, mA)
	alphaA_f2=ip.LinearNDInterpolator(points, alphaA)
	IA_f2=ip.LinearNDInterpolator(points, IA)
	alpha0_f=ip.LinearNDInterpolator(points, alpha0)
	pcen_f=ip.LinearNDInterpolator(points, pcen)
	phicen_f=ip.LinearNDInterpolator(points, phicen)

	for mbi in xrange(len(mbAvec)):
		t.display() #Shows the remaining computation time.
		t.increase() #Needed to calculate the remaining computation time.

		mbA_check=mbAvec[mbi]
	
		alphaAtrue=alphaA_f2(lphi0_vec, np.ones(abs_alpha0_bins)*mbA_check)
	
		alphaAtrue_m=0.5*(alphaAtrue[1:]+alphaAtrue[:-1])

		mAvec=mA_f2(lphi0_vec, np.ones(abs_alpha0_bins)*mbA_check)
		alphaAcalc=np.diff(np.log(mAvec))*1./np.diff(phi0_vec)
		betaA_2=np.diff(alphaAtrue)*1./np.diff(phi0_vec)
		final_abs_alpha0_vec_m=0.5*(final_abs_alpha0_vec[1:]+final_abs_alpha0_vec[:-1])

		alphaA_diff=abs(alphaAtrue_m-alphaAcalc)*100./abs(alphaAtrue_m)

		IAtrue=IA_f2(lphi0_vec, np.ones(abs_alpha0_bins)*mbA_check)
		kA_2=-np.diff(np.log(IAtrue))*1./np.diff(phi0_vec)

		if ploti:
			#Prepare plots.
			ax.cla()
			bx.cla()
			kx.cla()
			
			sel0=(-np.isnan(alphaAtrue))
			ax.plot(final_abs_alpha0_vec[sel0], abs(alphaAtrue[sel0]), '.', color='black')

			sel0=(-np.isnan(alphaAcalc))
			ax.plot(final_abs_alpha0_vec_m[sel0], abs(alphaAcalc[sel0]), '^', color='black')

			sel_tol=(-np.isnan(alphaAcalc))&(alphaA_diff<5.)
			ax.plot(final_abs_alpha0_vec_m[sel_tol], abs(alphaAcalc[sel_tol]), 's', color='pink')
			
			sel0=(-np.isnan(betaA_2))
			bx.plot(final_abs_alpha0_vec_m[sel0], betaA_2[sel0], '.', color='black')

			sel0=(-np.isnan(kA_2))
			kx.plot(final_abs_alpha0_vec_m[sel0], kA_2[sel0], '.', color='black')


		for phi0_i in xrange(abs_alpha0_steps-1):
			stepi=(abs(alpha0)>=abs_alpha0_vec[phi0_i])&(abs(alpha0)<abs_alpha0_vec[phi0_i+1])

			if len(mbA[stepi])<minpoints:
				more_pcen.extend(pcen[stepi])
				more_phicen.extend(phicen[stepi])
				break
		
			mtol=init_mtol #The tolerance in baryonic mass will decrease if necessary later on.

			while True: #First I check that the tolerance is not too small to begin with.
				mbAsel_ini=((abs(mbA[stepi]-mbA_check)*100./mbA_check)<mtol)
				if len(mbAsel_ini[mbAsel_ini])<minpoints:
					mtol=mtol_factor_up*mtol
				else:
					break

			while True: #Now the tolerance is big enough to have at least 'minpoints'. If alphaA calculated with different methods is not consistent, the tolerance will be reduced.
				mbAsel=((abs(mbA[stepi]-mbA_check)*100./mbA_check)<mtol) #Select points corresponding to a baryonic mass that differs from a given value (mbA_check) less than a certain percentage (mtol).
				if len(mbAsel[mbAsel])<minpoints: #If there are not enough points (minpoints), this region needs to be resolved with more input points. Save the inputs of this region!
					more_pcen.extend(pcen[stepi][mbAsel_ini])
					more_phicen.extend(phicen[stepi][mbAsel_ini])
					break

				#Select data in this alpha0 interval, select data that has baryonic mass close to the desired value, and sort data by increasing phi0.
				sorti=phi0[stepi][mbAsel].argsort()
				phi0_sort=phi0[stepi][mbAsel][sorti]
				mA_sort=mA[stepi][mbAsel][sorti]
				alpha0_sort=alpha0[stepi][mbAsel][sorti]
				alphaA_sort=alphaA[stepi][mbAsel][sorti]
				mbA_sort=mbA[stepi][mbAsel][sorti]
				IA_sort=IA[stepi][mbAsel][sorti]
				pcen_sort=pcen[stepi][mbAsel][sorti]
				phicen_sort=phicen[stepi][mbAsel][sorti]
				Rns_sort=Rns[stepi][mbAsel][sorti]

				lphi0_sort=np.log10(phi0_sort) #I assume that fitting on a log10(phi0_sort) is better than on a linear scale.
				alphaA_sort_m=0.5*(alphaA_sort[1:]+alphaA_sort[:-1]) #Defined at the arithmetic mean of the pixel.
				pcen_sort_m=0.5*(pcen_sort[1:]+pcen_sort[:-1]) #Defined at the arithmetic mean of the pixel.
				phicen_sort_m=0.5*(phicen_sort[1:]+phicen_sort[:-1]) #Defined at the arithmetic mean of the pixel.
				Rns_sort_m=0.5*(Rns_sort[1:]+Rns_sort[:-1]) #Defined at the arithmetic mean of the pixel.
				
				#betaA_sort_m=np.diff(alphaA_sort)*1./np.diff(phi0_sort) #This is a brute force way to calculate betaA; it should not be used in the output data.

				#Fit alphaA-log10(phi0) with a polynome.
				fit=np.polyfit(lphi0_sort, alphaA_sort, polyord)
				pol=np.poly1d(fit)
				alphaA_fit=pol(lphi0_sort)
				alphaA_fit_m=0.5*(alphaA_fit[1:]+alphaA_fit[:-1]) #Defined at the arithmetic mean of the pixel.

				#A quick way to obtain betaA is to analytically differentiate the polynome. However, given that the original polynome (alphaA) is defined in a small region, its derivative may be bad.
				dfit=(np.arange(len(fit))[::-1]*fit)[:-1]
				dpol=np.poly1d(dfit)
				betaA_fit=dpol(lphi0_sort)*1./(phi0_sort*np.log(10.)) #Not to be used in the output data. Just for plotting purposes.
				betaA_fit_m=0.5*(betaA_fit[1:]+betaA_fit[:-1]) #Defined at the arithmetic mean of the pixel.
				
				#Interpolate all quantities on the log10(phi0)-mbA plane.
				points=np.vstack((lphi0_sort, mbA_sort)).T
				try:
					mA_f=ip.LinearNDInterpolator(points, mA_sort)
					alphaA_f=ip.LinearNDInterpolator(points, alphaA_sort)
					#alpha0_f=ip.LinearNDInterpolator(points, alpha0_sort)
					IA_f=ip.LinearNDInterpolator(points, IA_sort)
					#pcen_f=ip.LinearNDInterpolator(points, pcen_sort) #Is this necessary?
					#phicen_f=ip.LinearNDInterpolator(points, phicen_sort) #Is this necessary?
					#Rns_f=ip.LinearNDInterpolator(points, Rns_sort) #Is this necessary?
				except scipy.spatial.qhull.QhullError:#If this happens, decreasing the mass tolerance will not help (I assume).
					mtol=0. #This will provoke that in the following iteration there will be no points, and hence move to the following |alpha0| step.
					continue

				#lphi0vec=np.linspace(min(lphi0_sort), max(lphi0_sort), lphi0bins)
				lphi0vec=lphi0_sort.copy()
				alpha0vec=alpha0_sort.copy()
				alpha0vec_m=0.5*(alpha0vec[1:]+alpha0vec[:-1]) #Defined at the arithmetic mean of the pixel.

				mA_interp=mA_f(lphi0vec, np.ones(len(lphi0vec))*mbA_check) #Vector of mA as a function of phi0 along the stripe of constant mbA.
				mA_interp_m=0.5*(mA_interp[1:]+mA_interp[:-1]) #Defined at the arithmetic mean of the pixel.
				IA_interp=IA_f(lphi0vec, np.ones(len(lphi0vec))*mbA_check)
				alphaA_interp=alphaA_f(lphi0vec, np.ones(len(lphi0vec))*mbA_check)
				alphaA_interp_m=0.5*(alphaA_interp[1:]+alphaA_interp[:-1]) #Defined at the arithmetic mean of the pixel.

				phi0vec_diff=np.diff(10**(lphi0vec))
				phi0vec_diff[phi0vec_diff==0.]=np.nan
				alphaA_calc_m=np.diff(np.log(mA_interp))*1./phi0vec_diff #An alternative way to obtain alphaA, from the derivative of mass; this is used as a check.
				betaA_calc_m=np.diff(alphaA_interp)*1./phi0vec_diff #Probably the best way to obtain betaA.
				kA_calc_m=-np.diff(np.log(IA_interp))*1./phi0vec_diff #Probably the best way to obtain kA.
				
				#Plot betaA with all points, compared to the betaA when selecting those points where the alphaA true and calc agree.
				
				sel=(-np.isnan(alphaA_interp_m))&(-np.isnan(alphaA_calc_m)) #Avoid numerical singularities.
				sel1=((abs(alphaA_sort_m[sel]-alphaA_fit_m[sel])*100./abs(alphaA_fit_m[sel]))<alphaA_tol) #The fitted alphaA must be similar to the original one.
				sel2=((abs(alphaA_interp_m[sel]-alphaA_fit_m[sel])*100./abs(alphaA_fit_m[sel]))<alphaA_tol) #The fitted alphaA must be similar to the interpolated one.
				sel3=((abs(alphaA_calc_m[sel]-alphaA_fit_m[sel])*100./abs(alphaA_fit_m[sel]))<alphaA_tol) #The fitted alphaA must be similar to the one calculated from a derivative of mass.
				good=sel1&sel2&sel3
				
				if len(sel[sel])<minpoints: #If this happens, decreasing the mass tolerance will not help.
					mtol=0. #This will provoke that in the following iteration there will be no points, and hence move to the following |alpha0| step.
					continue
				
				if ploti:
					a1=ax.plot(abs(alpha0vec_m[sel]), abs(alphaA_interp_m[sel]), 's', color='blue', alpha=0.5).pop(0)
					a2=ax.plot(abs(alpha0vec_m), abs(alphaA_sort_m), '^', color='green', alpha=0.5).pop(0)
					a3=ax.plot(abs(alpha0vec_m[sel]), abs(alphaA_calc_m[sel]), '.', color='red', alpha=0.5).pop(0)
					a4=ax.plot(abs(alpha0vec_m[sel][good]), abs(alphaA_calc_m[sel][good]), 'o', color='magenta', markersize=6., alpha=0.5).pop(0)
					#Sometimes there are red points in the yellow (i.e. allowed) region. These are points that, in absolute value, differ from |alphaA| less than the imposed tolerance. However, they have different sign from alphaA, and the actual difference is bigger than the tolerance.

					a5=ax.plot(abs(alpha0_sort), abs(alphaA_fit), color='black').pop(0)
					abs_alphaA_upp_m=abs(alphaA_fit_m)*(1.+alphaA_tol*0.01)
					abs_alphaA_low_m=abs(alphaA_fit_m)*(1.-alphaA_tol*0.01)
					a6=ax.fill_between(abs(alpha0vec_m[sel]), y1=abs_alphaA_upp_m[sel], y2=abs_alphaA_low_m[sel], color='yellow', alpha=0.5)

					ax.set_xscale('log')
					ax.set_yscale('log')
					ax.set_xlim(abs_alpha0_min, abs_alpha0_max)
					ax.set_xlabel('|alpha0|')
					ax.set_ylabel('|alphaA|')
					ax.set_title('beta0=%.3f, mbA=%.3f msun' %(beta0, mbA_check))
					
					b1=bx.plot(abs(alpha0vec_m[sel]), betaA_calc_m[sel], 's', color='blue', alpha=0.5).pop(0)
					#b2=bx.plot(abs(alpha0vec_m), betaA_sort_m, '^', color='green', alpha=0.5).pop(0)
					b3=bx.plot(abs(alpha0vec_m[sel][good]), betaA_calc_m[sel][good], 'o', color='magenta', markersize=6., alpha=0.5).pop(0)
					b4=bx.plot(abs(alpha0vec_m[sel]), betaA_fit_m[sel], color='black').pop(0)
					bx.set_xscale('log')
					bx.set_xlim(abs_alpha0_min, abs_alpha0_max)
					bx.set_xlabel('|alpha0|')
					bx.set_ylabel('betaA')
					
					k1=kx.plot(abs(alpha0vec_m[sel]), kA_calc_m[sel], 's', color='blue', alpha=0.5).pop(0)
					kx.set_xscale('log')
					kx.set_xlim(abs_alpha0_min, abs_alpha0_max)
					kx.set_xlabel('|alpha0|')
					kx.set_ylabel('kA')
				
					py.pause(0.0001)
					#raw_input('enter')

				if len(good[good])<minpoints:
					mtol=mtol_factor*mtol
					if ploti:
						a1.remove()
						a2.remove()
						a3.remove()
						a4.remove()
						a5.remove()
						a6.remove()
						b1.remove()
						#b2.remove()
						b3.remove()
						b4.remove()
						k1.remove()
						#del a1, a2, a3, a4, a5, a6, b1, b2, b3, b4
						del a1, a2, a3, a4, a5, a6, b1, b3, b4, k1
					continue
				else:
					#print 'Save these solutions of alphaA, betaA, kA. Then at the end fit the three with a pol, and populate the fit with a logspace vector in alpha0. That should be the output.'
					
					new_data=np.vstack((mA_interp_m[sel][good], alpha0vec_m[sel][good], alphaA_sort_m[sel][good], alphaA_calc_m[sel][good], betaA_calc_m[sel][good], kA_calc_m[sel][good], pcen_sort_m[sel][good], phicen_sort_m[sel][good])).T
					o_data=np.vstack((o_data, new_data))
					break

	#Save vector with the inputs where more data is needed.
	pcen_vec=np.logspace(np.log10(min(more_pcen)), np.log10(max(more_pcen)), more_pcen_bins)
	phicen_vec=np.logspace(np.log10(min(more_phicen)), np.log10(max(more_phicen)), more_phicen_bins)
	histi=np.histogram2d(more_pcen, more_phicen, bins=[pcen_vec, phicen_vec])[0].T
	#if ploti:
	#	py.clf()
	#	py.contourf(pcen_vec[:-1], phicen_vec[:-1], histi)
	#	py.xscale('log')
	#	py.yscale('log')
	#	raw_input('enter')
	dicti={'hist':histi, 'pcen_vec':pcen_vec, 'phicen_vec':phicen_vec, 'beta0':beta0, 'run_number':run_number, 'minpoints':minpoints, 'mbA_bins':mbA_bins, 'alphaA_tol':alphaA_tol, 'mbA_min':mbA_min, 'mbA_max':mbA_max, 'mbA_low':mbA_low,'mbA_upp':mbA_upp, 'polyord':polyord, 'init_mtol':init_mtol, 'mtol_factor':mtol_factor}

	np.save(mfile, dicti)
	#Save output data.
	np.savetxt(ofile, o_data, delimiter=',', newline='\n', header=' Parameters: \n beta0=%e \n sp=%e \n sM=%e \n mtb=%e \n nt0=%e \n Gamma=%e \n kns=%e \n Columns (output data): \n mA/solar_mass, alpha0, alphaA_true, alphaA_check, betaA, kA, pcen/Pa, phicen.' %(beta0, sp, sM, mtb, nt0, Gamma, kns))

