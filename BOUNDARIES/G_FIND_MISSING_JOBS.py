#!/usr/local/python-2.7.2/bin/python -u
import numpy as np
import pylab as py
import os, sys
from time import sleep
from USEFUL import time_estimate

#INPUT PARAMETERS:
run_number=5 #Number of the current run, from which data will be taken.

inputdir='/lustre/projects/p002_swin/prosado/scalartensor/data/ANALYSIS/BOUNDARIES/RUN%.3i/RAW/' %run_number

outputfile=inputdir+'../icfiles_not_yet_analysed.txt'

##############
#List all icfiles.
all_files=os.listdir(inputdir)

icfiles=np.sort([file for file in os.listdir(inputdir) if file[0]=='i'])
ofiles=np.sort([file for file in os.listdir(inputdir) if file[0]=='o'])

ofile=open(outputfile,'w')
ofile.close()
ofile=open(outputfile,'a')

missing=[]

t=time_estimate(len(icfiles))

for file in icfiles:
	t.display()
	t.increase()

	fileindex=file.split('icfile')[1]

	ofilename='ofile'+fileindex

	if ofilename not in ofiles:
		missing.append(file)
		ofile.write(file+'\n')
		ofile.flush()

print len(missing)

ofile.close()


