#!/usr/local/python-2.7.2/bin/python -u
import numpy as np
import pylab as py
import os
from scipy import interpolate as ip
from time import sleep
import COMMON as CM
import Eq_solver.DEFAULT_PARAMETERS as DEF
from USEFUL import time_estimate

##############################################
#INPUT PARAMETERS:
run_number=5

numjobs=800

##############################################
run_dir='/lustre/projects/p002_swin/prosado/scalartensor/data/ANALYSIS/BOUNDARIES/RUN%.3i/RAW/' %int(run_number)
qsub_dir='/home/prosado/projects/scalartensor/scalar-tensor-scripts/ANALYSIS/BOUNDARIES/G_qsub_files_RUN%.3i/'  %int(run_number)

#Create or relpace output directory.
CM.create_or_replace(qsub_dir)

#Locate files.
list_vec=os.listdir(run_dir)
parfile_vec,icfile_vec, ofile_vec=[],[],[]
for file in list_vec:
	if file[0:2]=='ic':
		icfile_vec.append(file)

#Analyse random files.
np.random.shuffle(icfile_vec)
#icfile_vec=np.array(icfile_vec)

filesperjob=len(icfile_vec)/numjobs
#leftjobs=to_analyse%numjobs

for job_i in xrange(numjobs):
	jobfile='job_%.3i.txt' %(job_i+1)
	if job_i==(numjobs-1):
		icfiles_sel=icfile_vec[filesperjob*job_i:]
	else:
		icfiles_sel=icfile_vec[filesperjob*job_i:filesperjob*(job_i+1)]
	np.savetxt(qsub_dir+jobfile, icfiles_sel, fmt='%s', newline='\n')

for job_i in xrange(numjobs):
	gfilename=qsub_dir+'job_%.3i.gstar' %(job_i+1)

	#Content of the gstar file:
	filetext=['#!/bin/csh \n\
#PBS -q gstar \n\
#PBS -l nodes=1:ppn=1 \n\
#PBS -l pmem=500mb \n\
#PBS -l walltime=10:00:00 \n\
#PBS -N %sO_job_%s \n\
#PBS -o %sO_output_%s \n\
#PBS -e %sO_error_%s \n\
\n\
echo Deploying job to CPUs ... \n\
cat $PBS_NODEFILE \n\
\n\
python /home/prosado/projects/scalartensor/scalar-tensor-scripts/ANALYSIS/BOUNDARIES/G_run_job.py %s %s job_%.3i.txt' %(qsub_dir, job_i+1, qsub_dir, job_i+1, qsub_dir, job_i+1, run_dir, qsub_dir, job_i+1)]

	np.savetxt(gfilename, filetext, fmt='%s', newline='\n')

	task='qsub '+gfilename
	print task
	os.system(task)

