#!/usr/bin/env python -u
import numpy as np
import pylab as py
import os, sys
import COMMON as CM
from scipy import interpolate as ip
from scipy.interpolate import LinearNDInterpolator as LNDI
from USEFUL import time_estimate

#INPUT PARAMETERS:
inputfile=sys.argv[1] #Scalar tensor data file.
outputfile=sys.argv[2] #Output file (with path).
parfile=sys.argv[3] #Path to pulsar par file.
abs_alpha0_min=float(sys.argv[4]) #Minimum |alpha0| for this job.
abs_alpha0_max=float(sys.argv[5]) #Maximum |alpha0|.

###########################################

#Observable pulsar data.
opd=CM.get_pulsar_data(parfile)

#Load scalar tensor data.
std=CM.getdata(inputfile)

#Split data.
std_split=CM.split_data(std, abs_alpha0_min, abs_alpha0_max)
std.clear() #This should remove the initial std from memory.

#Output chi square data to output file.
#filenum=inputfile.split('/')[-1].split('_')[1].split('.txt')[0]
#outputfile=odir+'chisq_%s.txt' %filenum
#outputfile=odir+inputfile.split('/')[-1].replace('analysis_', 'chisq_')

psrj=opd.PSRJ[0:4]

if psrj=='1141':
	CM.chisq_analysis(std_split, opd, outputfile)
elif psrj=='1738':
	CM.chisq_analysis_1738(std_split, opd, outputfile)



