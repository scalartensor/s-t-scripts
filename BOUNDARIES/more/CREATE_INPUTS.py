#!/usr/bin/env python -u
import numpy as np
import pylab as py
import os
from scipy import interpolate as ip
from time import sleep
import COMMON as CM
import Eq_solver.DEFAULT_PARAMETERS as DEF
from USEFUL import time_estimate

##############################################
#INPUT PARAMETERS:
run_dir='../../../data/ANALYSIS/BOUNDARIES/RUN2/'

split_bins=100 #The vector of central pressure will be splitted this many times.
pbins=500 #Number of values of central pressure in each splitted file.
phibins=20 #Number of values of central scalar field in each splitted file.
beta0_min=-6. #Minimum value of beta0.
beta0_max=6. #Maximum value of beta0.
beta0_bins=40 #Number of values of beta0 linearly spaced.
'''
split_bins=10 #The vector of central pressure will be splitted this many times.
pbins=10 #Number of values of central pressure in each splitted file.
phibins=10 #Number of values of central scalar field in each splitted file.
beta0_min=-6. #Minimum value of beta0.
beta0_max=6. #Maximum value of beta0.
beta0_bins=10 #Number of values of beta0 linearly spaced.
'''
##############################################
#Create or relpace output directory.
CM.create_or_replace(run_dir)

#Create parameter and initial conditions files.
numfiles=beta0_bins*split_bins

t=time_estimate(beta0_bins) #A class that prints estimated computation time.

print 'Creating %i parameter files and %i files of initial conditions...' %(beta0_bins,numfiles)
beta0vec=np.linspace(beta0_min, beta0_max, beta0_bins)
for beta0_i in xrange(beta0_bins):
	t.display() #Shows the remaining computation time.
	t.increase() #Needed to calculate the remaining computation time.
	beta0=beta0vec[beta0_i]
	parfile=run_dir+'parfile_%.3i.m' %beta0_i
	#Create parameters file.
	CM.create_par_file(beta0=beta0, parfile=parfile)
	pNsurT, rhoNcenT, pNminT, pNmaxT, phiNminT, phiNmaxT=DEF.par_fun(beta0)

	phiNcenT_vec=np.logspace(np.log10(phiNminT), np.log10(phiNmaxT), phibins)
	pcen_splitvec=np.logspace(np.log10(pNminT), np.log10(pNmaxT),split_bins+1)
	for pcen_i in xrange(len(pcen_splitvec)-1):
		icfile=run_dir+'icfile_%.3i_%.3i.txt' %(beta0_i, pcen_i)
		#outputfile=run_dir+'ofile_%.3i.txt' %beta0_i
		pNminT_splitmin=pcen_splitvec[pcen_i]
		pNmaxT_splitmax=pcen_splitvec[pcen_i+1]

		#Create initial conditions file.
		pNcenT_vec=np.logspace(np.log10(pNminT_splitmin), np.log10(pNmaxT_splitmax), pbins)
		P_vec, Phi_vec=np.meshgrid(pNcenT_vec, phiNcenT_vec)
		input=np.vstack((P_vec.flatten(),Phi_vec.flatten())).T
		np.savetxt(icfile, input, delimiter=',', newline='\n', header='pNcenT, phiNcenT')


