#!/usr/bin/env python -u
import numpy as np
import pylab as py
import os
import Eq_solver.DEFAULT_PARAMETERS as DEF
from scipy import interpolate as ip
from scipy.interpolate import LinearNDInterpolator as LNDI
from time import sleep
from USEFUL import time_estimate

#INPUT PARAMETERS:
lphi0bins=500 #Number of points in the vector of interpolated values of the scalar field at infinity, log10(phi0), in order to calculate alphaA, betaA, and kA.
mbAbins=500 #Number of points in the vector of baryonic mass.
inputdir='../../../data/ANALYSIS/BOUNDARIES/RUN2/COMBINED/'
outputdir='../../../data/ANALYSIS/BOUNDARIES/RUN2/NEW_ANALYSIS/'

#Physical conditions.
mbA_min=0.5 #Minimum value of NS baryonic mass (in msun).
mbA_max=3.5 #Maximum value of NS baryonic mass (in msun).
mA_min=0. #Minimum value of NS mass (in msun).
Rns_min=1000. #Minimum value of NS radius (in m).
pcen_lim=1e35 #####################!!!!!!!!!!!!!!!!!!!!!!
abs_alpha0_min=1e-4 #Minimum value of |alpha0|.
abs_alpha0_max=1 #Maximum value of |alpha0|.

###########################
#Create output folder if not existing.
if not os.path.isdir(outputdir): os.makedirs(outputdir)

py.ion()
fig = py.figure()
ax  = fig.add_subplot(3, 1, 1)
bx= fig.add_subplot(3,1,2)
kx=fig.add_subplot(3,1,3)

mbAvec=np.linspace(0.5, 3.5, 20)
mbAvec=np.linspace(1.2, 1.9, 4)###########

abs_alpha0_steps=10
abs_alpha0_vec=np.logspace(np.log10(abs_alpha0_min), np.log10(abs_alpha0_max), abs_alpha0_steps)

print 'CREATE ANOTHER ALPHA0 VEC, LOGSPACED, THAT WILL BE THE FITS OF ALPHAA, KA AND BETAA EVALUATED AT EACH ALPHA0 IN THAT VECTOR!'

#phi0bins=10 #I divide phi0 into this number of steps.
#phi0vec=np.logspace(min(np.log10(phi0)), max(np.log10(phi0)), phi0bins)
init_mtol=0.5 #Initial percentage of tolerance in mbA.
alphaA_tol=50.
minpoints=10 #Both for the alphaA selection and the mass selection.


##############
#Load data.
inputfiles=np.sort([file for file in os.listdir(inputdir) if file[0:5]=='comb_'])

t=time_estimate(len(inputfiles)) #A class that prints estimated computation time.

for fili in xrange(len(inputfiles)):
	t.display() #Shows the remaining computation time.
	t.increase() #Needed to calculate the remaining computation time.

	ifile=inputdir+inputfiles[fili]
	ofile=outputdir+'new_%.3i.txt' %fili

	data=np.load(ifile)[()]

	pcen=data['pcen']
	phicen=data['phicen']
	Rns=data['Rns']
	alphaA=data['alphaA']
	phi0=data['phi0']
	mA=data['mA']
	mbA=data['mbA']
	IA=data['IA']
	alpha0=data['alpha0']
	beta0=data['beta0']

	sp=DEF.sp #Typical maximum pressure at the centre of the NS in Pa.
	sM=DEF.sM #Typical radius of the NS in m.
	mtb=DEF.mtb #Baryonic mass (kg).
	nt0=DEF.nt0 #Baryon number density (m^-3).
	Gamma=DEF.Gamma #Dimensionless parameter of the polytrope EoS.
	kns=DEF.kns #Dimensionless constant of the polytrope EoS.

	############################
	#Take only physical solutions.
	c1=(mbA>mbA_min)&(mbA<mbA_max)&(mA>mA_min)&(Rns>Rns_min)
	c2=phicen*phi0>0 #Condition explained in DamourEsp.Far.1996-98.
	c3=pcen<pcen_lim
	c4=(abs(alpha0)<=abs_alpha0_max)&(abs(alpha0)>=abs_alpha0_min)
	check=c1&c2&c3&c4
	pcen=pcen[check]
	phicen=phicen[check]
	Rns=Rns[check]
	alphaA=alphaA[check]
	phi0=phi0[check]
	mA=mA[check]
	mbA=mbA[check]
	IA=IA[check]
	alpha0=alpha0[check]
	
	for mbi in xrange(len(mbAvec)):
		mbA_check=mbAvec[mbi]
		ax.cla()
		bx.cla()
		kx.cla()
		
		if beta0<-2:
			break

		#for phi0_i in xrange(len(phi0vec)-1):
		#	stepi=(phi0>=phi0vec[phi0_i])&(phi0<phi0vec[phi0_i+1])
		for phi0_i in xrange(abs_alpha0_steps-1):
			stepi=(abs(alpha0)>=abs_alpha0_vec[phi0_i])&(abs(alpha0)<abs_alpha0_vec[phi0_i+1])
		
			mtol=init_mtol
			while True:
			
				#I will fit the alphaA_true with a simple pol, impose that all points in alphaAtrue cannot differ from the fit by more than a certain percentage AND impose that alphaAcalc cannot differ from the fit by more than that percentage.
				mbAsel=((abs(mbA[stepi]-mbA_check)*100./mbA_check)<mtol)
				if len(mbAsel[mbAsel])<minpoints:
					print 'Not enough points!'
					print 'Save the pcen, phicen points for which this happens. Then repopulate these regions and rerun this analysis.'
					break
				
				sorti=phi0[stepi][mbAsel].argsort()
				phi0_sort=phi0[stepi][mbAsel][sorti]
				mA_sort=mA[stepi][mbAsel][sorti]
				alpha0_sort=alpha0[stepi][mbAsel][sorti]
				alphaA_sort=alphaA[stepi][mbAsel][sorti]
				mbA_sort=mbA[stepi][mbAsel][sorti]
				IA_sort=IA[stepi][mbAsel][sorti]

				lmA_sort=np.log(mA_sort) #Natural log of mA.
				lphi0_sort=np.log10(phi0_sort) #Log10 of phi0 (not natural!).

				polyord=4
				fit=np.polyfit(lphi0_sort, alphaA_sort, polyord)
				pol=np.poly1d(fit)
				dfit=(np.arange(len(fit))[::-1]*fit)[:-1]
				dpol=np.poly1d(dfit)
				alphaA_fit=pol(lphi0_sort)
				betaA_fit=dpol(lphi0_sort)*1./(phi0_sort*np.log(10.))
				
				#Interpolate all quantities on the log10(phi0)-mb plane.

				points=np.vstack((lphi0_sort, mbA_sort)).T
				mA_f=ip.LinearNDInterpolator(points, mA_sort)
				alphaA_f=ip.LinearNDInterpolator(points, alphaA_sort)
				alpha0_f=ip.LinearNDInterpolator(points, alpha0_sort)
				IA_f=ip.LinearNDInterpolator(points, IA_sort)

				#print len(lphi0_sort)
				#exit()

				betaA_sort_m=np.diff(alphaA_sort)*1./np.diff(phi0_sort)
				
				#lphi0vec=np.linspace(min(lphi0_sort), max(lphi0_sort), lphi0bins)
				lphi0vec=lphi0_sort.copy()
				#lphi0vec=np.sort(np.random.uniform(min(lphi0_sort), max(lphi0_sort), lphi0bins))
				mAvec=mA_f(lphi0vec, np.ones(len(lphi0vec))*mbA_check) #Vector of mA as a function of phi0 along the stripe of constant mbA.

				IAvec=IA_f(lphi0vec, np.ones(len(lphi0vec))*mbA_check)
				kA_calc_m=-np.diff(np.log(IAvec))*1./np.diff(10**(lphi0vec))

				alphaA_calc_m=np.diff(np.log(mAvec))*1./np.diff(10**(lphi0vec))
				alphaA_interp=alphaA_f(lphi0vec, np.ones(len(lphi0vec))*mbA_check)
				
				betaA_interp=np.diff(alphaA_interp)*1./np.diff(10**(lphi0vec))
				
				alpha0vec=10**(lphi0vec)*beta0

				#Make all quantities have same dimensions.
				alpha0vec_m=0.5*(alpha0vec[1:]+alpha0vec[:-1])
				alphaA_interp_m=0.5*(alphaA_interp[1:]+alphaA_interp[:-1])

				betaA_interp_m=0.5*(betaA_interp[1:]+betaA_interp[:-1])

				#betaA_fit_m=
				#Create betaA_fit in two different ways: differentiating the fitted alphaA, and from the analytical derivative of the polynomial.
				#Plot betaA with all points, compared to the betaA when selecting those points where the alphaA true and calc agree.

				alphaA_fit_m=0.5*(alphaA_fit[1:]+alphaA_fit[:-1])
				alphaA_sort_m=0.5*(alphaA_sort[1:]+alphaA_sort[:-1])
				
				betaA_fit_m=0.5*(betaA_fit[1:]+betaA_fit[:-1])

				sel=(-np.isnan(alphaA_interp_m))
				sel1=((abs(alphaA_interp_m[sel]-alphaA_fit_m[sel])*100./abs(alphaA_fit_m[sel]))<alphaA_tol)
				sel2=((abs(alphaA_sort_m[sel]-alphaA_fit_m[sel])*100./abs(alphaA_fit_m[sel]))<alphaA_tol)
				sel3=((abs(alphaA_calc_m[sel]-alphaA_fit_m[sel])*100./abs(alphaA_fit_m[sel]))<alphaA_tol)
				#sel3=((abs(abs(alphaA_calc_m[sel])-abs(alphaA_fit_m[sel]))*100./abs(alphaA_fit_m[sel]))<alphaA_tol) #This would be less restrictive, since the sign of alphaA would be ignored.
				
				good=sel1&sel2&sel3
			
				if len(sel[sel])>0:
					a1=ax.plot(abs(alpha0vec_m[sel]), abs(alphaA_interp_m[sel]), 's', color='blue', alpha=0.5).pop(0)
					a2=ax.plot(abs(alpha0vec_m), abs(alphaA_sort_m), '^', color='green', alpha=0.5).pop(0)
					a3=ax.plot(abs(alpha0vec_m[sel]), abs(alphaA_calc_m[sel]), '.', color='red', alpha=0.5).pop(0)
					a4=ax.plot(abs(alpha0vec_m[sel][good]), abs(alphaA_calc_m[sel][good]), 'o', color='magenta', markersize=6., alpha=0.5).pop(0)
					#Sometimes there are red points in the yellow (i.e. allowed) region. These are points that, in absolute value, differ from |alphaA| less than the imposed tolerance. However, they have different sign from alphaA, and the actual difference is bigger than the tolerance.

					a5=ax.plot(abs(alpha0_sort), abs(alphaA_fit), color='black').pop(0)
					abs_alphaA_upp_m=abs(alphaA_fit_m)*(1.+alphaA_tol*0.01)
					abs_alphaA_low_m=abs(alphaA_fit_m)*(1.-alphaA_tol*0.01)
					a6=ax.fill_between(abs(alpha0vec_m[sel]), y1=abs_alphaA_upp_m[sel], y2=abs_alphaA_low_m[sel], color='yellow', alpha=0.5)

					ax.set_xscale('log')
					ax.set_yscale('log')
					ax.set_xlim(abs_alpha0_min, abs_alpha0_max)
					ax.set_xlabel('|alpha0|')
					ax.set_ylabel('|alphaA|')
					ax.set_title('beta0=%.3f, mbA=%.3f msun' %(beta0, mbA_check))
					
					b1=bx.plot(abs(alpha0vec_m[sel]), betaA_interp_m[sel], 's', color='blue', alpha=0.5).pop(0)
					#b2=bx.plot(abs(alpha0vec_m), betaA_sort_m, '^', color='green', alpha=0.5).pop(0)
					b3=bx.plot(abs(alpha0vec_m[sel][good]), betaA_interp_m[sel][good], 'o', color='magenta', markersize=6., alpha=0.5).pop(0)
					b4=bx.plot(abs(alpha0vec_m[sel]), betaA_fit_m[sel], color='black').pop(0)
					bx.set_xscale('log')
					bx.set_xlim(abs_alpha0_min, abs_alpha0_max)
					bx.set_xlabel('|alpha0|')
					bx.set_ylabel('betaA')
					
					k1=kx.plot(abs(alpha0vec_m[sel]), kA_calc_m[sel], 's', color='blue', alpha=0.5).pop(0)
					kx.set_xscale('log')
					kx.set_xlim(abs_alpha0_min, abs_alpha0_max)
					kx.set_xlabel('|alpha0|')
					kx.set_ylabel('kA')
				
					py.draw()
					sleep(0.01)
					#raw_input('enter')

					if len(good[good])<minpoints:
						mtol=0.9*mtol
						a1.remove()
						a2.remove()
						a3.remove()
						a4.remove()
						a5.remove()
						a6.remove()
						b1.remove()
						#b2.remove()
						b3.remove()
						b4.remove()
						k1.remove()
						#del a1, a2, a3, a4, a5, a6, b1, b2, b3, b4
						del a1, a2, a3, a4, a5, a6, b1, b3, b4, k1
						continue
					else:
						print 'Save these solutions of alphaA, betaA, kA. Then at the end fit the three with a pol, and populate the fit with a logspace vector in alpha0. That should be the output.'
						break

