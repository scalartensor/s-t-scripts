#!/usr/bin/env python -u

##!/usr/local/python-2.7.2/bin/python -u
import numpy as np
import pylab as py
import os,sys
from USEFUL import time_estimate

#filename='/projects/p002_swin/vvenkatr/abc/glory_finished.txt'
filename=sys.argv[1]
delim=sys.argv[2]
FEA_file1='../../../data/FEA12_alpha0beta0_1141.txt'
FEA_file2='../../../data/FEA12_alpha0beta0_1738.txt'
#FEA_file1='/home/prosado/projects/scalartensor/scalar-tensor-scripts/ANALYSIS/FEA12_alpha0beta0_1141.txt'
#FEA_file2='/home/prosado/projects/scalartensor/scalar-tensor-scripts/ANALYSIS/FEA12_alpha0beta0_1738.txt'

#mA/solar_mass, alpha0, alphaA_true, alphaA_check, betaA, kA, pcen/Pa , phic,  beta0, percent, numpts, intersect.
#data=np.loadtxt(filename, delimiter=' ', dtype=float)
#data=np.loadtxt(filename, delimiter='%s' %str(delim))
#data=np.fromfile(filename, dtype=float, count=-1, sep=' ')
data=np.genfromtxt(filename,delimiter='%s' %str(delim))

FEA1=np.loadtxt(FEA_file1)
FEA2=np.loadtxt(FEA_file2)

mA=data[:,0]
alpha0=data[:,1]
beta0=data[:,8]
intersect=data[:,11].astype(int)
pcen=data[:,6]
mA_low=data[:,12]
mA_upp=data[:,13]
percent=data[:,9]
phicen=data[:,7]

mA_av=0.5*(mA_low+mA_upp)

beta0_min=-6.
beta0_max=6.
beta0_bins=50
beta0_vec=np.linspace(beta0_min, beta0_max, beta0_bins)

abs_alpha0_min=1e-4
abs_alpha0_max=1.
abs_alpha0_bins=500
abs_alpha0_vec=np.logspace(np.log10(abs_alpha0_min), np.log10(abs_alpha0_max), abs_alpha0_bins)

sel=(intersect==1)

histi=np.histogram2d(beta0[sel], abs(alpha0[sel]), bins=[beta0_vec, abs_alpha0_vec])[0].T

histi[histi>0]=1.2
levels=[0.,1.,2.]

beta0_vec_m=0.5*(beta0_vec[1:]+beta0_vec[:-1])
abs_alpha0_vec_m=0.5*(abs_alpha0_vec[1:]+abs_alpha0_vec[:-1])

#py.ion()
'''
py.figure(1)
#py.imshow(histi, aspect='auto', origin='lower')
py.contourf(beta0_vec_m, abs_alpha0_vec_m, histi, levels=levels)
py.yscale('log')
py.xlabel('beta0')
py.ylabel('|alpha0|')
'''
py.ion()
py.figure(2)
while True:
	py.clf()
	#py.yscale('log')
	#sel2=(pcen<8e33)&(mA>1.3)&(mA<1.5)
	#sel2=(mA>1.41)&(mA<1.52)
	mAvec=np.linspace(1., 2., 30)
	lalpha0=np.log10(abs(alpha0))
	beta0vec=np.linspace(-6.,6.,50)
	lalpha0vec=np.linspace(-4., 0., 50)
	for mi in xrange(len(mAvec)-1):
		#print mAvec[mi]
		py.clf()
		sel2=(mA>mAvec[mi])&(mA<mAvec[mi+1])
		sorti=mA[sel&sel2].argsort()
		#histi=np.histogram2d(beta0[sel&sel2], lalpha0[sel&sel2], bins=[beta0vec, lalpha0vec])[0]
		#histi[histi>0]=1
		#py.imshow(histi.T, origin='lower', aspect='auto', interpolation='none', extent=[-6.,6.,-4.,0.], alpha=0.5)
		py.plot(FEA1[:,0], np.log10(FEA1[:,1]), color='purple', linewidth=4, alpha=0.5)
		py.plot(FEA2[:,0], np.log10(FEA2[:,1]), color='blue', linewidth=4, alpha=0.5)
		sct=py.scatter(beta0[sel&sel2][sorti], lalpha0[sel&sel2][sorti], c=mA_av[sel&sel2][sorti], s=30)
		
		
		#cmap = colors.ListedColormap(['b','g','y','r'])
		cmap=py.cm.winter
		#norm = py.colors.BoundaryNorm(bounds, cmap.N)
		#im=ax.imshow(data[None], aspect='auto',cmap=cmap, norm=norm)
		#cbar = fig.colorbar(im, cax=cax, cmap=cmap, norm=norm, boundaries=bounds,
		bounds=np.linspace(1.,2.,15)
		ticks=bounds
		#norm = colors.BoundaryNorm(bounds, cmap.N)
		#im=ax.imshow(data[None], aspect='auto',cmap=cmap, norm=norm)
		#py.colorbar(sct, boundaries=bounds)
		good=(mA<=mA_upp)&(mA>=mA_low)
		if len(mA[sel&sel2&good])>0:
			print 'good ones!'
			py.scatter(beta0[sel&sel2&good], lalpha0[sel&sel2&good], c='black', s=50, marker='s')
		clb=py.colorbar(sct, cmap=cmap, boundaries=bounds, ticks=ticks)
		#		v = np.linspace(1., 2., 15)
		#clb=py.colorbar(ticks=v)
		clb.set_clim(1.,2.)
		py.xlim(-6.,6.)
		py.ylim(-4.,1.)
		py.ylim(np.log10(abs_alpha0_min), np.log10(abs_alpha0_max))
		py.title('%s, mass %.3f' %(filename, mAvec[mi]))

		py.pause(0.1)
#	raw_input('enter')
#py.show()
