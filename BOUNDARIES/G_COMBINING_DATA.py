#!/usr/local/python-2.7.2/bin/python -u
import numpy as np
import pylab as py
import os, sys
import COMMON as CM
import Eq_solver.DEFAULT_PARAMETERS as DEF
from USEFUL import time_estimate
from RUNS import run_fun

###########################
inputdir=sys.argv[1]
outputdir=sys.argv[2]
beta0_i=int(sys.argv[3])
run_number=int(sys.argv[4])

unused, unused, unused, beta0_min, beta0_max, beta0_bins=run_fun(run_number)

beta0vec=np.linspace(beta0_min, beta0_max, beta0_bins)

npyfile='comb_%.3i' %beta0_i

beta0=beta0vec[beta0_i]

initial_files=inputdir+'ofile_%.3i_*' %beta0_i
tempfile=inputdir+'temp_%.3i.txt' %beta0_i
correct_files="cat %s | awk '{if(NF==9) print $0}' | grep -v 'I' > %s" %(initial_files, tempfile) #Remove bad lines and "Indeterminate" solutions (or solutions with imaginary part, "I").
os.system(correct_files)

sols=np.loadtxt(tempfile, dtype='float')
pcen=sols[:,0]*DEF.sp #Pressure at the centre of the NS in Pa.
phicen=sols[:,1] #Value of the scalar field at the centre of the NS.
Rns=sols[:,2] #Radius of the NS in m.
alphaA=sols[:,3] #Effective coupling (dimensionless).
phi0=sols[:,4] #Scalar field at infinity (dimensionless).
mA=sols[:,5] #Mass in solar masses.
mbA=sols[:,6] #Mass (baryonic) in solar masses.
IA=sols[:,7] #Moment of inertia in kg m^2.
alpha0=sols[:,8] #Matter coupling in spatial infinity (dimensionless).

dicti={'pcen':pcen, 'phicen':phicen, 'Rns':Rns, 'alphaA':alphaA, 'phi0':phi0, 'mA':mA, 'mbA':mbA, 'IA':IA, 'alpha0':alpha0, 'beta0':beta0}

np.save(outputdir+npyfile, dicti)
remove_tempfile="rm %s" %tempfile
os.system(remove_tempfile)
