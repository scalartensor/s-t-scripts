#!/usr/local/python-2.7.2/bin/python -u
import numpy as np
import os
from USEFUL import time_estimate

##############################################
#INPUT PARAMETERS:
run_number=6

##############################################
run_dir='/lustre/projects/p002_swin/prosado/scalartensor/data/ANALYSIS/BOUNDARIES/RUN%.3i/RAW/' %int(run_number)

##############################################
icfiles=np.array([file for file in os.listdir(run_dir) if file[0:6]=='icfile'])

if len(icfiles)==0:
	print 'Input parameter files ("icfiles") have already been removed!'
	exit()

print 'Do you want to delete all of the following %i files of initial conditions?' %len(icfiles)
print icfiles

deci=raw_input('Type "y" to delete.')

if deci=='y':
	print 'All files of input parameters will be removed from %s.' %run_dir
	raw_input('Press enter to proceed.')
	t=time_estimate(len(icfiles)) #A class that prints estimated computation time.

	for file in icfiles:
		t.display() #Shows the remaining computation time.
		t.increase() #Needed to calculate the remaining computation time.

		file_to_remove='%s' %(run_dir+file)
		
		os.remove('%s' %file_to_remove)

