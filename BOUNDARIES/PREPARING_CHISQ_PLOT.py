#!/usr/bin/env python -u
import numpy as np
#import pylab as py
#import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as py
import os,sys
import COMMON as CM

#INPUT PARAMETERS:
datadir='/lustre/projects/p002_swin/prosado/scalartensor/data/ANALYSIS/BOUNDARIES/RUN006/'
dir1=datadir+'CHISQ_1141_invariant'
dir2=datadir+'CHISQ_1738_FEA12'
#dir3=datadir+'CHISQ_1141_poster'
dirlist=[dir1,dir2]
outputdir=datadir+'ALL_CHISQ/'

chisq_lim=1 #Chi square limit (usually 1).
Kepler=0 #Either 0 or 1 (to impose that Kepler's third law must be fulfilled).

#########################################

#Create output dir if not existing.
if not os.path.isdir(outputdir): os.makedirs(outputdir)

def combine_chisq(inputdir, outputfile, chisq_lim, Kepler):
	''''''	
	if Kepler==1:
		all_files=np.sort([file for file in os.listdir(inputdir) if (file[0:5]=='chisq')&(file[-10:]=='Kepler.txt') ])
		outputfile+='_Kepler'
	else:
		all_files=np.sort([file for file in os.listdir(inputdir) if (file[0:5]=='chisq')&(file[-10:]!='Kepler.txt') ])

	beta0=[]
	alpha0=[]
	chisq=[]
	mA=[]
	mB=[]
	alphaA=[]

	for fili in xrange(len(all_files)):
		data=np.genfromtxt(inputdir+'/'+all_files[fili])
		if len(np.shape(data))==2: #To avoid loading files that are not yet populated.
			beta0.extend(data[:,0])
			alpha0.extend(data[:,1])
			chisq.extend(data[:,2])
			mA.extend(data[:,3])
			mB.extend(data[:,4])
			alphaA.extend(data[:,5])

	beta0=np.array(beta0)
	alpha0=np.array(alpha0)
	chisq=np.array(chisq)
	mA=np.array(mA)
	mB=np.array(mB)
	alphaA=np.array(alphaA)

	sel=(chisq<=chisq_lim)&(chisq>0.)

	dicti={'beta0':beta0[sel], 'alpha0':alpha0[sel], 'mA':mA[sel], 'mB':mB[sel], 'alphaA':alphaA[sel], 'chisq':chisq[sel]}
	np.save(outputfile, dicti)


#################################

for inputdir in dirlist:
	pulsar_tag=inputdir.split('CHISQ_')[1]
	pulsar=pulsar_tag.split('_')[0]
	if pulsar=='1738' and Kepler==1:
		continue
	print 'Combining chisquare files for %s' %pulsar_tag
	outputfile=outputdir+'/'+pulsar_tag+'_chisq%i' %chisq_lim
	combine_chisq(inputdir, outputfile, chisq_lim, Kepler)	
