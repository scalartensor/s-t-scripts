#!/usr/local/python-2.7.2/bin/python -u
import numpy as np
import pylab as py
import os, sys
import COMMON as CM

run_dir=sys.argv[1]
qsub_dir=sys.argv[2]
job_filename=sys.argv[3]

job_file=open(qsub_dir+job_filename)
for icfile in job_file:
	par_i=icfile.split('.')[0].split('_')[1]
	pcen_i=icfile.split('.')[0].split('_')[2]
	parfilename='parfile_%s.m' %par_i
	icfilename='icfile_%s_%s.txt' %(par_i, pcen_i)
	ofilename='ofile_%s_%s.txt' %(par_i, pcen_i)
	CM.eq_solver(run_dir+parfilename, run_dir+icfilename, run_dir+ofilename, cluster=True)

