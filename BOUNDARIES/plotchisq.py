#!/usr/bin/env python
import pylab as py
import numpy as np
import sys

file = sys.argv[1]

data = np.loadtxt(file,delimiter=' ')

m2=data[:,2]
sini=data[:,1]
chisq=data[:,3]

chisq_mat=chisq.reshape((90,11))
m2_mat=m2.reshape((90,11))
sini_mat=sini.reshape((90,11))

delta_mat=chisq_mat-np.amin(chisq_mat, axis=0)

indi=np.argmin(delta_mat)

print 'Absolute minimum at m2=%.3f, sini=%.3e. ' %(m2[indi], sini[indi])
py.ion()

py.figure(1)
py.contourf(sini_mat, m2_mat, np.log10(delta_mat))
py.xlabel('sin(i)')
py.ylabel('m2/msun')
py.colorbar(label='log10(delta_chisq)')

py.figure(2)
py.contourf(sini_mat, m2_mat, np.log10(chisq_mat*1./46774.))
py.xlabel('sin(i)')
py.ylabel('m2/msun')
py.colorbar(label='log10(chisq)')

#raw_input('enter to exit')

py.figure(3)
for mi in xrange(11):
	py.plot(sini_mat[:,mi], delta_mat[:,mi])
	py.ylim(0.,10)
	py.xlabel('sin(i)')
	py.title('m2=%.3f' %m2_mat[:,mi][0])
	raw_input('enter')
exit()
del_chisq = data[:,3] - np.min(data[:,3])

indi=data[:,3].argmin()


py.scatter(data[:,0], data[:,2],c=(np.log10(del_chisq)))

py.xlabel('inclination')
py.ylabel('mass')
py.colorbar()

py.show()
