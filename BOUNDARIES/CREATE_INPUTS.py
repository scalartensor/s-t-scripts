#!/usr/bin/env python -u
import numpy as np
import pylab as py
import os
from scipy import interpolate as ip
from time import sleep
import COMMON as CM
import Eq_solver.DEFAULT_PARAMETERS as DEF
from USEFUL import time_estimate
from RUNS import run_fun

##############################################
#INPUT PARAMETERS:
run_number=5

##############################################
#Create or relpace output directory.

run_dir='../../../data/ANALYSIS/BOUNDARIES/RUN%.3i/RAW/' %int(run_number)

CM.create_or_replace(run_dir)

#Create parameter and initial conditions files.
split_bins, pbins, phibins, beta0_min, beta0_max, beta0_bins=run_fun(run_number)

numfiles=beta0_bins*split_bins

t=time_estimate(beta0_bins) #A class that prints estimated computation time.

print 'Creating %i parameter files and %i files of initial conditions...' %(beta0_bins,numfiles)
beta0vec=np.linspace(beta0_min, beta0_max, beta0_bins)
for beta0_i in xrange(beta0_bins):
	t.display() #Shows the remaining computation time.
	t.increase() #Needed to calculate the remaining computation time.
	beta0=beta0vec[beta0_i]
	parfile=run_dir+'parfile_%.3i.m' %beta0_i
	#Create parameters file.
	CM.create_par_file(beta0=beta0, parfile=parfile)
	pNsurT, rhoNcenT, pNminT, pNmaxT, phiNminT, phiNmaxT=DEF.par_fun(beta0)

	#phiNcenT_vec=np.logspace(np.log10(phiNminT), np.log10(phiNmaxT), phibins)
	pcen_splitvec=np.logspace(np.log10(pNminT), np.log10(pNmaxT),split_bins+1)
	for pcen_i in xrange(len(pcen_splitvec)-1):
		icfile=run_dir+'icfile_%.3i_%.3i.txt' %(beta0_i, pcen_i)
		#outputfile=run_dir+'ofile_%.3i.txt' %beta0_i
		pcen_splitmin=pcen_splitvec[pcen_i]
		pcen_splitmax=pcen_splitvec[pcen_i+1]

		#Create initial conditions file.
		#pNcenT_vec=np.logspace(np.log10(pcen_splitmin), np.log10(pcen_splitmax), pbins)
		P_vec=10.**(np.random.uniform(np.log10(pcen_splitmin), np.log10(pcen_splitmax), phibins*pbins))
		Phi_vec=10.**(np.random.uniform(np.log10(phiNminT), np.log10(phiNmaxT), phibins*pbins))
		#P_vec, Phi_vec=np.meshgrid(pNcenT_vec, phiNcenT_vec)
		#input=np.vstack((P_vec.flatten(),Phi_vec.flatten())).T
		input=np.vstack((P_vec,Phi_vec)).T
		
		#py.ion()
		#py.loglog(input[:,0], input[:,1],'.')
		#raw_input('enter')
		np.savetxt(icfile, input, delimiter=',', newline='\n', header='pNcenT, phiNcenT')



