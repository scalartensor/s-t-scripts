#!/usr/bin/env python -u
import numpy as np
import pylab as py
import os
import Eq_solver.DEFAULT_PARAMETERS as DEF
from scipy import interpolate as ip
from scipy.interpolate import LinearNDInterpolator as LNDI
from time import sleep
from USEFUL import time_estimate

#INPUT PARAMETERS:
lphi0bins=500 #Number of points in the vector of interpolated values of the scalar field at infinity, log10(phi0), in order to calculate alphaA, betaA, and kA.
mbAbins=500 #Number of points in the vector of baryonic mass.
inputdir='../../../data/ANALYSIS/BOUNDARIES/RUN2/COMBINED/'
outputdir='../../../data/ANALYSIS/BOUNDARIES/RUN2/OLD_ANALYSIS/'

py.ion()

#Physical conditions.
mbA_min=0.5 #Minimum value of NS baryonic mass (in msun).
mbA_max=3.5 #Maximum value of NS baryonic mass (in msun).
mA_min=0. #Minimum value of NS mass (in msun).
Rns_min=1000. #Minimum value of NS radius (in m).
alpha0_min=0. #Minimum value of alpha0.

###########################
#Create output folder if not existing.
if not os.path.isdir(outputdir): os.makedirs(outputdir)

##############
#Load data.
inputfiles=np.sort([file for file in os.listdir(inputdir) if file[0:5]=='comb_'])

t=time_estimate(len(inputfiles)) #A class that prints estimated computation time.

for fili in xrange(len(inputfiles)):
	t.display() #Shows the remaining computation time.
	t.increase() #Needed to calculate the remaining computation time.

	ifile=inputdir+inputfiles[fili]
	ofile=outputdir+'old_%.3i.txt' %fili

	data=np.load(ifile)[()]
	pcen=data['pcen']
	phicen=data['phicen']
	Rns=data['Rns']
	alphaA=data['alphaA']
	phi0=data['phi0']
	mA=data['mA']
	mbA=data['mbA']
	IA=data['IA']
	alpha0=data['alpha0']
	beta0=data['beta0']

	sp=DEF.sp #Typical maximum pressure at the centre of the NS in Pa.
	sM=DEF.sM #Typical radius of the NS in m.
	mtb=DEF.mtb #Baryonic mass (kg).
	nt0=DEF.nt0 #Baryon number density (m^-3).
	Gamma=DEF.Gamma #Dimensionless parameter of the polytrope EoS.
	kns=DEF.kns #Dimensionless constant of the polytrope EoS.

	############################
	#Take only physical solutions.
	c1=(mbA>mbA_min)&(mbA<mbA_max)&(mA>mA_min)&(Rns>Rns_min)
	c2=phicen*phi0>0 #Condition explained in DamourEsp.Far.1996-98.
	check=c1&c2
	pcen=pcen[check]
	phicen=phicen[check]
	Rns=Rns[check]
	alphaA=alphaA[check]
	phi0=phi0[check]
	mA=mA[check]
	mbA=mbA[check]
	IA=IA[check]
	alpha0=alpha0[check]

	#Interpolate all quantities on the log10(phi0)-mb plane.
	lphi0=np.log10(phi0)
	points=np.vstack((lphi0, mbA)).T
	mA_f=ip.LinearNDInterpolator(points, mA)
	alphaA_f=ip.LinearNDInterpolator(points, alphaA)
	IA_f=ip.LinearNDInterpolator(points, IA)
	alpha0_f=ip.LinearNDInterpolator(points, alpha0)
	pcen_f=ip.LinearNDInterpolator(points, pcen)
	phicen_f=ip.LinearNDInterpolator(points, phicen)

	lphi0vec=np.linspace(min(lphi0), max(lphi0), lphi0bins)
	mbAvec=np.linspace(min(mbA), max(mbA), mbAbins)
	alphaAcalc_mat=np.zeros((lphi0bins-1,mbAbins))
	alphaAtrue_mat=np.zeros(np.shape(alphaAcalc_mat))
	kA_mat=np.zeros(np.shape(alphaAcalc_mat))
	alpha0_mat=np.zeros(np.shape(alphaAcalc_mat))
	mA_mat=np.zeros(np.shape(alphaAcalc_mat))
	pcen_mat=np.zeros(np.shape(alphaAcalc_mat))
	phicen_mat=np.zeros(np.shape(alphaAcalc_mat))

	for mbi in xrange(len(mbAvec)):
		mAvec=mA_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi]) #Vector of mA as a function of phi0 along the stripe of constant mbA.
		mA_mat[:,mbi]=0.5*(mAvec[1:]+mAvec[:-1])

		alphaAcalc_mat[:,mbi]=np.diff(np.log(mAvec))*1./np.diff(10**(lphi0vec))
		alphaAtrue=alphaA_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi])
		alphaAtrue_mat[:,mbi]=0.5*(alphaAtrue[1:]+alphaAtrue[:-1])
		
		IAvec=IA_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi])
		kA_mat[:,mbi]=-np.diff(np.log(IAvec))*1./np.diff(10**(lphi0vec))

		alpha0true=alpha0_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi])
		alpha0_mat[:,mbi]=0.5*(alpha0true[1:]+alpha0true[:-1])
	
		pcenvec=pcen_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi])
		pcen_mat[:,mbi]=0.5*(pcenvec[1:]+pcenvec[:-1])

		phicenvec=phicen_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi])
		phicen_mat[:,mbi]=0.5*(phicenvec[1:]+phicenvec[:-1])
	
		xval=alpha0_mat[:,mbi].copy()
		xval[np.isnan(xval)]=0
		yval1=alphaAcalc_mat[:,mbi]
		yval1[np.isnan(yval1)]=0
		yval2=alphaAtrue_mat[:,mbi]
		yval2[np.isnan(yval2)]=0
		if len(yval1[yval1!=0])>0:
			py.clf()
			py.plot(abs(xval), abs(yval1),'.', color='blue')
			py.plot(abs(xval), abs(yval2),'.', color='green')
			py.xscale('log')
			py.yscale('log')
			py.xlim(1e-4, 1)
			py.title('beta0=%.3f, mbA=%.3f msun.' %(beta0, mbAvec[mbi]))
			py.draw()
			sleep(0.1)
			#raw_input('enter')
		else:
			continue

	
	#tol=5 #Tolerance: percentage to which the derived and "true" alphaA must coincide.

	alphaAtrue_m=0.5*(alphaAtrue_mat[1:,:]+alphaAtrue_mat[:-1,:])
	alphaAcalc_m=0.5*(alphaAcalc_mat[1:,:]+alphaAcalc_mat[:-1,:])
	lphi0vec_m=0.5*(lphi0vec[1:]+lphi0vec[:-1])
	betaA_mat=(np.diff(alphaAtrue_mat,axis=0).T*1./np.diff(lphi0vec_m)).T
	kA_m=0.5*(kA_mat[1:,:]+kA_mat[:-1,:])
	alpha0_m=0.5*(alpha0_mat[1:,:]+alpha0_mat[:-1,:])
	mA_m=0.5*(mA_mat[1:,:]+mA_mat[:-1,:])
	pcen_m=0.5*(pcen_mat[1:,:]+pcen_mat[:-1,:])
	phicen_m=0.5*(phicen_mat[1:,:]+phicen_mat[:-1,:])

	#Output a set of points that do not have nans.
	selecti=(-np.isnan(alphaAtrue_m))&(-np.isnan(betaA_mat))&(-np.isnan(kA_m))
	o_data=np.vstack((mA_m[selecti], alpha0_m[selecti], alphaAtrue_m[selecti], alphaAcalc_m[selecti], betaA_mat[selecti], kA_m[selecti], pcen_m[selecti], phicen_m[selecti])).T

	#Save data.
	#np.savetxt(ofile, o_data, delimiter=', ', newline='\n', header=' Parameters: \n beta0=%e \n sp=%e \n sM=%e \n mtb=%e \n nt0=%e \n Gamma=%e \n kns=%e \n Columns: \n mA/solar_mass, alpha0, alphaA_true, alphaA_check, betaA, kA, pcen/Pa, phicen.' %(beta0, sp, sM, mtb, nt0, Gamma, kns))


