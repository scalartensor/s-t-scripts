#!/usr/bin/env python -u
import numpy as np
#import pylab as py
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as py
import os,sys
import COMMON as CM

#INPUT PARAMETERS:
run_number=6
#Files to create exclusion plots (the mass-mass plot will be produced only for the last file of the list).
#all_files=['1141_poster_chisq1_Kepler.npy', '1141_bhat_chisq1_Kepler.npy', '1738_FEA12par_chisq1.npy']
#all_files=['1141_bhat_chisq1.npy','1738_FEA12_chisq1.npy','1141_poster_chisq1.npy']
all_files=['1738_FEA12_chisq1.npy','1141_invariant_chisq1.npy']
#all_files=['1141_invariant_chisq1.npy']
colorvec=['#000000', '#2b83ba', '#d7191c']
#colorvec=['#2b83ba','#d7191c']

#Kepler=0 #Either 0 (to consider all files, disregarding Kepler's third law condition), or 1 (to consider '_Kepler.txt' files, to take into account Kepler's third law).
outputplot='../../../plots/EXCLUSION_PLOT.png' #Output file for plot 1.
outputplot2='../../../plots/MASS_MASS_PLOT.png' #Output file for plot 2.
chisq_lim=1
cluster=0 #Either 0 or 1 (when run in the cluster). Only relevant to load the FEA curves. 

mB_min_good=0.7 #ONLY RELEVANT FOR 1141. Values of mB below this will be neglected, since they lead to spurious points in the exclusion plot, and in any case they are below the mass function limit.

###########################
#Identify files to add to the plot:
#if Kepler==1:
#	all_files=np.sort([file for file in os.listdir(inputdir) if (file[-4:]=='.npy')&(file[-10:]=='Kepler.npy')&(int(file.split('chisq')[1].replace('.','_').split('_')[0])==chisq_lim) ])
#else:
#        all_files=np.sort([file for file in os.listdir(inputdir) if (file[-4:]=='.npy')&(file[-10:]!='Kepler.npy')&(int(file.split('chisq')[1].replace('.','_').split('_')[0])==chisq_lim) ])

############################

mA_bins=1000
mB_bins=1000
abs_alpha0_min=1e-4
abs_alpha0_max=1
beta0_min=-6.
beta0_max=6.
mB_min_good=0.7 #Values of mB below this will be neglected, since they lead to spurious points in the exclusion plot, and in any case they are below the mass function limit.

#################################

if cluster==1:
	FEA_file1='../FEA12_alpha0beta0_1141.txt'
	FEA_file2='../FEA12_alpha0beta0_1738.txt'
	inputdir='/lustre/projects/p002_swin/prosado/scalartensor/data/ANALYSIS/BOUNDARIES/RUN%.3i/ALL_CHISQ/' %run_number
	outputplot='../../plots/EXCLUSION_PLOT.png' #Output directory for plots.
else:
	FEA_file1='../../../data/FEA12_alpha0beta0_1141.txt'
	FEA_file2='../../../data/FEA12_alpha0beta0_1738.txt'
	inputdir='../../../data/ANALYSIS/BOUNDARIES/RUN%.3i/ALL_CHISQ/' %run_number
	outputplot='../../../plots/EXCLUSION_PLOT.png' #Output directory for plots.

FEA1=np.loadtxt(FEA_file1)
FEA2=np.loadtxt(FEA_file2)
#colorvec=['red', 'green', 'blue']
levels=np.array([0.5, 1.5])
py.figure(1)

#matplotlib.rcParams.update({'font.size': 22})
py.rcParams.update({'font.size': 18})
py.rcParams.update({'legend.fontsize': 11.})

for fili in xrange(len(all_files)):
	chisq_file=all_files[fili]
	data=np.load(inputdir+chisq_file)[()]
	pulsar=chisq_file.split('_')[0]

	beta0=data['beta0']
	alpha0=data['alpha0']
	chisq=data['chisq']
	mA=data['mA']
	mB=data['mB']
	alphaA=data['alphaA']

	if all_files[fili]=='1738_FEA12_chisq1.npy':
		#print 'For the sake of the plot in Viveks talk!!!'
		#alpha0=0.55*alpha0 ##########Get rid of this!!
		#label='1738 by 2012'
		label='PSR J1738+0333 from Freire et al. 2012'
	elif all_files[fili]=='1141_bhat_chisq1.npy':
		#label='1141 by 2008'
		label='PSR J1141-6545 from Bhat et al. 2008'
	elif all_files[fili]=='1141_poster_chisq1.npy':
		#label='1141 by 2016'
		label='PSR J1141-6545 - this work'
	elif all_files[fili]=='1141_invariant_chisq1.npy':
		label='PSR J1141-6545 invariant'

	mA_min=min(mA)
	mA_max=max(mA)
	mB_min=min(mB)
	mB_max=max(mB)

	mA_vec=np.linspace(mA_min, mA_max, mA_bins)
	mB_vec=np.linspace(mB_min, mB_max, mB_bins)
	mA_vec_m=0.5*(mA_vec[:-1]+mA_vec[1:])
	mB_vec_m=0.5*(mB_vec[:-1]+mB_vec[1:])

	beta0_vec=np.unique(beta0)
	#beta0_vec=np.linspace(min(beta0), max(beta0), 1000)
	beta0_vec_m=0.5*(beta0_vec[1:]+beta0_vec[:-1])

	abs_alpha0_vec=np.unique(abs(alpha0))
	#abs_alpha0_vec=np.logspace(np.log10(min(abs(alpha0))), np.log10(max(abs(alpha0))), len(beta0_vec))
	abs_alpha0_vec_m=0.5*(abs_alpha0_vec[1:]+abs_alpha0_vec[:-1])

	if pulsar=='1141': 
		sel=(mB>mB_min_good) #Impose chisq>0 because chisq is imposed negative with some problematic points.
	elif pulsar=='1738':
		sel=(mB>0.) #Impose chisq>0 because chisq is imposed negative with some problematic points.

	histi=np.histogram2d(beta0[sel], abs(alpha0[sel]), bins=[beta0_vec, abs_alpha0_vec])[0].T
	histi_lim=np.zeros(np.shape(histi))
	histi_lim[histi>0]=1
	B0, A0=np.meshgrid(beta0_vec_m, abs_alpha0_vec_m)
	abs_alpha0_lim=np.amax(A0*histi_lim, axis=0)

	B0, A0=np.meshgrid(beta0_vec_m, abs_alpha0_vec_m)
	histi_min=np.zeros(np.shape(histi))+100
	histi_max=np.zeros(np.shape(histi))-100
	histi_min[histi>0]=B0[histi>0]
	histi_max[histi>0]=B0[histi>0]

	beta0_lim_min=np.amin(histi_min, axis=1)
	beta0_lim_max=np.amax(histi_max, axis=1)

	#levels=np.array([0., 0.5, 1.5])

	#py.semilogy(beta0[sel], abs(alpha0[sel]), '.')

	#py.semilogy(beta0_lim_min, abs_alpha0_vec_m, color=colorvec[fili], alpha=0.5)
	#py.semilogy(beta0_lim_max, abs_alpha0_vec_m, color=colorvec[fili], alpha=0.5)

	#py.plot(beta0_vec_m, abs_alpha0_lim, '.',color=colorvec[fili], alpha=0.5)
	py.fill_between(beta0_vec_m, y1=abs_alpha0_lim, y2=abs_alpha0_min, color=colorvec[fili], alpha=0.8, linewidth=0.0, label=label)
	#py.fill_betweenx(abs_alpha0_vec_m,x1=beta0_lim_min, x2=beta0_lim_max, color=colorvec[fili], alpha=0.5, linewidth=0.0)
	#py.contourf(B0, A0, histi_lim, levels=levels, colors=colorvec[fili])
	#py.scatter(beta0[sel], abs(alpha0[sel]), c=colorvec[fili])
	


#py.plot(FEA1[:,0], FEA1[:,1], color='purple', linewidth=4, alpha=0.4)
#py.plot(FEA2[:,0], FEA2[:,1], color='blue', linewidth=4, alpha=0.4)
py.legend(loc='upper right')
py.yscale('log')
py.xlabel('$\\beta_0$')
py.ylabel('$|\\alpha_0|$')
py.xlim(beta0_min, beta0_max)
py.ylim(abs_alpha0_min, abs_alpha0_max)
py.savefig(outputplot, dpi=600)
#py.colorbar()
#py.show()

levels=np.array([0.5, 1.5])
colors=np.array(['red', 'white'])

histi_m=np.histogram2d(mA[sel], mB[sel], bins=[mA_vec, mB_vec])[0].T
#histi=np.histogram2d(beta0[sel], np.log10(abs(alpha0[sel])), bins=[beta0_vec, abs_alpha0_vec])[0].T
histi_m_lim=np.zeros(np.shape(histi_m))
histi_m_lim[histi_m>0]=1

MA,MB=np.meshgrid(mA_vec, mB_vec)
#Kepler_GR=CM.Kepler_condi(MA, MB, opd.x, opd.Pb, 0., 0.)
#mb_lim=np.amax(MB*(-Kepler_GR), axis=0)

def Kepler_condi2(mA, mB, x, Pb, ST_factor):
	return mB**3*CM.msun/((mA+mB)**2.)>4.*np.pi**2.*x**3./(Pb**2.*CM.grav*ST_factor)

#ST_factor=np.amax((1.+alphaA*alpha0))
#Kepler_ST=Kepler_condi2(MA, MB, opd.x, opd.Pb, ST_factor)
#mb_lim_ST=np.amax(MB*(-Kepler_ST), axis=0)

py.figure(2)
py.contourf(mA_vec_m, mB_vec_m, histi_m_lim, levels=levels, colors=colors)
#py.imshow(histi_m_lim, aspect='auto', origin='lower', interpolation='None', extent=[mA_min, mA_max, mB_min, mB_max], cmap='winter')
#py.plot(mA_vec, mb_lim, color='black')
#py.plot(mA_vec, mb_lim_ST, color='green')
py.xlabel('mA/solar mass')
py.ylabel('mB/solar mass')
#py.xlim(mA_min, mA_max)
#py.ylim(mB_min, mB_max)
py.xlim(1.1,1.5)
#py.ylim(0.17,0.18)
py.savefig(outputplot2, dpi=600)


