####################################
#Constants.
Gn = 6.673e-11 #Newtons gravitational constant.
c = 2.99792458e8 #Speed of light in vacuum.
msun = 1.9891e30 #Solar mass.
rsun = 6.955e8 #Solar radius.

####################################
#Fixed input parameters.
sp = 1e34 #Typical maximum pressure at the centre of the NS in Pa.
sM = 1e4 #Typical radius of the NS in m.

####################################
#Variable input parameters.
pbins = 400 #Number of bins in values of pressure (log spaced).
phibins = 400 #Number of bins in values of scalar field (log spaced).

def par_fun(beta0):
	'''Returns input parameters for the Mathematica calculation. They are chosen for different values of beta0 to cover as much as possible of the interesting region of baryonic mass and |alpha0|. "pNminT" and "pNmaxT" are the minimum and maximum values of pressure at the centre of the NS in rescaled units (so the real value is that multiplied by sp). "phiNminT" and "phiNmaxT" are the minimum and maximum values of the scalar field at the centre of the NS. "pNsurT" is the pressure at the surface of the NS (in Pa); in theory it should be zero. "rhoNcenT" is the radial coordinate at the centre (in rescaled units); in theory it should be zero.'''
	pNsurT=1e-15
	if beta0<-2.5:
		rhoNcenT=2e-3

		pNminT = 3e-2
		pNmaxT = 50
		phiNminT = 3e-5
		phiNmaxT = 2
	elif -2.5<=beta0<-1.4:
		rhoNcenT=2e-3

		pNminT = 3e-2
		pNmaxT = 50
		phiNminT = 5e-5
		phiNmaxT = 2
	elif -1.4<=beta0<-1.:
		rhoNcenT=2e-3

		pNminT = 1e-2
		pNmaxT = 50
		phiNminT = 4e-5
		phiNmaxT = 5
	elif -1.<=beta0<-0.5:
		rhoNcenT=2e-3
	
		pNminT = 2e-3
		pNmaxT = 50
		phiNminT = 4e-5
		phiNmaxT = 5
	elif -0.5<=beta0<-0.2:
		rhoNcenT=2e-3
	
		pNminT = 1e-4
		pNmaxT = 50
		phiNminT = 1e-4
		phiNmaxT = 10
	elif -0.2<=beta0<-0.05:
		rhoNcenT=2e-2
	
		pNminT = 1e-5
		pNmaxT = 50
		phiNminT = 5e-4
		phiNmaxT = 10
	elif -0.05<=beta0<-0.02:
		rhoNcenT=2e-2
	
		pNminT = 1e-6
		pNmaxT = 50
		phiNminT = 1e-3
		phiNmaxT = 50
	elif -0.02<=beta0<0.02:
		rhoNcenT=2e-2
	
		pNminT = 1e-5
		pNmaxT = 50
		phiNminT = 4e-3
		phiNmaxT = 500
	elif 0.02<=beta0<0.1:
		rhoNcenT=2e-2
	
		pNminT = 1e-5
		pNmaxT = 50
		phiNminT = 1e-3
		phiNmaxT = 50
	elif 0.1<=beta0<0.5:
		rhoNcenT=2e-3
	
		pNminT = 1e-2
		pNmaxT = 50
		phiNminT = 1e-4
		phiNmaxT = 30
	elif 0.5<=beta0<2.:
		rhoNcenT=2e-3
	
		pNminT = 3e-2
		pNmaxT = 50
		phiNminT = 2e-5
		phiNmaxT = 10
	elif 2.<=beta0<3.5:
		rhoNcenT=2e-3
	
		pNminT = 3e-2
		pNmaxT = 50
		phiNminT = 1e-5
		phiNmaxT = 5
	elif 3.5<=beta0:
		rhoNcenT=2e-3
	
		pNminT = 3e-2
		pNmaxT = 50
		phiNminT = 4e-6
		phiNmaxT = 4
	return pNsurT, rhoNcenT, pNminT, pNmaxT, phiNminT, phiNmaxT

####################################
#Model for the Equation of state of the neutron star.
#Polytrope (from DamourEspositoFarese1996).
mtb = 1.66e-27 #Baryonic mass (kg).
nt0 = 1e44 #Baryon number density (m^-3).
Gamma = 2.34 #Dimensionless constant.
kns = 0.0195 #Dimensionless constant.

####################################
#Model for the scalar tensor theory.
afun='A[\[Beta]0_, \[CurlyPhi]_] = Exp[1/2 \[Beta]0 \[CurlyPhi]^2]'
alpha='\[Alpha][\[Beta]0_, \[CurlyPhi]_] = \[Beta]0 \[CurlyPhi]'
beta0=-3
