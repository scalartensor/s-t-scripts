#!/usr/bin/env python -u
import numpy as np
import pylab as py
import scipy
import os, sys
import COMMON as CM

#INPUT PARAMETERS:
#inputdir='../../../data/ANALYSIS/BOUNDARIES/RUN%.3i/COMBINED/' %int(run_number)
inputfile='./TEMP/combfile.npy'

#Physical conditions.
mbA_min=0. #Minimum value of NS baryonic mass (in msun).
mbA_max=5. #Maximum value of NS baryonic mass (in msun).
Rns_min=1000. #Minimum value of NS radius (in m).

##############
#Load data.
#inputfiles=np.sort([file for file in os.listdir(inputdir) if file[0:5]=='comb_'])

#for fili in xrange(len(inputfiles)):
#ifile=inputdir+inputfiles[fili]
#d=CM.getdata(ifile) #Get data.
d=CM.getdata(inputfile) #Get data.

#################
aalpha0=abs(d.alpha0)
py.ion()
check=(d.mbA>mbA_min)&(d.Rns>Rns_min)&(d.mbA<mbA_max)
#sel=(aalpha0>aalpha0vec[indi])&(aalpha0<=aalpha0vec[indi+1])&check
sel=check
enersel_yes=(np.sign(d.phi0*d.phicen)>0)
enersel_no=(np.sign(d.phi0*d.phicen)<0)
py.plot(d.Rns[sel&enersel_no], d.mA[sel&enersel_no], '*', color='red', alpha=0.5)
py.plot(d.Rns[sel&enersel_yes], d.mA[sel&enersel_yes], '.', color='blue', alpha=0.5)
#py.title('mA. beta0=%.3f, |alpha0|=%.3e' %(d.beta0, aalpha0vec[indi]))
py.ylim(0., 3.5)
py.xlim(8000.,20000)
#py.pause(0.001)
raw_input('enter to exit')
