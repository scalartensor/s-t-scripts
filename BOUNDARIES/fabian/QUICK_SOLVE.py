#!/usr/bin/env python -u
import numpy as np
import pylab as py
import os, sys
import Eq_solver.DEFAULT_PARAMETERS as DEF
import COMMON as CM

#######################################################
#INPUT_PARAMETERS:
cluster=None #True if run in the cluster. Otherwise None.

numpoints=1e3 #Number of points to analyse.
pcen_min=1e31 #Minimum central pressure.
pcen_max=1e36 #Maximum central pressure.
phicen_min=1e-5 #Minimum central scalar field.
phicen_max=1e1 #Maximum central scalar field.
beta0=1e-6
run_dir='./TEMP/' #Directory where input and output files will be saved.

#Parameters for the new analysis.
mbA_min=0.5
mbA_max=3.5
mA_min=0.5
mA_max=3.5
mA_low=1.
mA_upp=3.
Rns_min=0.
abs_alpha0_min=1e-6
abs_alpha0_max=1.
abs_alpha0_bins=50
mbA_bins=500

#Define other parameters.
parfile='parfile.m' #Name of parameters file to create.
icfile='icfile.txt' #Name of initial conditions file to create.
combfile='combfile' #Name of 'combined' file (the output of the Mathematica outputfile converted into numpy file).
ofile='ofile.txt' #Name of output file.
npyfile='ofile' #Name of final output numpy file.

#######################################################

#Create data directory.
CM.create_or_replace(run_dir)

#Create parameters file.
CM.create_par_file(beta0=beta0, parfile=run_dir+parfile)

#Create initial conditions file.
P_vec=10.**(np.random.uniform(np.log10(pcen_min*1./DEF.sp), np.log10(pcen_max*1./DEF.sp), numpoints))
Phi_vec=10.**(np.random.uniform(np.log10(phicen_min), np.log10(phicen_max), numpoints))
input=np.vstack((P_vec,Phi_vec)).T
np.savetxt(run_dir+icfile, input, delimiter=',', newline='\n', header='pNcenT, phiNcenT')

#Solve with Mathematica.
print 'Solving...'
CM.eq_solver(run_dir+parfile, run_dir+icfile, run_dir+ofile, cluster=cluster)

#Clean Mathematica output.
try:
	raw_data=np.loadtxt(run_dir+ofile, dtype='str')
except ValueError:
	bad=run_dir+ofile
	corrected=run_dir+ofile[:-4]+'_corr.txt'
	delete_bad_lines="cat %s | awk '{if(NF==9) print $0}' > %s" %(bad, corrected)
	os.system(delete_bad_lines)
	raw_data=np.loadtxt(corrected, dtype='str')

#Create output numpy file
c0=(raw_data[:,8]!='Indeterminate') #This condition will eliminate "Indeterminate" solutions.
#Convert data to floats (first get rid of "Indeterminate" rows).
bad_rows=np.where(raw_data=='Indeterminate')[0]
raw_data[bad_rows,2:]='1e90'
#raw_data[raw_data=='Indeterminate']='1e90'
sols=raw_data.astype(float)
pcen=sols[:,0]*DEF.sp #Pressure at the centre of the NS in Pa.
phicen=sols[:,1] #Value of the scalar field at the centre of the NS.
Rns=sols[:,2] #Radius of the NS in m.
alphaA=sols[:,3] #Effective coupling (dimensionless).
phi0=sols[:,4] #Scalar field at infinity (dimensionless).
mA=sols[:,5] #Mass in solar masses.
mbA=sols[:,6] #Mass (baryonic) in solar masses.
IA=sols[:,7] #Moment of inertia in kg m^2.
alpha0=sols[:,8] #Matter coupling in spatial infinity (dimensionless).
dicti={'pcen':pcen, 'phicen':phicen, 'Rns':Rns, 'alphaA':alphaA, 'phi0':phi0, 'mA':mA, 'mbA':mbA, 'IA':IA, 'alpha0':alpha0, 'beta0':beta0}

np.save(run_dir+combfile, dicti)

exit()
#Apply new analysis.
print 'Applying new analysis...'

beta0, sp, sM, mtb, nt0, Gamma, kns, o_data=CM.analysis(dicti, mbA_min=mbA_min, mbA_max=mbA_max, mA_min=mA_min, mA_max=mA_max, Rns_min=Rns_min, abs_alpha0_min=abs_alpha0_min, abs_alpha0_max=abs_alpha0_max, mA_low=mA_low, mA_upp=mA_upp, abs_alpha0_bins=abs_alpha0_bins, mbA_bins=mbA_bins)

mA=o_data[:,0]
alpha0=o_data[:,1]
alphaA=o_data[:,2]
alphaA_check=o_data[:,3]
betaA=o_data[:,4]
kA=o_data[:,5]
pcen=o_data[:,6]
phicen=o_data[:,7]
mbA=o_data[:,8]
Rns=o_data[:,9]

output_dicti={'pcen':pcen, 'phicen':phicen, 'Rns':Rns, 'alphaA':alphaA, 'phi0':phi0, 'mA':mA, 'mbA':mbA, 'IA':IA, 'alpha0':alpha0, 'beta0':beta0, 'alphaA_check':alphaA_check, 'betaA':betaA, 'kA':kA}

np.save(run_dir+npyfile, output_dicti)

