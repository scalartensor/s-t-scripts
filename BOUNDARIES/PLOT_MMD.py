#!/usr/bin/env python -u
import numpy as np
import pylab as py
import os,sys
import COMMON as CM
from scipy.optimize import fmin
from USEFUL import time_estimate

parfilename=sys.argv[1]

mA_min=0.5
mA_max=2.5
mA_bins=1000
#mB_min=0.5
#mB_max=1.5
#mB_bins=10000

#Load pulsar data from par file name (only useful to test whether generalised Kepler's law is fulfilled).
opd=CM.get_pulsar_data(parfilename)

mA_vec=np.linspace(mA_min, mA_max, mA_bins)
#mB_vec=np.linspace(mB_min, mB_max, mB_bins)
mA_vec_m=0.5*(mA_vec[:-1]+mA_vec[1:])
#mB_vec_m=0.5*(mB_vec[:-1]+mB_vec[1:])

mB_vec=np.zeros(len(mA_vec))
chisq_vec=np.zeros(len(mA_vec))
mB_lim=np.zeros(len(mA_vec))

mB_Pbdot_mean=np.zeros(len(mA_vec))
mB_Pbdot_upp=np.zeros(len(mA_vec))
mB_Pbdot_low=np.zeros(len(mA_vec))

mB_gamma_mean=np.zeros(len(mA_vec))
mB_gamma_upp=np.zeros(len(mA_vec))
mB_gamma_low=np.zeros(len(mA_vec))

mB_omdot_mean=np.zeros(len(mA_vec))
mB_omdot_upp=np.zeros(len(mA_vec))
mB_omdot_low=np.zeros(len(mA_vec))

def Kepler_find(mB, mA, x, Pb, alphaA, alphaB):
	''''''
	return abs(mB**3*CM.msun/((mA+mB)**2.)-4.*np.pi**2.*x**3./(Pb**2.*CM.grav*(1.+alphaA*alphaB)))

def Pbdot_find(mB, Pbdot_obs, mma, Pb, ecc, alphaA, alphaB, betaA, betaB):
	''''''
	return abs(CM.Pbdot_all_f(mma, mB, Pb, ecc, alphaA, alphaB, betaA, betaB) - Pbdot_obs)

def gamma_find(mB, gamma_obs, mma, Pb, ecc, alphaA, alphaB, kA):
	''''''
	return abs(CM.gamma_f(mma, mB, Pb, ecc, alphaA, alphaB, kA)-gamma_obs)

t=time_estimate(len(mA_vec)) #A class that prints estimated computation time.

def omdot_find(mB, omdot_obs, mma, Pb, ecc, alphaA, alphaB, betaA, betaB):
	''''''
	return abs(CM.omdot_f(mma, mB, Pb, ecc, alphaA, alphaB, betaA, betaB)-omdot_obs)

for mAi in xrange(len(mA_vec)):
	t.display() #Shows the remaining computation time.
	t.increase() #Needed to calculate the remaining computation time.

	#args=(mA_vec[mAi], 0., 0., 0., 0., 0., opd.Pb, opd.ecc, opd.Pbdot+opd.Pbdot_add, opd.Pbdot_err+opd.Pbdot_add_err, opd.gamma, opd.gamma_err, opd.omdot, opd.omdot_err)
	args=(mA_vec[mAi], 0., 0., 0., 0., 0., opd.Pb, opd.ecc, opd.Pbdot-opd.Pbdot_add, opd.Pbdot_err+opd.Pbdot_add_err, opd.gamma, opd.gamma_err, opd.omdot, opd.omdot_err)

	mB_GR=CM.mB_GR_f(mA_vec[mAi], opd.Pb, opd.ecc, opd.omdot)
	sol=fmin(CM.chisq_masses, mB_GR, args=args, disp=False, full_output=True)

	mB_vec[mAi]=sol[0]
	chisq_vec[mAi]=sol[1]

	#Find maximum mB that fulfills Kepler's third law.
	args=(mA_vec[mAi], opd.x, opd.Pb, 0., 0.)

	sol=fmin(Kepler_find, mB_GR, args=args, disp=False, full_output=True)
	mB_lim[mAi]=sol[0]

	#Find Pbdot_mean contour.
	#args=(opd.Pbdot+opd.Pbdot_add, mA_vec[mAi], opd.Pb, opd.ecc, 0., 0., 0., 0.)
	args=(opd.Pbdot-opd.Pbdot_add, mA_vec[mAi], opd.Pb, opd.ecc, 0., 0., 0., 0.)
	sol=fmin(Pbdot_find, mB_GR, args=args, disp=False, full_output=True)
	mB_Pbdot_mean[mAi]=sol[0]

	#Find Pbdot_upp contour.
	args=(opd.Pbdot+opd.Pbdot_add+opd.Pbdot_err+opd.Pbdot_add_err, mA_vec[mAi], opd.Pb, opd.ecc, 0., 0., 0., 0.)
	sol=fmin(Pbdot_find, mB_GR, args=args, disp=False, full_output=True)
	mB_Pbdot_upp[mAi]=sol[0]

	#Find Pbdot_low contour.
	args=((opd.Pbdot+opd.Pbdot_add)-(opd.Pbdot_err+opd.Pbdot_add_err), mA_vec[mAi], opd.Pb, opd.ecc, 0., 0., 0., 0.)
	sol=fmin(Pbdot_find, mB_GR, args=args, disp=False, full_output=True)
	mB_Pbdot_low[mAi]=sol[0]

	#Find gamma_mean contour.
	args=((opd.gamma), mA_vec[mAi], opd.Pb, opd.ecc, 0., 0., 0.)
	sol=fmin(gamma_find, mB_GR, args=args, disp=False, full_output=True)
	mB_gamma_mean[mAi]=sol[0]

	#Find gamma_upp contour.
	args=((opd.gamma)+(opd.gamma_err), mA_vec[mAi], opd.Pb, opd.ecc, 0., 0., 0.)
	sol=fmin(gamma_find, mB_GR, args=args, disp=False, full_output=True)
	mB_gamma_upp[mAi]=sol[0]

	#Find gamma_low contour.
	args=((opd.gamma)-(opd.gamma_err), mA_vec[mAi], opd.Pb, opd.ecc, 0., 0., 0.)
	sol=fmin(gamma_find, mB_GR, args=args, disp=False, full_output=True)
	mB_gamma_low[mAi]=sol[0]

	#Find omdot_mean contour.
	args=((opd.omdot), mA_vec[mAi], opd.Pb, opd.ecc, 0., 0., 0., 0.)
	sol=fmin(omdot_find, mB_GR, args=args, disp=False, full_output=True)
	mB_omdot_mean[mAi]=sol[0]

	#Find omdot_upp contour.
	args=((opd.omdot)+(opd.omdot_err), mA_vec[mAi], opd.Pb, opd.ecc, 0., 0., 0., 0.)
	sol=fmin(omdot_find, mB_GR, args=args, disp=False, full_output=True)
	mB_omdot_upp[mAi]=sol[0]

	#Find omdot_low contour.
	args=((opd.omdot)-(opd.omdot_err), mA_vec[mAi], opd.Pb, opd.ecc, 0., 0., 0., 0.)
	sol=fmin(omdot_find, mB_GR, args=args, disp=False, full_output=True)
	mB_omdot_low[mAi]=sol[0]

print min(chisq_vec)

#py.ion()
#py.scatter(mA_vec, mB_vec, c=np.log10(chisq_vec), edgecolors=None)
py.scatter(mA_vec, mB_vec, c=np.log10(chisq_vec), lw=0)
py.plot(mA_vec, mB_lim, color='black')

py.plot(mA_vec, mB_Pbdot_mean, color='black', alpha=0.5)
py.plot(mA_vec, mB_Pbdot_upp, color='black')
py.plot(mA_vec, mB_Pbdot_low, color='black')

py.plot(mA_vec, mB_gamma_mean, color='black', alpha=0.5)
py.plot(mA_vec, mB_gamma_upp, color='black')
py.plot(mA_vec, mB_gamma_low, color='black')

py.plot(mA_vec, mB_omdot_mean, color='black', alpha=0.5)
py.plot(mA_vec, mB_omdot_upp, color='black')
py.plot(mA_vec, mB_omdot_low, color='black')

py.xlim(mA_min, mA_max)
py.ylim(0., 3.)
py.colorbar()
py.show()
#raw_input('enter')

