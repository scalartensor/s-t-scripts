#!/usr/bin/env python -u
import numpy as np
import pylab as py
import os,sys

#filename='/projects/p002_swin/vvenkatr/abc/glory_finished.txt'
filename=sys.argv[1]
delim=' '
#FEA_file1='../FEA12_alpha0beta0_1141.txt'
#FEA_file2='../FEA12_alpha0beta0_1738.txt'
FEA_file1='../../../data/FEA12_alpha0beta0_1141.txt'
FEA_file2='../../../data/FEA12_alpha0beta0_1738.txt'

#mA/solar_mass, alpha0, alphaA_true, alphaA_check, betaA, kA, pcen/Pa , phic,  beta0, percent, numpts, intersect.
#data=np.loadtxt(filename, delimiter=',')
data=np.loadtxt(filename, delimiter='%s' %str(delim))
FEA1=np.loadtxt(FEA_file1)
FEA2=np.loadtxt(FEA_file2)

mA=data[:,0]
alpha0=data[:,1]
beta0=data[:,8]
chisq=data[:,9]

sel=(chisq<=1.)

beta0_min=-6.
beta0_max=6.
beta0_bins=50
beta0_vec=np.linspace(beta0_min, beta0_max, beta0_bins)

abs_alpha0_min=1e-4
abs_alpha0_max=1.
abs_alpha0_bins=500
abs_alpha0_vec=np.logspace(np.log10(abs_alpha0_min), np.log10(abs_alpha0_max), abs_alpha0_bins)

histi=np.histogram2d(beta0[sel], abs(alpha0[sel]), bins=[beta0_vec, abs_alpha0_vec])[0].T

#histi[histi>0]=1.2
#levels=[0.,1.,2.]
levels=np.linspace(np.amin(histi), np.amax(histi), 10)

beta0_vec_m=0.5*(beta0_vec[1:]+beta0_vec[:-1])
abs_alpha0_vec_m=0.5*(abs_alpha0_vec[1:]+abs_alpha0_vec[:-1])

#py.ion()

py.figure(1)
#py.imshow(histi, aspect='auto', origin='lower')
py.contourf(beta0_vec_m, abs_alpha0_vec_m, histi, levels=levels)
py.plot(FEA1[:,0], FEA1[:,1], color='purple', linewidth=4, alpha=0.8)
py.plot(FEA2[:,0], FEA2[:,1], color='blue', linewidth=4, alpha=0.8)
py.yscale('log')
py.xlabel('beta0')
py.ylabel('|alpha0|')
py.colorbar()
py.show()
'''
py.figure(2)
py.cla()
sct=py.scatter(beta0[sel], abs(alpha0[sel]), s=chisq[sel]*50, c=chisq[sel], alpha=0.2)
#sct=py.scatter(beta0[sel&sel2], abs(alpha0[sel&sel2]), c=mA[sel&sel2])
#sct=py.scatter(beta0, abs(alpha0), c=chisq, s=30)
py.plot(FEA1[:,0], FEA1[:,1], color='purple', linewidth=4, alpha=0.5)
py.plot(FEA2[:,0], FEA2[:,1], color='blue', linewidth=4, alpha=0.5)
#py.colorbar(sct)
py.ylim(abs_alpha0_min, abs_alpha0_max)
py.yscale('log')
py.show()
raw_input('enter')
'''
