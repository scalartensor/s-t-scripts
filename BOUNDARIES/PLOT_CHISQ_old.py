#!/usr/bin/env python -u
import numpy as np
#import pylab as py
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as py
import os,sys
import COMMON as CM

#INPUT PARAMETERS:

inputdir=sys.argv[1] #Input directory with chi square files.
cluster=sys.argv[2] #Either 0 (if run in laptop) or 1 (if run in the cluster).
chisq_lim=eval(sys.argv[3]) #Chi square limit (usually 1).
parfilename=sys.argv[4] #Par file.
Kepler=sys.argv[5] #Either 0 (to consider all files, disregarding Kepler's third law condition), or 1 (to consider '_Kepler.txt' files, to take into account Kepler's third law).
outputdir=sys.argv[6] #Output directory for plots.

mA_bins=1000
mB_bins=1000
abs_alpha0_min=1e-4
abs_alpha0_max=1
beta0_min=-6.
beta0_max=6.
mB_min_good=0.7 #Values of mB below this will be neglected, since they lead to spurious points in the exclusion plot, and in any case they are below the mass function limit.

#################################

delim=' '
if cluster=='1':
	FEA_file1='../FEA12_alpha0beta0_1141.txt'
	FEA_file2='../FEA12_alpha0beta0_1738.txt'
else:
	FEA_file1='../../../data/FEA12_alpha0beta0_1141.txt'
	FEA_file2='../../../data/FEA12_alpha0beta0_1738.txt'

#mA/solar_mass, alpha0, alphaA_true, alphaA_check, betaA, kA, pcen/Pa , phic,  beta0, percent, numpts, intersect.
#data=np.loadtxt(filename, delimiter=',')
FEA1=np.loadtxt(FEA_file1)
FEA2=np.loadtxt(FEA_file2)

'''
beta0_min=-6.
beta0_max=6.
beta0_bins=50
beta0_vec=np.linspace(beta0_min, beta0_max, beta0_bins)

abs_alpha0_min=1e-4
abs_alpha0_max=1.
abs_alpha0_bins=1000
abs_alpha0_vec=np.logspace(np.log10(abs_alpha0_min), np.log10(abs_alpha0_max), abs_alpha0_bins)
'''
#Load pulsar data from par file name (only useful to test whether generalised Kepler's law is fulfilled).
opd=CM.get_pulsar_data(parfilename)

#Make list of chi square data files.
#all_files=np.sort([file for file in os.listdir(inputdir) if file[0:4]!='.DS_'])
if Kepler=='1':
	all_files=np.sort([file for file in os.listdir(inputdir) if (file[0:5]=='chisq')&(file[-10:]=='Kepler.txt') ])
else:
	all_files=np.sort([file for file in os.listdir(inputdir) if (file[0:5]=='chisq')&(file[-10:]!='Kepler.txt') ])

beta0=[]
alpha0=[]
chisq=[]
mA=[]
mB=[]
alphaA=[]

for fili in xrange(len(all_files)):
	#data=np.loadtxt(inputdir+all_files[fili], delimiter='%s' %str(delim), usecols=(0,1,2))
	data=np.genfromtxt(inputdir+all_files[fili])
	if len(np.shape(data))==2: #To avoid loading files that are not yet populated.
		beta0.extend(data[:,0])
		alpha0.extend(data[:,1])
		chisq.extend(data[:,2])
		mA.extend(data[:,3])
		mB.extend(data[:,4])
		alphaA.extend(data[:,5])

beta0=np.array(beta0)
alpha0=np.array(alpha0)
chisq=np.array(chisq)
mA=np.array(mA)
mB=np.array(mB)
alphaA=np.array(alphaA)

mA_min=min(mA)
mA_max=max(mA)
mB_min=min(mB)
mB_max=max(mB)

mA_vec=np.linspace(mA_min, mA_max, mA_bins)
mB_vec=np.linspace(mB_min, mB_max, mB_bins)
mA_vec_m=0.5*(mA_vec[:-1]+mA_vec[1:])
mB_vec_m=0.5*(mB_vec[:-1]+mB_vec[1:])

beta0_vec=np.unique(beta0)
#beta0_min=min(beta0_vec)
#beta0_max=max(beta0_vec)
beta0_vec_m=0.5*(beta0_vec[1:]+beta0_vec[:-1])

abs_alpha0_vec=np.unique(abs(alpha0))
#abs_alpha0_vec=np.unique(np.log10(abs(alpha0)))
#abs_alpha0_min=min(abs_alpha0_vec)
#abs_alpha0_max=max(abs_alpha0_vec)
abs_alpha0_vec_m=0.5*(abs_alpha0_vec[1:]+abs_alpha0_vec[:-1])

#sel=(chisq<=chisq_lim)&(chisq>0.) #Impose chisq>0 because chisq is imposed negative with some problematic points.
#print 'The single points around the exclusion plot can be subtracted by imposing mB>0.7. That is a safe condition, given that all points below that mass do not fulfill the generalised Keplers Third Law (they are below the mass function limit).'

pulsar=opd.PSRJ[0:4]
if pulsar=='1141': 
	sel=(chisq<=chisq_lim)&(chisq>0.)&(mB>mB_min_good) #Impose chisq>0 because chisq is imposed negative with some problematic points.
elif pulsar=='1738':
	sel=(chisq<=chisq_lim)&(chisq>0.) #Impose chisq>0 because chisq is imposed negative with some problematic points.

histi=np.histogram2d(beta0[sel], abs(alpha0[sel]), bins=[beta0_vec, abs_alpha0_vec])[0].T
#histi=np.histogram2d(beta0[sel], np.log10(abs(alpha0[sel])), bins=[beta0_vec, abs_alpha0_vec])[0].T
histi_lim=np.zeros(np.shape(histi))
histi_lim[histi>0]=1

B0, A0=np.meshgrid(beta0_vec_m, abs_alpha0_vec_m)
abs_alpha0_lim=np.amax(A0*histi_lim, axis=0)

#levels=np.array([0., 0.5, 1.5])
levels=np.array([0.5, 1.5])
colors=np.array(['red', 'white'])

#py.ion()
py.semilogy(beta0[sel], abs(alpha0[sel]), '.')

oplot1='/fig1.png'
py.figure(1)
#py.imshow(histi_lim, aspect='auto', origin='lower', interpolation='None')
#py.contourf(beta0_vec_m, abs_alpha0_vec_m, histi_lim, levels=levels)
#py.contourf(beta0_vec_m, abs_alpha0_vec_m, histi_lim, levels=levels, colors=colors)
py.plot(beta0_vec_m, abs_alpha0_lim, color='blue')
py.fill_between(beta0_vec_m, y1=abs_alpha0_lim, y2=abs_alpha0_min, color='blue', alpha=0.5)
py.plot(FEA1[:,0], FEA1[:,1], color='purple', linewidth=4, alpha=0.8)
py.plot(FEA2[:,0], FEA2[:,1], color='blue', linewidth=4, alpha=0.8)
py.yscale('log')
py.xlabel('beta0')
py.ylabel('|alpha0|')
py.xlim(beta0_min, beta0_max)
py.ylim(abs_alpha0_min, abs_alpha0_max)
py.savefig(outputdir+oplot1, dpi=600)
#py.colorbar()
#py.show()

print 'Plot saved in %s' %(outputdir+oplot1)

histi_m=np.histogram2d(mA[sel], mB[sel], bins=[mA_vec, mB_vec])[0].T
#histi=np.histogram2d(beta0[sel], np.log10(abs(alpha0[sel])), bins=[beta0_vec, abs_alpha0_vec])[0].T
histi_m_lim=np.zeros(np.shape(histi_m))
histi_m_lim[histi_m>0]=1

MA,MB=np.meshgrid(mA_vec, mB_vec)
Kepler_GR=CM.Kepler_condi(MA, MB, opd.x, opd.Pb, 0., 0.)
mb_lim=np.amax(MB*(-Kepler_GR), axis=0)

def Kepler_condi2(mA, mB, x, Pb, ST_factor):
	return mB**3*CM.msun/((mA+mB)**2.)>4.*np.pi**2.*x**3./(Pb**2.*CM.grav*ST_factor)

ST_factor=np.amax((1.+alphaA*alpha0))
Kepler_ST=Kepler_condi2(MA, MB, opd.x, opd.Pb, ST_factor)
mb_lim_ST=np.amax(MB*(-Kepler_ST), axis=0)

oplot2='/fig2.png'
py.figure(2)
py.contourf(mA_vec_m, mB_vec_m, histi_m_lim, levels=levels, colors=colors)
#py.imshow(histi_m_lim, aspect='auto', origin='lower', interpolation='None', extent=[mA_min, mA_max, mB_min, mB_max], cmap='winter')
py.plot(mA_vec, mb_lim, color='black')
py.plot(mA_vec, mb_lim_ST, color='green')
py.xlabel('mA/solar mass')
py.ylabel('mB/solar mass')
py.xlim(mA_min, mA_max)
py.ylim(mB_min, mB_max)
py.savefig(outputdir+oplot2, dpi=600)

print 'Plot saved in %s' %(outputdir+oplot2)

raw_input('enter')
exit()
#Kepler_mat=CM.Kepler_condi(mA[sel], mB[sel], opd.x, opd.Pb, alphaA[sel], alpha0[sel])

sel=(chisq<=chisq_lim)&(chisq>0.)&CM.Kepler_condi(mA, mB, opd.x, opd.Pb, alphaA, alpha0) #Impose chisq>0 because chisq is imposed negative with some problematic points.

histi=np.histogram2d(beta0[sel], abs(alpha0[sel]), bins=[beta0_vec, abs_alpha0_vec])[0].T
#histi=np.histogram2d(beta0[sel], np.log10(abs(alpha0[sel])), bins=[beta0_vec, abs_alpha0_vec])[0].T
histi_lim=np.zeros(np.shape(histi))
histi_lim[histi>0]=1

histi_m=np.histogram2d(mA[sel], mB[sel], bins=[mA_vec, mB_vec])[0].T
#histi=np.histogram2d(beta0[sel], np.log10(abs(alpha0[sel])), bins=[beta0_vec, abs_alpha0_vec])[0].T
histi_m_lim=np.zeros(np.shape(histi_m))
histi_m_lim[histi_m>0]=1

py.figure(3)
py.contourf(beta0_vec_m, abs_alpha0_vec_m, histi_lim, levels=levels, colors=colors)
py.plot(FEA1[:,0], FEA1[:,1], color='purple', linewidth=4, alpha=0.8)
py.plot(FEA2[:,0], FEA2[:,1], color='blue', linewidth=4, alpha=0.8)
py.yscale('log')
py.xlabel('beta0')
py.ylabel('|alpha0|')
py.xlim(beta0_min, beta0_max)
py.ylim(abs_alpha0_min, abs_alpha0_max)
#py.colorbar()
#py.show()

py.figure(4)
py.contourf(mA_vec_m, mB_vec_m, histi_m_lim, levels=levels)
#py.imshow(histi_m_lim, aspect='auto', origin='lower', interpolation='None', extent=[mA_min, mA_max, mB_min, mB_max], cmap='winter')
py.plot(mA_vec, mb_lim, '.', color='black')
py.xlabel('mA/solar mass')
py.ylabel('mB/solar mass')
py.xlim(mA_min, mA_max)
py.ylim(mB_min, mB_max)
#py.colorbar()
#py.show()

raw_input('enter')
