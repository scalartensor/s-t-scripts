#!/usr/local/python-2.7.2/bin/python -u
import numpy as np
import pylab as py
import os, sys
from scipy import interpolate as ip
from time import sleep
import COMMON as CM
import Eq_solver.DEFAULT_PARAMETERS as DEF
from USEFUL import time_estimate

##############################################
#INPUT PARAMETERS:
run_number=6
pulsar=sys.argv[1] #Either 0 (for a broad range of masses), or 1141, or 1738, or 1141_less (which is a test), ...

if pulsar=='0':
	mA_low=1.
	mA_upp=2.
	mbA_bins=5000
	dirtag=''
elif pulsar=='1141':
	mA_low=1.1
	mA_upp=1.5
	mbA_bins=4000
	dirtag='_1141'
elif pulsar=='1738':
	mA_low=1.3
	mA_upp=1.6
	mbA_bins=4000 #In runs 4 and 5 I used 1000, and the result was worse than FreireEtAl2012; maybe because of too few bins. Hence I increase it.
	dirtag='_1738'
elif pulsar=='1141_less':
	mA_low=1.3
	mA_upp=1.35
	mbA_bins=200
	dirtag='_1141_less'
elif pulsar=='1738_less':
	mA_low=1.3
	mA_upp=1.6
	mbA_bins=200
	dirtag='_1738_less'
elif pulsar=='1141_zoom':
	mA_low=1.3
	mA_upp=1.35
	mbA_bins=1000
	dirtag='_1141_zoom'

##############################################

run_dir='/lustre/projects/p002_swin/prosado/scalartensor/data/ANALYSIS/BOUNDARIES/RUN%.3i/COMBINED/' %int(run_number)
qsub_dir='/home/prosado/projects/scalartensor/scalar-tensor-scripts/ANALYSIS/BOUNDARIES/G_qsub_ANALYSIS_RUN%.3i%s/'  %(int(run_number), dirtag)
outputdir='/lustre/projects/p002_swin/prosado/scalartensor/data/ANALYSIS/BOUNDARIES/RUN%.3i/ANALYSIS%s/' %(int(run_number), dirtag)

##############################################

#Create or replace output directory.
CM.create_or_replace(qsub_dir, cluster=True)
CM.create_or_replace(outputdir, cluster=True)

#Locate files.
inputfiles=np.sort([file for file in os.listdir(run_dir) if file[0:5]=='comb_'])

#Analyse files.

for job_i in xrange(len(inputfiles)):
	gfilename=qsub_dir+'job_%.3i.gstar' %(job_i+1)

	#Content of the gstar file:
	filetext=['#!/bin/csh \n\
#PBS -q sstar \n\
#PBS -l nodes=1:ppn=1 \n\
#PBS -l pmem=500mb \n\
#PBS -l walltime=01:00:00:00 \n\
#PBS -N %sO_job_%s \n\
#PBS -o %sO_output_%s \n\
#PBS -e %sO_error_%s \n\
\n\
echo Deploying job to CPUs ... \n\
cat $PBS_NODEFILE \n \n ' %(qsub_dir, job_i+1, qsub_dir, job_i+1, qsub_dir, job_i+1) ]
	filetext2=['python /home/prosado/projects/scalartensor/scalar-tensor-scripts/ANALYSIS/BOUNDARIES/G_ANALYSIS.py %s %s %i %e %e %i' %(run_dir, outputdir, job_i, mA_low, mA_upp, mbA_bins)]

	np.savetxt(gfilename, filetext+filetext2, fmt='%s', newline='\n')

	task='qsub '+gfilename
	print task
	os.system(task)

