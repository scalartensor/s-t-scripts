#!/usr/local/python-2.7.2/bin/python -u
import numpy as np
import pylab as py
import os
import COMMON as CM
import Eq_solver.DEFAULT_PARAMETERS as DEF
from USEFUL import time_estimate
from RUNS import run_fun

###########################
#INPUT PARAMETERS:
run_number=4

###########################
inputdir='/lustre/projects/p002_swin/prosado/scalartensor/data/ANALYSIS/BOUNDARIES/RUN%.3i/RAW/' %int(run_number)
outputdir='/lustre/projects/p002_swin/prosado/scalartensor/data/ANALYSIS/BOUNDARIES/RUN%.3i/COMBINED/' %int(run_number)

unused, unused, unused, beta0_min, beta0_max, beta0_bins=run_fun(run_number)

beta0vec=np.linspace(beta0_min, beta0_max, beta0_bins)

###########################
#Create output folder if not existing.
CM.create_or_replace(outputdir, cluster=True)
#if not os.path.isdir(outputdir): os.makedirs(outputdir)

##############
#Load data (solutions of the equations by Mathematica).
t=time_estimate(len(beta0vec)) #A class that prints estimated computation time.

for beta0_i in xrange(len(beta0vec)):
	t.display() #Shows the remaining computation time.
	t.increase() #Needed to calculate the remaining computation time.

	npyfile='comb_%.3i' %beta0_i

	beta0_in=beta0vec[beta0_i]

	beta0, ofile_sel, split_bins=CM.files_to_analyse(beta0_in, inputdir)

	check=abs(beta0-beta0_in)*100./beta0_in
	if check>1.:
		print 'This value of beta0 could not be found. Redefine the vector of beta0 values.'
		exit()

	if len(ofile_sel)<split_bins:
		print 'There are still ofiles to be analysed!'
		exit()

	#Load data from all these ofiles together.
	###################
	#print ofile_sel[0]
	#raw_data=np.loadtxt(inputdir+ofile_sel[0], dtype='str')
	#raw_data[raw_data=='Indeterminate']='1e90'
	#sols=raw_data.astype(float)
	#print sols[:,8]*1./sols[:,4]
	#print
	####################

	#raw_data=np.array([[],[],[],[],[],[],[],[],[]]).T

	initial_files=inputdir+'ofile_%.3i_*' %beta0_i
	tempfile=inputdir+'temp.txt'
	correct_files="cat %s | awk '{if(NF==9) print $0}' | grep -v 'I' > %s" %(initial_files, tempfile) #Remove bad lines and "Indeterminate" solutions (or solutions with imaginary part, "I").
	os.system(correct_files)

	#for file in ofile_sel:
	#	print file
	#	tempfile=inputdir+'temp.txt'
	#	delete_bad_lines="cat %s | awk '{if(NF==9) print $0}' | grep -v 'I' > %s" %(bad, tempfile)
	#	os.system(delete_bad_lines)
	#	new_data=np.loadtxt(tempfile, dtype='float')

	#raw_data=np.array([[],[],[],[],[],[],[],[],[]]).T
	#for file in ofile_sel:
	#	try:
	#		new_data=np.loadtxt(inputdir+file, dtype='str')
	#	except ValueError:
	#		bad=inputdir+file
	#		corrected=inputdir+file[:-4]+'_corr.txt'
	#		delete_bad_lines="cat %s | awk '{if(NF==9) print $0}' > %s" %(bad, corrected)
	#		os.system(delete_bad_lines)
	#		new_data=np.loadtxt(corrected, dtype='str')
	#	raw_data=np.vstack((raw_data, new_data))

	#try:
	#	raw_data=np.loadtxt(inputdir+ofile_sel[0], dtype='str')
	#except ValueError:
	#	#print
	#	#print 'Bad line found in file %s.' %ofile_sel[0]
	#	bad=inputdir+ofile_sel[0]
	#	corrected=inputdir+ofile_sel[0][:-4]+'_corr.txt'
	#	delete_bad_lines="cat %s | awk '{if(NF==9) print $0}' > %s" %(bad, corrected)
	#	os.system(delete_bad_lines)
	#	raw_data=np.loadtxt(corrected, dtype='str')
	#for file in ofile_sel[1:]:
	#	print file##################
	#	try:
	#		new_data=np.loadtxt(inputdir+file, dtype='str')
	#	except ValueError:
	#		#print
	#		#print 'Bad line found in file %s.' %file
	#		bad=inputdir+file
	#		corrected=inputdir+file[:-4]+'_corr.txt'
	#		delete_bad_lines="cat %s | awk '{if(NF==9) print $0}' > %s" %(bad, corrected)
	#		os.system(delete_bad_lines)
	#		new_data=np.loadtxt(corrected, dtype='str')
	#	raw_data=np.vstack((raw_data, new_data))

	#c0=(raw_data[:,8]!='Indeterminate') #This condition will eliminate "Indeterminate" solutions.
	#Convert data to floats (first get rid of "Indeterminate" rows).
	#bad_rows=np.where(raw_data=='Indeterminate')[0]
	#raw_data[bad_rows,2:]='1e90'
	#raw_data[raw_data=='Indeterminate']='1e90'
	#sols=raw_data.astype(float)
	sols=np.loadtxt(tempfile, dtype='float')
	pcen=sols[:,0]*DEF.sp #Pressure at the centre of the NS in Pa.
	phicen=sols[:,1] #Value of the scalar field at the centre of the NS.
	Rns=sols[:,2] #Radius of the NS in m.
	alphaA=sols[:,3] #Effective coupling (dimensionless).
	phi0=sols[:,4] #Scalar field at infinity (dimensionless).
	mA=sols[:,5] #Mass in solar masses.
	mbA=sols[:,6] #Mass (baryonic) in solar masses.
	IA=sols[:,7] #Moment of inertia in kg m^2.
	alpha0=sols[:,8] #Matter coupling in spatial infinity (dimensionless).

	dicti={'pcen':pcen, 'phicen':phicen, 'Rns':Rns, 'alphaA':alphaA, 'phi0':phi0, 'mA':mA, 'mbA':mbA, 'IA':IA, 'alpha0':alpha0, 'beta0':beta0}

	np.save(outputdir+npyfile, dicti)
	remove_tempfile="rm %s" %tempfile
	os.system(remove_tempfile)
