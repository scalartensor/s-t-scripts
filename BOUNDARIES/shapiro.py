#!/usr/bin/env python -u
import pylab as py
import numpy as np
import sys
from USEFUL import time_estimate
#Pulsar name:
pul='1141'
#pul='1141_bhat'

#########################

G=6.673e-11
c=299792458.0
msun=1.9891e+30

def deltasb(m2, sini, ecc, omega, E):
	r=G*m2*msun*1./(c**3.)
	s=sini
	return -2.*r*np.log(1.-ecc*np.cos(E)-s*(np.sin(omega)*(np.cos(E)-ecc)+np.sqrt(1.-ecc**2.)*np.cos(omega)*np.sin(E)))

if pul=='1141':
	m2=0.97
	sini=0.998
	ecc=0.17
elif pul=='1141_bhat':
	m2=1.01
	sini=0.951
	ecc=0.17
elif pul=='1909':
	m2=0.2
	sini=0.99822
	ecc=1.35e-7
elif pul=='1738':
	m2=0.181
	sini=0.537
	ecc=1e-7
elif pul=='1640':
	m2=0.15
	sini=0.99
	ecc=0.0007972
elif pul=='0737':
	rquoted=6.21e-6
	m2=rquoted/(G*msun)*c**3.
	sini=0.99974
	ecc=0.08777
elif pul=='1534':
	m2=1.35
	sini=0.9772
	ecc=0.2736
elif pul=='1756':
	m2=1.23
	sini=0.93
	ecc=0.18

omega=np.linspace(0., np.pi, 1000)
Evec=np.linspace(0., np.pi, 1000)
Emat, omegamat=np.meshgrid(Evec, omega)

deltasbvec=deltasb(m2, sini, ecc, omegamat, Emat)

print 'Maximum Shapiro time delay with the given parameters: %.3f microseconds.' %(np.max(deltasbvec)*1e6)

m2vec=np.linspace(0.5, 1.5, 100)
maxdsb=np.zeros(len(m2vec))
for mi in xrange(len(m2vec)):
	deltasbvec=deltasb(m2vec[mi], 1, ecc, omegamat, Emat)
	maxdsb[mi]=np.max(deltasbvec)*1e6
py.ion()
py.figure(1)
py.xlabel('m2/msun')
py.ylabel('Shapiro time delay / microseconds')
py.plot(m2vec,maxdsb, '.')
py.title('ecc=%.3f, sini=1' %(ecc))
#py.show()

maxdsb=np.zeros(len(m2vec))
for mi in xrange(len(m2vec)):
	deltasbvec=deltasb(m2vec[mi], sini, ecc, omegamat, Emat)
	maxdsb[mi]=np.max(deltasbvec)*1e6
py.figure(2)
py.plot(m2vec, maxdsb, '.')
py.xlabel('m2/msun')
py.ylabel('Shapiro time delay / microseconds')
py.title('ecc=%.3f, sini=%.3f' %(ecc, sini))

raw_input('enter to exit')
exit()

min_m2=0.8
max_m2=1.2
max_delay=120e-6

#m2vec=np.linspace(min_m2, max_m2, 100)
#sinivec=np.linspace(0.,1.,100)

m2vec=np.random.uniform(min_m2, max_m2, 100)
sinivec=np.linspace(0.,1.,100)
consistent=[]

t=time_estimate(len(m2vec)*len(sinivec)) #A class that prints estimated computation time.

py.figure(3)
for mi in xrange(len(m2vec)):
	for si in xrange(len(sinivec)):
		t.display() #Shows the remaining computation time.
		t.increase() #Needed to calculate the remaining computation time.

		deltasbvec=deltasb(m2vec[mi], sinivec[si], ecc, omegamat, Emat)
		if np.max(deltasbvec)<max_delay:
			consistent.append([m2vec[mi], sinivec[si]])
			py.plot(m2vec[mi], sinivec[si], '.')
			py.pause(0.0000000001)

#Consistent with observations:
#py.figure(3)
#py.plot(consistent[:,0], consistent[:,1], '.')
#raw_input()
'''
sinivec=np.linspace(0.,1.,100)
maxdsb=np.zeros(len(sinivec))
for mi in xrange(len(sinivec)):
	deltasbvec=deltasb(m2, sinivec[mi], ecc, omegamat, Emat)
	maxdsb[mi]=np.max(deltasbvec)*1e6
py.plot(sinivec,maxdsb, '.')
py.show()
'''