def run_fun(run_number):
	'''Every mathematica job should have approx. 10000 points, i.e. pbins x phibins should not exceed 10000. Each cluster job should have approx. 100 calculations, i.e. split_bins*beta0_bins/numjobs should not exceed 100; if it does, increase numjobs.'''
	if run_number==2:
		split_bins=100 #The vector of central pressure will be splitted this many times.
		pbins=20 #Number of values of central pressure in each splitted file.
		phibins=500 #Number of values of central scalar field in each splitted file.
		beta0_min=-6. #Minimum value of beta0.
		beta0_max=6. #Maximum value of beta0.
		beta0_bins=50 #Number of values of beta0 linearly spaced.
	elif run_number==3:
		split_bins=100 #The vector of central pressure will be splitted this many times.
		pbins=20 #Number of values of central pressure in each splitted file.
		phibins=500 #Number of values of central scalar field in each splitted file.
		beta0_min=-1.34 #Minimum value of beta0.
		beta0_max=-1.10 #Maximum value of beta0.
		beta0_bins=50 #Number of values of beta0 linearly spaced.
	elif run_number==4:
		split_bins=400 #The vector of central pressure will be splitted this many times.
		pbins=5 #Number of values of central pressure in each splitted file.
		phibins=2000 #Number of values of central scalar field in each splitted file.
		beta0_min=-6. #Minimum value of beta0.
		beta0_max=6. #Maximum value of beta0.
		beta0_bins=100 #Number of values of beta0 linearly spaced.
	elif run_number==5:
		split_bins=500 #The vector of central pressure will be splitted this many times.
		pbins=5 #Number of values of central pressure in each splitted file.
		phibins=2500 #Number of values of central scalar field in each splitted file.
		beta0_min=-6. #Minimum value of beta0.
		beta0_max=6. #Maximum value of beta0.
		beta0_bins=200 #Number of values of beta0 linearly spaced.
	elif run_number==6:
		split_bins=500 #The vector of central pressure will be splitted this many times.
		pbins=5 #Number of values of central pressure in each splitted file.
		phibins=2500 #Number of values of central scalar field in each splitted file.
		beta0_min=-6. #Minimum value of beta0.
		beta0_max=6. #Maximum value of beta0.
		beta0_bins=200 #Number of values of beta0 linearly spaced.
	return split_bins, pbins, phibins, beta0_min, beta0_max, beta0_bins
