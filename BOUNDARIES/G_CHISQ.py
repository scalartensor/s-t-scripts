#!/usr/bin/env python -u
import numpy as np
import pylab as py
import os, sys
import COMMON as CM
from scipy import interpolate as ip
from scipy.interpolate import LinearNDInterpolator as LNDI
from USEFUL import time_estimate

#INPUT PARAMETERS:
inputfile=sys.argv[1] #Directory of scalar tensor data.
odir=sys.argv[2] #Output directory.
parfile=sys.argv[3] #Path to pulsar par file.

###########################################

#Observable pulsar data.
opd=CM.get_pulsar_data(parfile)

#Load scalar tensor data.
std=CM.getdata(inputfile)

#Output chi square data to output file.
#filenum=inputfile.split('/')[-1].split('_')[1].split('.txt')[0]
#outputfile=odir+'chisq_%s.txt' %filenum
outputfile=odir+inputfile.split('/')[-1].replace('analysis_', 'chisq_')

#CM.chisq_analysis(std, opd, outputfile)
psrj=opd.PSRJ[0:4]

if psrj=='1141':
        CM.chisq_analysis(std_split, opd, outputfile)
elif psrj=='1738':
        CM.chisq_analysis_1738(std_split, opd, outputfile)
