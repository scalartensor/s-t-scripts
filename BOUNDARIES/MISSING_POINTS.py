#!/usr/bin/env python -u
import numpy as np
import pylab as py
import os
from scipy import interpolate as ip
from scipy.interpolate import LinearNDInterpolator as LNDI
from time import sleep
import COMMON as CM
from scipy.ndimage import gaussian_filter
import Eq_solver.DEFAULT_PARAMETERS as DEF
from USEFUL import time_estimate

###############################
#INPUT PARAMETERS:
inputdir='../../../data/ANALYSIS/BOUNDARIES/RUN002/NEW_ANALYSIS_B/'

ploti=False #To plot histograms of missing points.
###############################

listfiles=np.sort([file for file in os.listdir(inputdir) if file[0:4]=='miss'])

if ploti:
	py.ion()
	levels=[0.,1.,2.]


for fili in xrange(len(listfiles)):
	data=np.load(inputdir+listfiles[fili])[()]
	phicen=data['phicen_vec']
	pcen=data['pcen_vec']
	beta0=data['beta0']
	histi=data['hist']
	
	histi[histi>0]=1.2
	
	pNsurT, rhoNcenT, pNminT, pNmaxT, phiNminT, phiNmaxT= DEF.par_fun(beta0)
	pcen_min=pNminT*DEF.sp
	pcen_max=pNmaxT*DEF.sp
	phicen_min=phiNminT
	phicen_max=phiNmaxT
	
	pcen_m=0.5*(pcen[1:]+pcen[:-1])
	phicen_m=0.5*(phicen[1:]+phicen[:-1])

	if ploti:
		py.figure(1)
		py.cla()
		py.contourf(pcen_m, phicen_m, histi, levels=levels, interpolation='none')
		if fili==0:
			py.colorbar()
		py.xlim(pcen_min, pcen_max)
		py.ylim(phicen_min, phicen_max)
		py.xlabel('Central pressure / Pa')
		py.ylabel('Central scalar field')
		py.title('Beta0=%.3f' %beta0)
		py.xscale('log')
		py.yscale('log')

	numadded=100000 #Add this number of points.
	numpix=len(histi[histi>0.])
	ppp=int(numadded*1./numpix)
	left_points=numadded-ppp*numpix

	unif_p=np.random.uniform(0., 1., numadded)
	unif_phi=np.random.uniform(0., 1., numadded)

	P,PHI=np.meshgrid(pcen,phicen)

	P_pop=P[:-1,:-1][histi!=0.]
	P_dif=P[1:,1:][histi!=0.]-P[:-1,:-1][histi!=0.]

	P_pop_rep=np.repeat(P_pop, ppp)
	P_pop_rep=np.hstack((P_pop_rep,P_pop[0:left_points]))

	P_dif_rep=np.repeat(P_dif, ppp)
	P_dif_rep=np.hstack((P_dif_rep, P_dif[0:left_points]))

	p_added=(unif_p*P_dif_rep)+P_pop_rep

	PHI_pop=PHI[:-1,:-1][histi!=0.]
	PHI_dif=PHI[1:,1:][histi!=0.]-PHI[:-1,:-1][histi!=0.]

	PHI_pop_rep=np.repeat(PHI_pop, ppp)
	PHI_pop_rep=np.hstack((PHI_pop_rep,PHI_pop[0:left_points]))

	PHI_dif_rep=np.repeat(PHI_dif, ppp)
	PHI_dif_rep=np.hstack((PHI_dif_rep, PHI_dif[0:left_points]))

	phi_added=(unif_phi*PHI_dif_rep)+PHI_pop_rep

	if ploti:
		py.figure(2)
		py.cla()
		histi2=np.histogram2d(p_added, phi_added, bins=[pcen, phicen])[0].T
		histi2[histi2>0]=1.2
		py.contourf(pcen_m, phicen_m, histi2, levels=levels, interpolation='none')
		if fili==0:
			py.colorbar()
		py.xlim(pcen_min, pcen_max)
		py.ylim(phicen_min, phicen_max)
		py.xlabel('Central pressure / Pa')
		py.ylabel('Central scalar field')
		py.title('Beta0=%.3f' %beta0)
		py.xscale('log')
		py.yscale('log')
		py.pause(0.0001)


