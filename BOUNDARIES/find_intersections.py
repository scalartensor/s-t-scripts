#!/usr/bin/env python -u
import numpy as np
import pylab as py
import os
import COMMON as CM
from scipy import interpolate as ip
from scipy.interpolate import LinearNDInterpolator as LNDI
from USEFUL import time_estimate

#Input parameters.
inputdir='../../../data/ANALYSIS/BOUNDARIES/RUN002/NEW_ANALYSIS/'
outputfile='../../../data/ANALYSIS/BOUNDARIES/alpha0beta0_NEW.txt'
maxnum=100 #Number of random points to take from every beta0 file.
#mpmin, mpmax=0.1, 3. #Minimum and maximum pulsar mass.
#mcmin, mcmax = 0.1, 3. #Minimum and maximum companion mass.
#Around the region where there is overlap in GR.
mpmin, mpmax=1.28, 1.38 #Minimum and maximum pulsar mass.
mcmin, mcmax = 0.9, 1.05 #Minimum and maximum companion mass.
#A region outside the GR one.
#mpmin, mpmax=1.68, 1.88 #Minimum and maximum pulsar mass.
#mcmin, mcmax = 1.1, 1.2 #Minimum and maximum companion mass.
mpbins=500 #Number of pixels in mp.
mcbins=500 #Number of pixels in mc.
Pbdot_err_factor=1. #Factor by which the error of Pbdot is multiplied.
gamma_err_factor=1. #Same thing for gamma.
omdot_err_factor=1. #Same thing for omegadot.
plot_mm=False #"True" to plot mass mass diagram.

#Binary observable parameters, from '../../data/poster.par'.
Pb=0.19765096245435756787*CM.day
Pbdot=-3.8756162690082992223e-13+5.05e-15-7.96e-16
Pbdot_err=3.0493044461725964325e-15
omdot=5.3102844274576932438*np.pi/180.*1./CM.yr
omdot_err=0.00006147865970216246*np.pi/180.*1./CM.yr
gamma=0.00073204316533203326844
gamma_err=0.00000196110316567236
ecc=0.17188249375774956839
mf=0.171706 #Mass function in solar mass.

#Mass-mass diagram.
mpvec=np.linspace(mpmin, mpmax, mpbins)
mcvec=np.linspace(mcmin, mcmax, mcbins)
MP,MC=np.meshgrid(mpvec, mcvec)

if plot_mm:
	py.ion()

#all_files=os.listdir(inputdir)
all_files=np.sort([file for file in os.listdir(inputdir) if file[0:4]!='.DS_'])

np.random.shuffle(all_files)

alpha0beta0_consistent=[]

of=open(outputfile,'w') #To empty the file.
of.close()

#t=time_estimate(len(alphaA)) #A class that prints estimated computation time.
t=time_estimate(len(all_files)*maxnum) #A class that prints estimated computation time.

for fili in xrange(len(all_files)):
	ifile=all_files[fili]
	#Load scalar tensor data.
	npyfile=ifile[:-3]+'npy'
	
	of=open(outputfile,'a')
	
	#Load beta0 (and other parameters if necessary).
	f = open(inputdir+ifile,'r')
	for line in f.xreadlines():
		if line[3:8]=='beta0':
			beta0=eval(line[9:])
			break
	f.close()

	data=np.loadtxt(inputdir+ifile, skiprows=10, delimiter=',')
	mA=data[:,0]
	alpha0=data[:,1]
	alphaA=data[:,2]
	alphaA_check=data[:,3]
	betaA=data[:,4]
	kA=data[:,5]
	pcen=data[:,6]
	phicen=data[:,7]

	alphaB=alpha0
	betaB=beta0*np.ones(len(alpha0))

	#To avoid a long computation, select a number "maxnum" of random points.
	indis=np.arange(len(alphaA))
	np.random.shuffle(indis)
	indis_sel=indis[0:maxnum]

	#for i in xrange(len(alphaA)):
	for i in indis_sel:
		t.display() #Shows the remaining computation time.
		t.increase() #Needed to calculate the remaining computation time.

		mA_i=mA[i]
		alphaA_i=alphaA[i]
		alphaA_check_i=alphaA_check[i]
		alphaB_i=alphaB[i]
		betaA_i=betaA[i]
		betaB_i=betaB[i]
		kA_i=kA[i]
		pcen_i=pcen[i]

		#GR case.
		#alphaA_i=0
		#alphaA_check_i=0
		#alphaB_i=0
		#betaA_i=0
		#betaB_i=0
		#kA_i=0

		#print alphaA_i, alphaB_i, betaA_i, betaB_i, kA_i
		monopole, dipole, quadrupole_phi, quadrupole_g=CM.Pbdot_f(MP, MC, Pb, ecc, alphaA_i, alphaB_i, betaA_i, betaB_i)
		Pbdot_mat=monopole+dipole+quadrupole_phi+quadrupole_g
		gamma_mat=CM.gamma_f(MP, MC, Pb, ecc, alphaA_i, alphaB_i, kA_i)
		omdot_mat=CM.omdot_f(MP, MC, Pb, ecc, alphaA_i, alphaB_i, betaA_i, betaB_i)

		#Selections.
		Pbdot_sel=(abs(Pbdot_mat-Pbdot)<Pbdot_err*Pbdot_err_factor)
		gamma_sel=(abs(gamma_mat-gamma)<gamma_err*gamma_err_factor)
		omdot_sel=(abs(omdot_mat-omdot)<omdot_err*omdot_err_factor)
		mf_sel=(MC**3.>(mf*(MP+MC)**2)) #Mass function requirement.
		#all_sel=(Pbdot_sel&gamma_sel&omdot_sel)
		all_sel=(Pbdot_sel&gamma_sel&omdot_sel&mf_sel)

		overlap=len(all_sel[all_sel])
		if overlap>0:
			perc=abs((alphaA_i-alphaA_check_i)*100./alphaA_i) #Percentage of difference between alphaA and alphaA_check.
			alpha0beta0_consistent.append((alphaB_i, beta0, perc))
			#of.write('%f, %f, %f \n' %(alphaB_i, beta0, perc))
			of.write('%f, %f, %f, %e \n' %(alphaB_i, beta0, perc, pcen_i))

		#Plot.
		if plot_mm:
			numlevels=30
			levels=np.logspace(np.log10(np.amin(-Pbdot_mat)), np.log10(np.amax(-Pbdot_mat)), numlevels)

			Pbdot_yes=np.zeros(np.shape(Pbdot_mat))
			Pbdot_yes[Pbdot_sel]=1
			gamma_yes=np.zeros(np.shape(gamma_mat))
			gamma_yes[gamma_sel]=1
			omdot_yes=np.zeros(np.shape(omdot_mat))
			omdot_yes[omdot_sel]=1

			py.clf()
			py.contourf(MP, MC, Pbdot_yes, cmap='ocean_r', alpha=0.5)
			py.contourf(MP, MC, gamma_yes, cmap='ocean_r', alpha=0.5)
			py.contourf(MP, MC, omdot_yes, cmap='ocean_r', alpha=0.5)
			raw_input('enter')
	of.close()




