#!/usr/bin/env python
import numpy as np
import pylab as py
import os
from scipy import interpolate as ip
from scipy.interpolate import LinearNDInterpolator as LNDI
from scipy.optimize import fmin
import Eq_solver.DEFAULT_PARAMETERS as DEF
from USEFUL import time_estimate

#Define constants.
grav=DEF.Gn
light=DEF.c
msun=DEF.msun
rsun=DEF.rsun
day=24.*3600.
yr=365.*day

#Define some functions useful for this analysis.
class AttrDict(dict):
	'''Class that converts dictionary keys into class attributes, from "http://stackoverflow.com/questions/4984647/accessing-dict-keys-like-an-attribute-in-python".'''
	def __init__(self, *args, **kwargs):
		super(AttrDict, self).__init__(*args, **kwargs)
		self.__dict__ = self

def getdata(filename):
	'''Get data from either a npy file or a txt file.
		-If it is a npy file, assume the data is a dictionary.
		-If it is a txt file, assume it starts with some comment lines (starting with "#") where some parameters are defined (like beta0=XXX). The last comment line must be the list of data columns (with the same delimiter as the data).
		It can be used as: "d=getdata(filename)", and then "d.beta0", or "d.pcen", etc. To print all possible attributes of data, "print d.keys()".'''
	if filename[-3:]=='txt':
		data={}
		line_vec=[]
		for line in open(filename):
			line_vec.append(line)
			if '=' in line:
				data['%s' %(line.split('=')[0].split()[1])]=eval(line.split('=')[1])
			if line[0]!='#':
				column_names=line_vec[-2].split()[1:]
				break
		#delimiter=column_names[0][-1]
		#delimiter=','
		delimiter=' '
		skiprows=len(line_vec)-1
		keys=[]
		arrays=np.loadtxt(filename, delimiter=delimiter, skiprows=skiprows)
		for ii,column in enumerate(column_names):
			key=column.replace(',','').replace('.','').split('/')[0]
			data['%s' %key]=arrays[:,ii]
	elif filename[-3:]=='npy':
		data=np.load(filename)[()]
	#return data #This would return data as dictionary.
	return AttrDict(data) #This returns data as a class, so the keys can be called as attributes (more convenient).

def py2math(variable):
	'''Replaces an input variable in format %e by the corresponding Mathematica symbol.'''
	return str('%e' %variable).replace('e', ' 10^')

def py2math_vec(vec):
	'''Replaces an input variable in format %e by the corresponding Mathematica symbol.'''
	outputvec=[]
	for var in vec:
		if type(var)==str:
			outputvec.append(var)
		else:
			outputvec.append(py2math(var))
	return tuple(outputvec)

def create_or_replace(run_dir, cluster=None):
	'''Creates directory if it does not exist, or asks if files inside should be removed.'''
	if not cluster:
		if not os.path.isdir(run_dir):
			print 'Creating folder: %s' %run_dir
			os.makedirs(run_dir)
		else:
			if len(os.listdir(run_dir))>0:
				decide=raw_input('Files already exist in this folder. Press "r" to replace. Press anything else to exit. \n')
				if decide=='r':
					raw_input('Press enter to remove all files in: %s' %run_dir)
					os.system('cd %s; rm *' %run_dir)
				else:
					exit()
	else:
		if not os.path.isdir(run_dir):
			os.makedirs(run_dir)
		else:
			if len(os.listdir(run_dir))>0:
				os.system('cd %s; rm *' %run_dir)

def files_to_analyse(beta0_in, run_dir):
	'''Given a tentative value of beta0, "beta0_in", it searches for Mathematica output files in directory "run_dir" studying the closest to that value. It outputs that list of output files. It also outputs the number of files that exist for each beta0 value.'''
	#List all files.
	#list_vec=os.listdir(run_dir)
	list_vec=np.sort([file for file in os.listdir(run_dir) if file[0]!='.'])
	parfile_vec, beta0_vec, ofile_vec, split_bins=[], [], [], 0

	#Read values of beta0 from parfiles and create vector of ofiles.
	for file in list_vec:
		if file[0:2]=='pa':
			parfile_vec.append(file)
			parfile=open(run_dir+file)
			for line in parfile:
				line_i=line.split('=')
				if line_i[0]=='\[Beta]0':
					base=eval(line_i[1].split(' ')[0])
					pow=eval(line_i[1].split(' ')[1].replace(';',')').replace('^','**('))
					beta0_vec.append(base*pow)
			parfile.close()
		elif file[0:2]=='of':
			ofile_vec.append(file)
		#To count how many files there should be for each value of beta0.
		if (file.split('_')[0]=='icfile') & (file.split('_')[1]=='000'):
			split_bins+=1

	#Find indices of files with the closest value to beta0_i.
	indi=abs(np.array(beta0_vec)-beta0_in).argmin()
	beta0=beta0_vec[indi]

	#List all ofiles studying that value of beta0.
	ofile_sel=[]
	for file in ofile_vec:
		if file.split('_')[1]=='%.3i' %indi:
			ofile_sel.append(file)
	ofile_sorted=np.sort(ofile_sel)
	return beta0, ofile_sorted, split_bins

def input_par_math_file(sp=None, sM=None, pNsurT=None, rhoNcenT=None, mtb=None, nt0=None, Gamma=None, kns=None, afun=None, alpha=None, beta0=None):
	'''It inputs the parameters to be used in the Mathematica calculation, and will update the INPUT_PARAMETERS.m file.'''
	files_dir='./Eq_solver/'
	default_file=files_dir+'DEFAULT_PARAMETERS.m'
	input_file=files_dir+'INPUT_PARAMETERS.m'

	#Load default parameters, replace undefined parameters by default.
	Gn, c, msun, rsun=py2math(DEF.Gn), py2math(DEF.c), py2math(DEF.msun), py2math(DEF.rsun)
	#if not sp: sp=py2math(DEF.sp)
	sp = py2math(DEF.sp) if not sp else py2math(sp)
	sM = py2math(DEF.sM) if not sM else py2math(sM)
	pNsurT = py2math(DEF.pNsurT) if not pNsurT else py2math(pNsurT)
	rhoNcenT = py2math(DEF.rhoNcenT) if not rhoNcenT else py2math(rhoNcenT)
	mtb = py2math(DEF.mtb) if not mtb else py2math(mtb)
	nt0 = py2math(DEF.nt0) if not nt0 else py2math(nt0)
	Gamma = py2math(DEF.Gamma) if not Gamma else py2math(Gamma)
	kns = py2math(DEF.kns) if not kns else py2math(kns)
	afun = DEF.afun if not afun else afun
	alpha = DEF.alpha if not alpha else alpha
	beta0 = py2math(DEF.beta0) if not beta0 else py2math(beta0)
	mathvars=(Gn, c, msun, rsun, sp, sM, pNsurT, rhoNcenT, mtb, nt0, Gamma, kns, afun, alpha, beta0)
	file=open(input_file,'w')
	file.write('Gn=%s; \nc=%s; \nMsun=%s; \nrsun=%s; \nsp=%s; \nsM=%s; \npNsurT=%s; \n\[Rho]NcenT=%s; \nmtb=%s; \nnt0=%s; \n\[CapitalGamma]=%s; \nkns=%s; \n%s; \n%s; \n\[Beta]0=%s; \n' %mathvars)
	file.close()

def create_par_file(sp=DEF.sp, sM=DEF.sM, parfile=None, mtb=DEF.mtb, nt0=DEF.nt0, Gamma=DEF.Gamma, kns=DEF.kns, afun=DEF.afun, alpha=DEF.alpha, beta0=DEF.beta0):
	'''It inputs the parameters to be used in the Mathematica calculation, and will update the INPUT_PARAMETERS.m file.'''
	files_dir='./Eq_solver/'
	default_file=files_dir+'DEFAULT_PARAMETERS.m'
	if not parfile:
		parfile=files_dir+'INPUT_PARAMETERS.m'
	pNsurT, rhoNcenT, pNminT, pNmaxT, phiNminT, phiNmaxT=DEF.par_fun(beta0)
	pythonvars=(grav, light, msun, rsun, sp, sM, pNsurT, rhoNcenT, mtb, nt0, Gamma, kns, afun, alpha, beta0)
	mathvars=py2math_vec(pythonvars)
	file=open(parfile,'w')
	file.write('Gn=%s; \nc=%s; \nMsun=%s; \nrsun=%s; \nsp=%s; \nsM=%s; \npNsurT=%s; \n\[Rho]NcenT=%s; \nmtb=%s; \nnt0=%s; \n\[CapitalGamma]=%s; \nkns=%s; \n%s; \n%s; \n\[Beta]0=%s; \n' %mathvars)
	file.close()

def eq_solver(parfile, inputfile, outputfile, cluster=None):
	'''Solves the system of differential equations using the Mathematica script.'''
	if not cluster:
		os.system('./Eq_solver/Eq_solver.m %s %s %s' %(parfile, inputfile, outputfile))
	else:
		os.system('/home/prosado/projects/scalartensor/scalar-tensor-scripts/ANALYSIS/BOUNDARIES/Eq_solver/G_Eq_solver.m %s %s %s' %(parfile, inputfile, outputfile))
	return

def old_analysis(data, mbA_min=0.5, mbA_max=3.5, mA_min=0., Rns_min=0., abs_alpha0_min=1e-4, lphi0bins=500, mbAbins=500):
	'''Inputs data (a dictionary) with the Mathematica output and outputs a numpy array with the scalar tensor parameters using the old analysis.'''
	pcen=data['pcen']
	phicen=data['phicen']
	Rns=data['Rns']
	alphaA=data['alphaA']
	phi0=data['phi0']
	mA=data['mA']
	mbA=data['mbA']
	IA=data['IA']
	alpha0=data['alpha0']
	beta0=data['beta0']

	sp=DEF.sp #Typical maximum pressure at the centre of the NS in Pa.
	sM=DEF.sM #Typical radius of the NS in m.
	mtb=DEF.mtb #Baryonic mass (kg).
	nt0=DEF.nt0 #Baryon number density (m^-3).
	Gamma=DEF.Gamma #Dimensionless parameter of the polytrope EoS.
	kns=DEF.kns #Dimensionless constant of the polytrope EoS.

	############################
	#Take only physical solutions.
	c1=(mbA>mbA_min)&(mbA<mbA_max)&(mA>mA_min)&(Rns>Rns_min)&(abs(alpha0)>abs_alpha0_min)
	c2=phicen*phi0>0 #Condition explained in DamourEsp.Far.1996-98.
	check=c1&c2
	pcen=pcen[check]
	phicen=phicen[check]
	Rns=Rns[check]
	alphaA=alphaA[check]
	phi0=phi0[check]
	mA=mA[check]
	mbA=mbA[check]
	IA=IA[check]
	alpha0=alpha0[check]

	#Interpolate all quantities on the log10(phi0)-mb plane.
	lphi0=np.log10(phi0)
	points=np.vstack((lphi0, mbA)).T
	mA_f=ip.LinearNDInterpolator(points, mA)
	alphaA_f=ip.LinearNDInterpolator(points, alphaA)
	IA_f=ip.LinearNDInterpolator(points, IA)
	alpha0_f=ip.LinearNDInterpolator(points, alpha0)
	pcen_f=ip.LinearNDInterpolator(points, pcen)
	phicen_f=ip.LinearNDInterpolator(points, phicen)

	lphi0vec=np.linspace(min(lphi0), max(lphi0), lphi0bins)
	mbAvec=np.linspace(min(mbA), max(mbA), mbAbins)
	alphaAcalc_mat=np.zeros((lphi0bins-1,mbAbins))
	alphaAtrue_mat=np.zeros(np.shape(alphaAcalc_mat))
	kA_mat=np.zeros(np.shape(alphaAcalc_mat))
	alpha0_mat=np.zeros(np.shape(alphaAcalc_mat))
	mA_mat=np.zeros(np.shape(alphaAcalc_mat))
	pcen_mat=np.zeros(np.shape(alphaAcalc_mat))
	phicen_mat=np.zeros(np.shape(alphaAcalc_mat))

	for mbi in xrange(len(mbAvec)):
		mAvec=mA_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi]) #Vector of mA as a function of phi0 along the stripe of constant mbA.
		mA_mat[:,mbi]=0.5*(mAvec[1:]+mAvec[:-1])

		alphaAcalc_mat[:,mbi]=np.diff(np.log(mAvec))*1./np.diff(10**(lphi0vec))
		alphaAtrue=alphaA_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi])
		alphaAtrue_mat[:,mbi]=0.5*(alphaAtrue[1:]+alphaAtrue[:-1])

		IAvec=IA_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi])
		kA_mat[:,mbi]=-np.diff(np.log(IAvec))*1./np.diff(10**(lphi0vec))

		alpha0true=alpha0_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi])
		alpha0_mat[:,mbi]=0.5*(alpha0true[1:]+alpha0true[:-1])
	
		pcenvec=pcen_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi])
		pcen_mat[:,mbi]=0.5*(pcenvec[1:]+pcenvec[:-1])

		phicenvec=phicen_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi])
		phicen_mat[:,mbi]=0.5*(phicenvec[1:]+phicenvec[:-1])
	
	#tol=5 #Tolerance: percentage to which the derived and "true" alphaA must coincide.

	alphaAtrue_m=0.5*(alphaAtrue_mat[1:,:]+alphaAtrue_mat[:-1,:])
	alphaAcalc_m=0.5*(alphaAcalc_mat[1:,:]+alphaAcalc_mat[:-1,:])
	lphi0vec_m=0.5*(lphi0vec[1:]+lphi0vec[:-1])
	#betaA_mat=(np.diff(alphaAtrue_mat,axis=0).T*1./np.diff(lphi0vec_m)).T
	betaA_mat=(np.diff(alphaAtrue_mat,axis=0).T*1./np.diff(10**(lphi0vec_m))).T
	kA_m=0.5*(kA_mat[1:,:]+kA_mat[:-1,:])
	alpha0_m=0.5*(alpha0_mat[1:,:]+alpha0_mat[:-1,:])
	mA_m=0.5*(mA_mat[1:,:]+mA_mat[:-1,:])
	pcen_m=0.5*(pcen_mat[1:,:]+pcen_mat[:-1,:])
	phicen_m=0.5*(phicen_mat[1:,:]+phicen_mat[:-1,:])

	#Output a set of points that do not have nans.
	selecti=(-np.isnan(alphaAtrue_m))&(-np.isnan(betaA_mat))&(-np.isnan(kA_m))
	o_data=np.vstack((mA_m[selecti], alpha0_m[selecti], alphaAtrue_m[selecti], alphaAcalc_m[selecti], betaA_mat[selecti], kA_m[selecti], pcen_m[selecti], phicen_m[selecti])).T
	return beta0, sp, sM, mtb, nt0, Gamma, kns, o_data

def old_analysis2(data, mbA_min=0.5, mbA_max=3.5, mA_min=0.5, mA_max=3.5, Rns_min=0., abs_alpha0_min=1e-4, abs_alpha0_max=1., mbA_low=1., mbA_upp=3., abs_alpha0_bins=500, mbA_bins=500):
	'''Inputs data (a dictionary) with the Mathematica output and outputs a numpy array with the scalar tensor parameters using the new analysis.'''
	pcen=data['pcen']
	phicen=data['phicen']
	Rns=data['Rns']
	alphaA=data['alphaA']
	phi0=data['phi0']
	mA=data['mA']
	mbA=data['mbA']
	IA=data['IA']
	alpha0=data['alpha0']
	beta0=data['beta0']

	sp=DEF.sp #Typical maximum pressure at the centre of the NS in Pa.
	sM=DEF.sM #Typical radius of the NS in m.
	mtb=DEF.mtb #Baryonic mass (kg).
	nt0=DEF.nt0 #Baryon number density (m^-3).
	Gamma=DEF.Gamma #Dimensionless parameter of the polytrope EoS.
	kns=DEF.kns #Dimensionless constant of the polytrope EoS.

	############################
	#Take only physical solutions.
	c1=(mbA>mbA_min)&(mbA<mbA_max)&(mA>mA_min)&(mA<mA_max)&(Rns>Rns_min)&(abs(alpha0)>abs_alpha0_min)
	c2=phicen*phi0>0 #Condition explained in DamourEsp.Far.1996-98.
	check=c1&c2
	pcen=pcen[check]
	phicen=phicen[check]
	Rns=Rns[check]
	alphaA=alphaA[check]
	phi0=phi0[check]
	mA=mA[check]
	mbA=mbA[check]
	IA=IA[check]
	alpha0=alpha0[check]
	
	lphi0=np.log10(phi0)

	lphi0bins=abs_alpha0_bins
	if DEF.alpha!='\[Alpha][\[Beta]0_, \[CurlyPhi]_] = \[Beta]0 \[CurlyPhi]':
		print 'I assume that alpha0=phi0/beta0. If it is not, it is not very important: simply, the output will not be a constant grid on |alpha0|.'
		lphi0vec=np.linspace(min(lphi0), max(lphi0), lphi0bins)
		alpha0vec=10**(lphi0vec)*beta0
	else:
		phi0_min=abs_alpha0_min*1./abs(beta0)
		phi0_max=abs_alpha0_max*1./abs(beta0)
		lphi0vec=np.linspace(np.log10(phi0_min), np.log10(phi0_max), lphi0bins)
		alpha0vec=np.sign(beta0)*np.logspace(np.log10(abs_alpha0_min), np.log10(abs_alpha0_max), abs_alpha0_bins)
	#mbAvec=np.linspace(min(mbA), max(mbA), mbAbins)
	#mbAvec=np.linspace(mbA_low, mbA_upp, mbA_bins)
	mbAvec=np.sort(np.random.uniform(mbA_low, mbA_upp, mbA_bins))
	
	#Interpolate all quantities on the log10(phi0)-mb plane.
	points=np.vstack((lphi0, mbA)).T
	mA_f=ip.LinearNDInterpolator(points, mA)
	alphaA_f=ip.LinearNDInterpolator(points, alphaA)
	IA_f=ip.LinearNDInterpolator(points, IA)
	#alpha0_f=ip.LinearNDInterpolator(points, alpha0)
	pcen_f=ip.LinearNDInterpolator(points, pcen)
	phicen_f=ip.LinearNDInterpolator(points, phicen)
	Rns_f=ip.LinearNDInterpolator(points, Rns)

	alphaAcalc_mat=np.zeros((lphi0bins-1,mbA_bins))
	alphaAtrue_mat=np.zeros(np.shape(alphaAcalc_mat))
	betaA_mat=np.zeros(np.shape(alphaAcalc_mat))
	kA_mat=np.zeros(np.shape(alphaAcalc_mat))
	alpha0_mat=np.zeros(np.shape(alphaAcalc_mat))
	mA_mat=np.zeros(np.shape(alphaAcalc_mat))
	pcen_mat=np.zeros(np.shape(alphaAcalc_mat))
	phicen_mat=np.zeros(np.shape(alphaAcalc_mat))
	mbA_mat=np.zeros(np.shape(alphaAcalc_mat))
	Rns_mat=np.zeros(np.shape(alphaAcalc_mat))

	for mbi in xrange(len(mbAvec)):
		mbA_check=np.ones(lphi0bins)*mbAvec[mbi]
		
		#Quantities obtained by interpolation.
		mAvec=mA_f(lphi0vec, mbA_check) #Vector of mA as a function of phi0 along the stripe of constant mbA.
		alphaAtrue=alphaA_f(lphi0vec, mbA_check)
		IAvec=IA_f(lphi0vec, mbA_check)
		#alpha0vec=alpha0_f(lphi0vec, mbA_check) #Instead of outputting the interpolated alpha0, I will output the original one.
		pcenvec=pcen_f(lphi0vec, mbA_check)
		phicenvec=phicen_f(lphi0vec, mbA_check)
		Rnsvec=Rns_f(lphi0vec, mbA_check)

		#Quantities obtained by differentiation.
		alphaAcalc=np.diff(np.log(mAvec))*1./np.diff(10**(lphi0vec))
		betaAvec=np.diff(alphaAtrue)*1./np.diff(10**(lphi0vec))
		kAvec=-np.diff(np.log(IAvec))*1./np.diff(10**(lphi0vec))

		#Create output matrices.
		mA_mat[:,mbi]=0.5*(mAvec[1:]+mAvec[:-1])
		alphaAtrue_mat[:,mbi]=0.5*(alphaAtrue[1:]+alphaAtrue[:-1])
		alphaAcalc_mat[:,mbi]=alphaAcalc
		betaA_mat[:,mbi]=betaAvec
		kA_mat[:,mbi]=kAvec
		alpha0_mat[:,mbi]=0.5*(alpha0vec[1:]+alpha0vec[:-1])
		pcen_mat[:,mbi]=0.5*(pcenvec[1:]+pcenvec[:-1])
		phicen_mat[:,mbi]=0.5*(phicenvec[1:]+phicenvec[:-1])
		mbA_mat[:,mbi]=0.5*(mbA_check[1:]+mbA_check[:-1])
		Rns_mat[:,mbi]=0.5*(Rnsvec[1:]+Rnsvec[:-1])

	#Output a set of points that do not have nans.
	selecti=(-np.isnan(alphaAtrue_mat))&(-np.isnan(betaA_mat))&(-np.isnan(kA_mat))
	o_data=np.vstack((mA_mat[selecti], alpha0_mat[selecti], alphaAtrue_mat[selecti], alphaAcalc_mat[selecti], betaA_mat[selecti], kA_mat[selecti], pcen_mat[selecti], phicen_mat[selecti], mbA_mat[selecti], Rns_mat[selecti])).T
	return beta0, sp, sM, mtb, nt0, Gamma, kns, o_data

def analysis(data, mbA_min=0.5, mbA_max=3.5, mA_min=0.5, mA_max=3.5, Rns_min=0., abs_alpha0_min=1e-4, abs_alpha0_max=1., mA_low=1., mA_upp=3., abs_alpha0_bins=500, mbA_bins=500):
	'''Inputs data (a dictionary) with the Mathematica output and outputs a numpy array with the scalar tensor parameters using the new analysis.'''
	pcen=data['pcen']
	phicen=data['phicen']
	Rns=data['Rns']
	alphaA=data['alphaA']
	phi0=data['phi0']
	mA=data['mA']
	mbA=data['mbA']
	IA=data['IA']
	alpha0=data['alpha0']
	beta0=data['beta0']

	sp=DEF.sp #Typical maximum pressure at the centre of the NS in Pa.
	sM=DEF.sM #Typical radius of the NS in m.
	mtb=DEF.mtb #Baryonic mass (kg).
	nt0=DEF.nt0 #Baryon number density (m^-3).
	Gamma=DEF.Gamma #Dimensionless parameter of the polytrope EoS.
	kns=DEF.kns #Dimensionless constant of the polytrope EoS.

	############################
	#Take only physical solutions.
	c1=(mbA>mbA_min)&(mbA<mbA_max)&(mA>mA_min)&(mA<mA_max)&(Rns>Rns_min)&(abs(alpha0)>abs_alpha0_min)
	c2=phicen*phi0>0 #Condition explained in DamourEsp.Far.1996-98.
	check=c1&c2
	pcen=pcen[check]
	phicen=phicen[check]
	Rns=Rns[check]
	alphaA=alphaA[check]
	phi0=phi0[check]
	mA=mA[check]
	mbA=mbA[check]
	IA=IA[check]
	alpha0=alpha0[check]
	
	lphi0=np.log10(phi0)

	lphi0bins=abs_alpha0_bins
	if DEF.alpha!='\[Alpha][\[Beta]0_, \[CurlyPhi]_] = \[Beta]0 \[CurlyPhi]':
		print 'I assume that alpha0=phi0/beta0. If it is not, it is not very important: simply, the output will not be a constant grid on |alpha0|.'
		lphi0vec=np.linspace(min(lphi0), max(lphi0), lphi0bins)
		alpha0vec=10**(lphi0vec)*beta0
	else:
		phi0_min=abs_alpha0_min*1./abs(beta0)
		phi0_max=abs_alpha0_max*1./abs(beta0)
		lphi0vec=np.linspace(np.log10(phi0_min), np.log10(phi0_max), lphi0bins)
		alpha0vec=np.sign(beta0)*np.logspace(np.log10(abs_alpha0_min), np.log10(abs_alpha0_max), abs_alpha0_bins)
	
	#Find the minimum and maximum values of baryonic mass that fulfill the restrictions imposed.
	mass_sel=(mA<=mA_upp)&(mA>=mA_low)
	mbA_low=np.amin(mbA[mass_sel]) #Absolute minimum of mbA, given the constraints.
	mbA_upp=np.amax(mbA[mass_sel]) #Absolute maximum.
	
	#mbAvec=np.linspace(min(mbA), max(mbA), mbAbins)
	#mbAvec=np.linspace(mbA_low, mbA_upp, mbA_bins)
	#mbAvec=np.sort(np.random.uniform(mbA_low, mbA_upp, mbA_bins))
	mbAvec=np.sort(np.random.uniform(mbA_low, mbA_upp, mbA_bins))
	
	#Interpolate all quantities on the log10(phi0)-mb plane.
	points=np.vstack((lphi0, mbA)).T
	mA_f=ip.LinearNDInterpolator(points, mA)
	alphaA_f=ip.LinearNDInterpolator(points, alphaA)
	IA_f=ip.LinearNDInterpolator(points, IA)
	#alpha0_f=ip.LinearNDInterpolator(points, alpha0)
	pcen_f=ip.LinearNDInterpolator(points, pcen)
	phicen_f=ip.LinearNDInterpolator(points, phicen)
	Rns_f=ip.LinearNDInterpolator(points, Rns)

	alphaAcalc_mat=np.zeros((lphi0bins-1,mbA_bins))
	alphaAtrue_mat=np.zeros(np.shape(alphaAcalc_mat))
	betaA_mat=np.zeros(np.shape(alphaAcalc_mat))
	kA_mat=np.zeros(np.shape(alphaAcalc_mat))
	alpha0_mat=np.zeros(np.shape(alphaAcalc_mat))
	mA_mat=np.zeros(np.shape(alphaAcalc_mat))
	pcen_mat=np.zeros(np.shape(alphaAcalc_mat))
	phicen_mat=np.zeros(np.shape(alphaAcalc_mat))
	mbA_mat=np.zeros(np.shape(alphaAcalc_mat))
	Rns_mat=np.zeros(np.shape(alphaAcalc_mat))

	for mbi in xrange(len(mbAvec)):
		mbA_check=np.ones(lphi0bins)*mbAvec[mbi]
		
		#Quantities obtained by interpolation.
		mAvec=mA_f(lphi0vec, mbA_check) #Vector of mA as a function of phi0 along the stripe of constant mbA.
		alphaAtrue=alphaA_f(lphi0vec, mbA_check)
		IAvec=IA_f(lphi0vec, mbA_check)
		#alpha0vec=alpha0_f(lphi0vec, mbA_check) #Instead of outputting the interpolated alpha0, I will output the original one.
		pcenvec=pcen_f(lphi0vec, mbA_check)
		phicenvec=phicen_f(lphi0vec, mbA_check)
		Rnsvec=Rns_f(lphi0vec, mbA_check)

		#Quantities obtained by differentiation.
		alphaAcalc=np.diff(np.log(mAvec))*1./np.diff(10**(lphi0vec))
		betaAvec=np.diff(alphaAtrue)*1./np.diff(10**(lphi0vec))
		kAvec=-np.diff(np.log(IAvec))*1./np.diff(10**(lphi0vec))

		#Create output matrices.
		mA_mat[:,mbi]=0.5*(mAvec[1:]+mAvec[:-1])
		alphaAtrue_mat[:,mbi]=0.5*(alphaAtrue[1:]+alphaAtrue[:-1])
		alphaAcalc_mat[:,mbi]=alphaAcalc
		betaA_mat[:,mbi]=betaAvec
		kA_mat[:,mbi]=kAvec
		alpha0_mat[:,mbi]=0.5*(alpha0vec[1:]+alpha0vec[:-1])
		pcen_mat[:,mbi]=0.5*(pcenvec[1:]+pcenvec[:-1])
		phicen_mat[:,mbi]=0.5*(phicenvec[1:]+phicenvec[:-1])
		mbA_mat[:,mbi]=0.5*(mbA_check[1:]+mbA_check[:-1])
		Rns_mat[:,mbi]=0.5*(Rnsvec[1:]+Rnsvec[:-1])

	#Output a set of points that do not have nans.
	selecti=(-np.isnan(alphaAtrue_mat))&(-np.isnan(betaA_mat))&(-np.isnan(kA_mat))
	o_data=np.vstack((mA_mat[selecti], alpha0_mat[selecti], alphaAtrue_mat[selecti], alphaAcalc_mat[selecti], betaA_mat[selecti], kA_mat[selecti], pcen_mat[selecti], phicen_mat[selecti], mbA_mat[selecti], Rns_mat[selecti])).T
	return beta0, sp, sM, mtb, nt0, Gamma, kns, o_data

def stability_analysis(data, mbA_min=0.5, mbA_max=3.5, mA_min=0.5, mA_max=3.5, Rns_min=0., abs_alpha0_min=1e-4, abs_alpha0_max=1., mbA_low=1., mbA_upp=3., abs_alpha0_bins=500, mbA_bins=500):
	'''Inputs data (a dictionary) with the Mathematica output and outputs a numpy array with the scalar tensor parameters using the old analysis.'''
	pcen=data['pcen']
	phicen=data['phicen']
	Rns=data['Rns']
	alphaA=data['alphaA']
	phi0=data['phi0']
	mA=data['mA']
	mbA=data['mbA']
	IA=data['IA']
	alpha0=data['alpha0']
	beta0=data['beta0']

	sp=DEF.sp #Typical maximum pressure at the centre of the NS in Pa.
	sM=DEF.sM #Typical radius of the NS in m.
	mtb=DEF.mtb #Baryonic mass (kg).
	nt0=DEF.nt0 #Baryon number density (m^-3).
	Gamma=DEF.Gamma #Dimensionless parameter of the polytrope EoS.
	kns=DEF.kns #Dimensionless constant of the polytrope EoS.

	############################
	#Take only physical solutions.
	c1=(mbA>mbA_min)&(mbA<mbA_max)&(mA>mA_min)&(mA<mA_max)&(Rns>Rns_min)&(abs(alpha0)>abs_alpha0_min)
	c2=phicen*phi0>0 #Condition explained in DamourEsp.Far.1996-98.
	check=c1&c2
	pcen=pcen[check]
	phicen=phicen[check]
	Rns=Rns[check]
	alphaA=alphaA[check]
	phi0=phi0[check]
	mA=mA[check]
	mbA=mbA[check]
	IA=IA[check]
	alpha0=alpha0[check]
	
	py.ion()
	#py.scatter(pcen, mbA, c=np.log10(phi0))
	#py.plot(pcen, mA, '.', alpha=0.5)
	#py.xscale('log')
	#raw_input('enter')
	
	lphi0=np.log10(phi0)

	lphi0bins=abs_alpha0_bins
	if DEF.alpha!='\[Alpha][\[Beta]0_, \[CurlyPhi]_] = \[Beta]0 \[CurlyPhi]':
		print 'I assume that alpha0=phi0/beta0. If it is not, it is not very important: simply, the output will not be a constant grid on |alpha0|.'
		lphi0vec=np.linspace(min(lphi0), max(lphi0), lphi0bins)
	else:
		phi0_min=abs_alpha0_min*1./abs(beta0)
		phi0_max=abs_alpha0_max*1./abs(beta0)
		lphi0vec=np.linspace(np.log10(phi0_min), np.log10(phi0_max), lphi0bins)
	#mbAvec=np.linspace(min(mbA), max(mbA), mbAbins)
	mbAvec=np.linspace(mbA_low, mbA_upp, mbA_bins)
	
	#Interpolate all quantities on the log10(phi0)-mb plane.
	points=np.vstack((lphi0, mbA)).T
	mA_f=ip.LinearNDInterpolator(points, mA)
	#alphaA_f=ip.LinearNDInterpolator(points, alphaA)
	#IA_f=ip.LinearNDInterpolator(points, IA)
	#alpha0_f=ip.LinearNDInterpolator(points, alpha0)
	pcen_f=ip.LinearNDInterpolator(points, pcen)
	#phicen_f=ip.LinearNDInterpolator(points, phicen)

	#for mbi in xrange(len(mbAvec)):
	print beta0
	for phi0_i in xrange(lphi0bins):
		lphi0_check=np.ones(mbA_bins)*lphi0vec[phi0_i]

		#sel=(abs(phi0-10**(lphi0vec[phi0_i]))*100./(10**(lphi0vec[phi0_i]))<30.)
		#mbA_check=np.ones(lphi0bins)*mbAvec[mbi]
		
		#Quantities obtained by interpolation.
		mAvec=mA_f(lphi0_check, mbAvec) #Vector of mA as a function of phi0 along the stripe of constant mbA.
		pcenvec=pcen_f(lphi0_check, mbAvec)
		sel=(-np.isnan(mAvec))
		if len(sel[sel])==0:
			continue
		py.clf()
		py.plot(mAvec, pcenvec, '.')
		#py.plot(mA[sel], pcen[sel], '.')
		py.yscale('log')
		py.pause(0.001)
		#raw_input('enter')

	#Output a set of points that do not have nans.
	#selecti=(-np.isnan(alphaAtrue_mat))&(-np.isnan(betaA_mat))&(-np.isnan(kA_mat))
	#o_data=np.vstack((mA_mat[selecti], alpha0_mat[selecti], alphaAtrue_mat[selecti], alphaAcalc_mat[selecti], betaA_mat[selecti], kA_mat[selecti], pcen_mat[selecti], phicen_mat[selecti])).T
	return 1,2,3,4,5,6,7,8

def Pbdot_f(mma, mmb, Pb, ecc, alphaA, alphaB, betaA, betaB):
	'''mma and mmb are in solar masses.'''
	ma=mma*msun #Mass A in kg.
	mb=mmb*msun #Mass B.
	M=(ma+mb) #Total mass.
	n=2.*np.pi*1./Pb
	XA=ma*1./M
	XB=1.-XA
	nu=XA*XB
	GABterm=(grav*(1.+alphaA*alphaB)*M*n/(light**3))
	monopole1=-3.*np.pi*nu*1./(1.+alphaA*alphaB)*GABterm**(5./3.)*ecc*ecc*(1.+ecc*ecc*1./4.)*1./(1.-ecc*ecc)**(7./2.)
	monopole2=(5./3.*(alphaA+alphaB)-2./3.*(alphaA*XA+alphaB*XB)+(betaA*alphaB+betaB*alphaA)*1./(1.+alphaA*alphaB))**2.
	monopole=monopole1*monopole2

	dipole1=-2.*np.pi*nu*1./(1.+alphaA*alphaB)*GABterm*(1.+ecc*ecc*1./2)/(1.-ecc*ecc)**(5./2.)*(alphaA-alphaB)**2.
	dipole2=-4.*np.pi/(1.+alphaA*alphaB)*nu*GABterm**(5./3.)*1./(1.-ecc*ecc)
	dipole3=8./5.*(1.+31.*ecc*ecc/8.+19.*ecc**4./32.)*(alphaA-alphaB)*(alphaA*XA+alphaB*XB)*(XA-XB)+(1.+3.*ecc*ecc+3.*ecc**4./8.)*(alphaA-alphaB)*(betaB*alphaA*XA-betaA*alphaB*XB)*1./(1.+alphaA*alphaB)
	#dipole=dipole1-dipole2*dipole3
	dipole=dipole1+dipole2*dipole3

	quadrupole_g=-192.*np.pi*nu/(5.*(1.+alphaA*alphaB))*GABterm**(5./3.)*(1.+73.*ecc*ecc/24.+37.*ecc**4./96.)/(1.-ecc*ecc)**(7./2.)

	quadrupole_phi=quadrupole_g*1./6.*((alphaA+alphaB)-alphaA*XA+alphaB*XB)**2.
	return monopole, dipole, quadrupole_phi, quadrupole_g

def Pbdot_all_f(mma, mmb, Pb, ecc, alphaA, alphaB, betaA, betaB):
	'''Same as Pbdot_f, but outputting all contributions together.'''
	monopole, dipole, quadrupole_phi, quadrupole_g=Pbdot_f(mma, mmb, Pb, ecc, alphaA, alphaB, betaA, betaB)
	return monopole+dipole+quadrupole_phi+quadrupole_g

def gamma_f(mma, mmb, Pb, ecc, alphaA, alphaB, kA):
	''''''
	ma=mma*msun #Mass A in kg.
	mb=mmb*msun #Mass B.
	M=(ma+mb) #Total mass.
	n=2.*np.pi*1./Pb
	XA=ma*1./M
	XB=1.-XA
	nu=XA*XB
	GABterm=(grav*(1.+alphaA*alphaB)*M*n/(light**3))
	return ecc*XB*1./(n*(1.+alphaA*alphaB))*GABterm**(2./3.)*(XB*(1.+alphaA*alphaB)+1.+kA*alphaB)

def omdot_f(mma, mmb, Pb, ecc, alphaA, alphaB, betaA, betaB):
	''''''
	ma=mma*msun #Mass A in kg.
	mb=mmb*msun #Mass B.
	M=(ma+mb) #Total mass.
	n=2.*np.pi*1./Pb
	XA=ma*1./M
	XB=1.-XA
	nu=XA*XB
	GABterm=(grav*(1.+alphaA*alphaB)*M*n/(light**3))
	return 3.*n*1./(1.-ecc*ecc)*GABterm**(2./3.)*((1.-1./3.*alphaA*alphaB)*1./(1.+alphaA*alphaB)-(XA*betaB*alphaA*alphaA+XB*betaA*alphaB*alphaB)*1./(6.*(1.+alphaA*alphaB)**2.))

def chisq(pbdot_obs, pbdot_th, pbdot_err, gamma_obs, gamma_th, gamma_err, omdot_obs, omdot_th, omdot_err):
	'''Chi square function, as defined in DamourEsposito-Farese1998. Keep in mind that Pbdot_obs should contain the contribution of gal!!'''
	return ((pbdot_obs-pbdot_th)*1./(pbdot_err))**2+((gamma_obs-gamma_th)*1./(gamma_err))**2.+((omdot_obs-omdot_th)*1./(omdot_err))**2.

def chisq_analysis_old(std, opd, outputfile, mB_min=0.5, mB_max=1.5, mB_bins=10000):
	''''''
	#Create (or empty) the output file.
	of=open(outputfile,'w')
	of.close()
	of=open(outputfile,'a') #Open it again for appending.

	aalpha0vec=np.unique(abs(std.alpha0)) #Vector of |alpha0| bins.
	mBvec=np.linspace(mB_min, mB_max, mB_bins) #Vector of companion masses.

	t=time_estimate(len(aalpha0vec)) #A class that prints estimated computation time.

	for aai in xrange(len(aalpha0vec)):
		t.display() #Shows the remaining computation time.
		t.increase() #Needed to calculate the remaining computation time.

		sel=(abs(std.alpha0)==aalpha0vec[aai])
		mAbins=len(std.mA[sel])

		#Extend all quantities to be a matrix.
		alpha0=aalpha0vec[aai]
		beta0=std.beta0
		mA_mat=np.tile(std.mA[sel], (mB_bins, 1))
		mB_mat=np.tile(mBvec, (mAbins, 1)).T
		alphaA_mat=np.tile(std.alphaA_true[sel], (mB_bins, 1))
		betaA_mat=np.tile(std.betaA[sel], (mB_bins, 1))
		kA_mat=np.tile(std.kA[sel], (mB_bins, 1))
		
		if np.sign(np.amin(1.+alphaA_mat*alpha0))<0: #This would produce nans.
			abc=[beta0, alpha0, -1, -1, -1, -1, -1, -1]
		else:
			alphaA_mat[(1.+alphaA_mat*alpha0)<0]=1e20

			Pbdot_th=Pbdot_all_f(mA_mat, mB_mat, opd.Pb, opd.ecc, alphaA_mat, alpha0, betaA_mat, beta0)
			gamma_th=gamma_f(mA_mat, mB_mat, opd.Pb, opd.ecc, alphaA_mat, alpha0, kA_mat)
			omdot_th=omdot_f(mA_mat, mB_mat, opd.Pb, opd.ecc, alphaA_mat, alpha0, betaA_mat, beta0)
			chisq_vec=chisq(opd.Pbdot+opd.Pbdot_add, Pbdot_th, opd.Pbdot_err+opd.Pbdot_add_err, opd.gamma, gamma_th, opd.gamma_err, opd.omdot, omdot_th, opd.omdot_err)

			#Here I must also impose a condition on sin(i). This comes from Kepler's third law (generalised to SCT). The easiest is to impose -1<sin(i)<1, for a given SCT and a given pair of masses. A better approach is to accept only the values of sin(i) for which the Shapiro delay is consistent with the (not) observed one.

			i,j=np.unravel_index(chisq_vec.argmin(), chisq_vec.shape)

			abc=[beta0, alpha0, chisq_vec[i,j], mA_mat[i,j], mB_mat[i,j], alphaA_mat[i,j], betaA_mat[i,j], kA_mat[i,j]]
		abc_str=['%e ' %item for item in abc]

		of.write(' '.join(abc_str)+'\n')
		of.flush() #So that I can access the file while it is being populated.
		#with open(outputfile,'a') as of:
		#	of.write(' '.join(abc_str)+'\n')
	of.close()

def mB_GR_f(mA, Pb, ecc, omdot):
	'''Mass of the companion, according to GR, given the mass of the pulsar "mA" in solar mass, the period of the binary "Pb" in s, the eccentricity "ecc", and the advance of periastron "omdot" in rad/s.'''
	return ((light**2.*omdot*(1.-ecc*ecc)*1./(3.*(2.*np.pi/Pb)**(5./3.)*grav**(2./3.)))**(3./2.))*1./msun-mA

def Kepler_condi(mA, mB, x, Pb, alphaA, alphaB):
	'''Checks if Kepler's third law (for a ST theory with parameters "alphaA" and "alphaB") is fulfilled, given the mass of the pulsar "mA", in solar mass, the mass of the companion "mB", in solar mass, the projected semi-major axis of the orbit "x", in m (not in light-seconds!), and the orbital period "Pb" in s. It returns True (when fulfilled) or False (when not fulfilled).'''
	return mB**3*msun/((mA+mB)**2.)>4.*np.pi**2.*x**3./(Pb**2.*grav*(1.+alphaA*alphaB))

def chisq_masses(mB, mA, alphaA, alphaB, betaA, betaB, kA, Pb, ecc, pbdot_obs, pbdot_err, gamma_obs, gamma_err, omdot_obs, omdot_err):
	''''''
	pbdot_th=Pbdot_all_f(mA, mB, Pb, ecc, alphaA, alphaB, betaA, betaB)
	gamma_th=gamma_f(mA, mB, Pb, ecc, alphaA, alphaB, kA)
	omdot_th=omdot_f(mA, mB, Pb, ecc, alphaA, alphaB, betaA, betaB)
	return chisq(pbdot_obs, pbdot_th, pbdot_err, gamma_obs, gamma_th, gamma_err, omdot_obs, omdot_th, omdot_err)

def chisq_masses_Kepler(mB, mA, alphaA, alphaB, betaA, betaB, kA, x, Pb,ecc, pbdot_obs, pbdot_err, gamma_obs, gamma_err, omdot_obs, omdot_err):
	''''''
	pbdot_th=Pbdot_all_f(mA, mB, Pb, ecc, alphaA, alphaB, betaA, betaB)
	gamma_th=gamma_f(mA, mB, Pb, ecc, alphaA, alphaB, kA)
	omdot_th=omdot_f(mA, mB, Pb, ecc, alphaA, alphaB, betaA, betaB)
	Kepler_term=(not Kepler_condi(mA, mB, x, Pb, alphaA, alphaB))*1e20 #This sould be 0 if Keplers third law is fulfilled, otherwise infinity.
	return chisq(pbdot_obs, pbdot_th, pbdot_err, gamma_obs, gamma_th, gamma_err, omdot_obs, omdot_th, omdot_err)+Kepler_term

def chisq_analysis(std, opd, outputfile):
	''''''
	#Create (or empty) the output file.
	of=open(outputfile,'w')
	of.close()
	of=open(outputfile,'a') #Open it again for appending.

	outputfile_Kepler=outputfile.replace('.txt', '_Kepler.txt')
	of_Kepler=open(outputfile_Kepler,'w')
	of_Kepler.close()
	of_Kepler=open(outputfile_Kepler,'a') #Open it again for appending.

	aalpha0vec=np.unique(abs(std.alpha0)) #Vector of |alpha0| bins.

	t=time_estimate(len(aalpha0vec)) #A class that prints estimated computation time.

	for aai in xrange(len(aalpha0vec)):
		t.display() #Shows the remaining computation time.
		t.increase() #Needed to calculate the remaining computation time.

		sel=(abs(std.alpha0)==aalpha0vec[aai])
	
		mA_sel=std.mA[sel]
		alphaA_sel=std.alphaA_true[sel]
		alpha0_sel=std.alpha0[sel]
		betaA_sel=std.betaA[sel]
		kA_sel=std.kA[sel]

		mB_vec=np.zeros(len(mA_sel))
		chisq_vec=np.zeros(len(mA_sel))

		mB_Kepler_vec=np.zeros(len(mA_sel))
		chisq_Kepler_vec=np.zeros(len(mA_sel))
		
		for mAi in xrange(len(mA_sel)):
			#I will add kinematic and galactic contributions to Pbdot, and their respective errors.
			mB_GR=mB_GR_f(mA_sel[mAi], opd.Pb, opd.ecc, opd.omdot) #The value of mB will be searched for numerically, starting with the GR value.

			args=(mA_sel[mAi], alphaA_sel[mAi], alpha0_sel[mAi], betaA_sel[mAi], std.beta0, kA_sel[mAi], opd.Pb, opd.ecc, opd.Pbdot+opd.Pbdot_add, opd.Pbdot_err+opd.Pbdot_add_err, opd.gamma, opd.gamma_err, opd.omdot, opd.omdot_err)

			sol=fmin(chisq_masses, mB_GR, args=args, disp=False, full_output=True)
			mB_vec[mAi]=sol[0]
			chisq_vec[mAi]=sol[1]

			args=(mA_sel[mAi], alphaA_sel[mAi], alpha0_sel[mAi], betaA_sel[mAi], std.beta0, kA_sel[mAi], opd.x, opd.Pb, opd.ecc, opd.Pbdot+opd.Pbdot_add, opd.Pbdot_err+opd.Pbdot_add_err, opd.gamma, opd.gamma_err, opd.omdot, opd.omdot_err)

			sol=fmin(chisq_masses_Kepler, mB_GR, args=args, disp=False, full_output=True)
			mB_Kepler_vec[mAi]=sol[0]
			chisq_Kepler_vec[mAi]=sol[1]
	
			#Here I must also impose a condition on sin(i). This comes from Kepler's third law (generalised to SCT). The easiest is to impose -1<sin(i)<1, for a given SCT and a given pair of masses. A better approach is to accept only the values of sin(i) for which the Shapiro delay is consistent with the (not) observed one.

		indi=chisq_vec.argmin()
		abc=[std.beta0, alpha0_sel[indi], chisq_vec[indi], mA_sel[indi], mB_vec[indi], alphaA_sel[indi], betaA_sel[indi], kA_sel[indi]]
		abc_str=['%e ' %item for item in abc]

		indi=chisq_Kepler_vec.argmin()
		abc_Kepler=[std.beta0, alpha0_sel[indi], chisq_Kepler_vec[indi], mA_sel[indi], mB_Kepler_vec[indi], alphaA_sel[indi], betaA_sel[indi], kA_sel[indi]]
		abc_Kepler_str=['%e ' %item for item in abc_Kepler]

		of.write(' '.join(abc_str)+'\n')
		of.flush() #So that I can access the file while it is being populated.

		of_Kepler.write(' '.join(abc_Kepler_str)+'\n')
		of_Kepler.flush() #So that I can access the file while it is being populated.
	of.close()
	of_Kepler.close()

def old_chisq_masses_1738(mB, mA, alphaA, alphaB, betaA, betaB, Pb, ecc, pbdot_obs, pbdot_err, mB_obs, mB_err, q_obs, q_err):
	''''''
	pbdot_th=Pbdot_all_f(mA, mB, Pb, ecc, alphaA, alphaB, betaA, betaB)
	q=mA*1./mB
	optical_condi=(mB>=(mB_obs-mB_err))&(mB<=(mB_obs+mB_err))&(q>=(q_obs-q_err))&(q<=(q_obs+q_err))
	optical_term=(not optical_condi)*1e20 #This sould be 0 if the constraints from optical measurements on mB and mass ratio are fulfilled, otherwise infinity.
	return ((pbdot_obs-pbdot_th)*1./pbdot_err)**2.+optical_term

def old_chisq_analysis_1738(std, opd, outputfile):
	''''''
	#Create (or empty) the output file.
	of=open(outputfile,'w')
	of.close()
	of=open(outputfile,'a') #Open it again for appending.

	aalpha0vec=np.unique(abs(std.alpha0)) #Vector of |alpha0| bins.

	t=time_estimate(len(aalpha0vec)) #A class that prints estimated computation time.

	for aai in xrange(len(aalpha0vec)):
		t.display() #Shows the remaining computation time.
		t.increase() #Needed to calculate the remaining computation time.

		sel=(abs(std.alpha0)==aalpha0vec[aai])
	
		mA_sel=std.mA[sel]
		alphaA_sel=std.alphaA_true[sel]
		alpha0_sel=std.alpha0[sel]
		betaA_sel=std.betaA[sel]
		kA_sel=std.kA[sel] #Not necessary for 1738, but kept for completeness.

		mB_vec=np.zeros(len(mA_sel))
		chisq_vec=np.zeros(len(mA_sel))

		for mAi in xrange(len(mA_sel)):
			#I will add kinematic and galactic contributions to Pbdot, and their respective errors.
			#The initial guess for mB will be the optical value.
			mB_initial=opd.mB

			args=(mA_sel[mAi], alphaA_sel[mAi], alpha0_sel[mAi], betaA_sel[mAi], std.beta0, opd.Pb, opd.ecc, opd.Pbdot+opd.Pbdot_add, opd.Pbdot_err+opd.Pbdot_add_err, opd.mB, opd.mB_err, opd.q, opd.q_err)

			sol=fmin(chisq_masses_1738, mB_initial, args=args, disp=False, full_output=True)
			mB_vec[mAi]=sol[0]
			chisq_vec[mAi]=sol[1]

		indi=chisq_vec.argmin()
		abc=[std.beta0, alpha0_sel[indi], chisq_vec[indi], mA_sel[indi], mB_vec[indi], alphaA_sel[indi], betaA_sel[indi], kA_sel[indi]]
		abc_str=['%e ' %item for item in abc]

		of.write(' '.join(abc_str)+'\n')
		of.flush() #So that I can access the file while it is being populated.

	of.close()

def chisq_masses_1738(mB, mA, mbA, alphaA, alphaB, betaA, betaB, Pb, ecc, pbdot_obs, pbdot_err, mB_obs, mB_err, q_obs, q_err):
	''''''
	pbdot_th=Pbdot_all_f(mA, mB, Pb, ecc, alphaA, alphaB, betaA, betaB)
	q=mA*1./mB
	#q=mbA*1./mB #This mass ratio should be between baryonic masses; I assume mbB=mB.
	optical_condi=(mB>=(mB_obs-mB_err))&(mB<=(mB_obs+mB_err))&(q>=(q_obs-q_err))&(q<=(q_obs+q_err))
	optical_term=(not optical_condi)*1e20 #This sould be 0 if the constraints from optical measurements on mB and mass ratio are fulfilled, otherwise infinity.
	return ((pbdot_obs-pbdot_th)*1./pbdot_err)**2.+optical_term

def chisq_analysis_1738(std, opd, outputfile):
	''''''
	#Create (or empty) the output file.
	of=open(outputfile,'w')
	of.close()
	of=open(outputfile,'a') #Open it again for appending.

	aalpha0vec=np.unique(abs(std.alpha0)) #Vector of |alpha0| bins.

	t=time_estimate(len(aalpha0vec)) #A class that prints estimated computation time.

	for aai in xrange(len(aalpha0vec)):
		t.display() #Shows the remaining computation time.
		t.increase() #Needed to calculate the remaining computation time.

		sel=(abs(std.alpha0)==aalpha0vec[aai])
	
		mA_sel=std.mA[sel]
		mbA_sel=std.mbA[sel]
		alphaA_sel=std.alphaA_true[sel]
		alpha0_sel=std.alpha0[sel]
		betaA_sel=std.betaA[sel]
		kA_sel=std.kA[sel] #Not necessary for 1738, but kept for completeness.

		mB_vec=np.zeros(len(mA_sel))
		chisq_vec=np.zeros(len(mA_sel))

		for mAi in xrange(len(mA_sel)):
			#I will add kinematic and galactic contributions to Pbdot, and their respective errors.
			#The initial guess for mB will be the optical value.
			mB_initial=opd.mB

			args=(mA_sel[mAi], mbA_sel[mAi], alphaA_sel[mAi], alpha0_sel[mAi], betaA_sel[mAi], std.beta0, opd.Pb, opd.ecc, opd.Pbdot+opd.Pbdot_add, opd.Pbdot_err+opd.Pbdot_add_err, opd.mB, opd.mB_err, opd.q, opd.q_err)

			sol=fmin(chisq_masses_1738, mB_initial, args=args, disp=False, full_output=True)
			mB_vec[mAi]=sol[0]
			chisq_vec[mAi]=sol[1]

		indi=chisq_vec.argmin()
		abc=[std.beta0, alpha0_sel[indi], chisq_vec[indi], mA_sel[indi], mB_vec[indi], alphaA_sel[indi], betaA_sel[indi], kA_sel[indi], mbA_sel[indi]]
		abc_str=['%e ' %item for item in abc]

		of.write(' '.join(abc_str)+'\n')
		of.flush() #So that I can access the file while it is being populated.

	of.close()

def split_data(std, abs_alpha0_min, abs_alpha0_max):
	''''''
	alpha0=std['alpha0']
	std_split={}
	sel=(abs(alpha0)>=abs_alpha0_min)&(abs(alpha0)<abs_alpha0_max)
	for key in std.keys():
		if type(std[key])==np.ndarray:
			if len(std[key])==len(alpha0):
				std_split[key]=std[key][sel]
		else:
			std_split[key]=std[key]
	return AttrDict(std_split)

def get_pulsar_data(parfilename):
	'''Extract data from a given temp2 par file and export a class of pulsar data.'''
	data={}
	for line in open(parfilename):
		if len(line.split())==0: #Omit empty lines.
			continue
		data['%s' %line.split()[0]]=line.split()[1:]
	PSRJ=str(data['PSRJ'][0])
	x=float(data['A1'][0])*light
	x_err=float(data['A1'][2])*light
	Pb=float(data['PB'][0])*24.*3600.
	Pbdot=float(data['PBDOT'][0])
	Pbdot_err=float(data['PBDOT'][2])
	omdot=float(data['OMDOT'][0])*np.pi/180.*1./yr
	omdot_err=float(data['OMDOT'][2])*np.pi/180.*1./yr
	gamma=float(data['GAMMA'][0])
	gamma_err=float(data['GAMMA'][2])
	ecc=float(data['ECC'][0])
	Pbkin=float(data['PBKIN'][0])
	Pbkin_err=float(data['PBKIN'][2])
	Pbgal=float(data['PBGAL'][0])
	Pbgal_err=float(data['PBGAL'][2])

	Pbdot_add=-Pbgal-Pbkin
	Pbdot_add_err=Pbkin_err+Pbgal_err
	
	odata={'PSRJ':PSRJ, 'Pb':Pb, 'Pbdot':Pbdot, 'Pbdot_err':Pbdot_err, 'Pbdot_add':Pbdot_add, 'Pbdot_add_err':Pbdot_add_err, 'omdot':omdot, 'omdot_err':omdot_err, 'gamma':gamma, 'gamma_err':gamma_err, 'ecc':ecc, 'x':x, 'x_err':x_err}

	if 'M2' in data.keys():
		odata['mB']=float(data['M2'][0])
		odata['mB_err']=float(data['M2'][2])
	if 'Q' in data.keys():
		odata['q']=float(data['Q'][0])
		odata['q_err']=float(data['Q'][2])

	return AttrDict(odata)
