#!/usr/bin/env python -u
import numpy as np
import pylab as py
import os
from scipy import interpolate as ip
from scipy.interpolate import LinearNDInterpolator as LNDI
from time import sleep
import COMMON as CM
from scipy.ndimage import gaussian_filter
import Eq_solver.DEFAULT_PARAMETERS as DEF
from USEFUL import time_estimate

#DESCRIPTION.
#This script finds what are the best initial conditions to cover the interesting output parameter space.
#(A) The central pressure must be in a range that the NS baryonic mass is always within a specific range, for all values of beta0.
#(B) The central scalar field must be in a range such that |alpha0| is always within a specific range, for all values of beta0.

#INPUT PARAMETERS:
run_dir='./OUTPUT/BOUNDARIES_try/'
pbins=30
phibins=30
beta0bins=30
abs_alpha0_max=1. #Maximum allowed |alpha0|.
abs_alpha0_min=1e-4 #Minimum allowed |alpha0|.
mbA_min=0.5 #Minimum allowed baryonic mass.
mbA_max=3.5 #Maximum allowed baryonic mass.
beta0vec=np.linspace(-6.,6., beta0bins)
#beta0vec=np.array([-6.,-5.,-4.,-3.,-2.,-1.,0.,1.,2.,3.,4.,5.,6.]) #All values of beta0 considered.
#beta0vec=np.array([6.])

plotdir='./plots/'
max_stable_pcen=1e35 #Approximate value of pressure (in Pa) where unstable solutions appear.

#Other parameters specific for this script.
mbAbins=20 #Number of points in the vector of baryonic mass.
phi0_bins=50 #Number of bins in phi0 to search for the maximum baryonic mass.
PLOTTING=False #True to show plots as they are obtained. Otherwise they will just be saved.
SAVING=True #True to save plots.

##########################################
#MAIN CALCULATION.
#Create folder for plots.
if SAVING:
	filenum=[]
	for fili in os.listdir(plotdir): filenum.append(int(fili[1:]) if fili[0]=='B' else 0)
	odir=plotdir+'B%i' %(max(filenum)+1)
	os.makedirs(odir)

if not os.path.isdir(run_dir):
	os.makedirs(run_dir)

mbAvec=np.linspace(mbA_min, mbA_max, mbAbins) #Vector of baryonic mass to see lines in the plots.
if PLOTTING:
	py.ion()

t=time_estimate(len(beta0vec)) #A class that prints estimated computation time.

for beta0i in xrange(len(beta0vec)):
	t.display() #Shows the remaining computation time.
	t.increase() #Needed to calculate the remaining computation time.

	beta0=beta0vec[beta0i]
	#print 'Beta0=%.3f' %beta0
	
	#CM.input_par_math_file(beta0=beta0)
	parfile=run_dir+'parfile_%.3i.m' %beta0i
	inputfile=run_dir+'ifile_%.3i.txt' %beta0i #Input file for the Mathematica script.
	outputfile=run_dir+'ofile_%.3i.txt' %beta0i #Output file for the Mathematica script.

	#Parameters and initial conditions.
	pNsurT, rhoNcenT, pNminT, pNmaxT, phiNminT, phiNmaxT=DEF.par_fun(beta0)
	CM.create_par_file(parfile=parfile, beta0=beta0)
	pNcenT_vec=np.logspace(np.log10(pNminT), np.log10(pNmaxT), pbins)
	phiNcenT_vec=np.logspace(np.log10(phiNminT), np.log10(phiNmaxT), phibins)
	P_vec, Phi_vec=np.meshgrid(pNcenT_vec, phiNcenT_vec)
	input=np.vstack((P_vec.flatten(),Phi_vec.flatten())).T
	#Create input file with initial conditions.
	np.savetxt(inputfile, input, delimiter=',', newline='\n', header='pNcenT, phiNcenT')

	#Solve equations.
	CM.eq_solver(parfile, inputfile, outputfile)
	
	#Load solutions.
	raw_data=np.loadtxt(outputfile, dtype='str')
	c0=(raw_data[:,8]!='Indeterminate') #This condition will eliminate "Indeterminate" solutions.
	#Convert data to floats (first get rid of "Indeterminate" rows).
	raw_data[raw_data=='Indeterminate']='999999'
	sols=raw_data.astype(float)
	pcen=sols[:,0]*DEF.sp #Pressure at the centre of the NS in Pa.
	phicen=sols[:,1] #Value of the scalar field at the centre of the NS.
	Rns=sols[:,2] #Radius of the NS in m.
	alphaA=sols[:,3] #Effective coupling (dimensionless).
	phi0=sols[:,4] #Scalar field at infinity (dimensionless).
	mA=sols[:,5] #Mass in solar masses.
	mbA=sols[:,6] #Mass (baryonic) in solar masses.
	IA=sols[:,7] #Moment of inertia in kg m^2.
	alpha0=sols[:,8] #Matter coupling in spatial infinity (dimensionless).

	#Physical conditions.
	c1=(phicen*phi0>=0) #As explained in DamourEspositoFarese1998 (referring to DamourEspositoFarese1996), configurations where that product is negative are energetically disfavored.
	c2=(Rns>0.)&(mA>0.)&(mbA>0.) #NS radius must be positive.
	#c3=(abs(alpha0)<1) #|alpha0| cannot be larger than 1.
	sel=c0&c1&c2

	#Plot of input parameters with stripes of constant baryonic mass.
	py.figure(1)
	py.clf()
	plottitle=(str('%.3f' %beta0)).replace('.','p')
	#py.loglog(pcen, phicen,'.', color='blue', alpha=0.5, label='All')
	py.loglog(pcen[-c0], phicen[-c0], 's', color='red', alpha=0.5, label='Singularities')
	py.loglog(pcen[-c1], phicen[-c1], '^', color='magenta', alpha=0.5, label='Energetically disfavoured')
	py.loglog(pcen[-c2], phicen[-c2], '^', color='yellow', alpha=0.5, label='Negative mass or radius')
	#py.loglog(pcen[-c3], phicen[-c3], '*', color='brown', alpha=0.5, label='|alpha0|>1')
	py.loglog(pcen[sel], phicen[sel], '.', color='green', alpha=1, label='Good solutions')
	for mbAi in xrange(len(mbAvec)):
		mbAsel=(abs(mbA-mbAvec[mbAi])<0.01)
		py.loglog(pcen[mbAsel], phicen[mbAsel], 'o', alpha=0.8)
	py.title('Beta0=%.3f' %beta0)
	py.legend(loc='lower right')
	py.xlabel('Pressure at the centre of the NS/Pa')
	py.ylabel('Scalar field at the centre of the NS')
	if SAVING:
		py.savefig('%s/inputs_%i_beta0_%s.png' %(odir,beta0i,plottitle))

	#Plot of |alpha0| versus mbA.
	py.figure(2)
	py.clf()
	py.plot(mbA, abs(alpha0),'.', color='blue', label='All')
	stable=(pcen[sel]<max_stable_pcen)
	py.plot(mbA[sel][stable], abs(alpha0[sel][stable]),'.', color='green', label='Stable solutions')
	py.plot(mbA[sel][-stable], abs(alpha0[sel][-stable]),'.', color='red', label='Unstable solutions')
	py.hlines(abs_alpha0_max, mbA_min, mbA_max, color='black')
	py.hlines(abs_alpha0_min, mbA_min, mbA_max, color='black')
	py.vlines(mbA_min, abs_alpha0_min, abs_alpha0_max, color='black')
	py.vlines(mbA_max, abs_alpha0_min, abs_alpha0_max, color='black')
	py.yscale('log')
	py.legend(loc='lower right')
	py.xlim(0.,5.)
	py.ylim(1e-5, 10)
	py.xlabel('Baryonic mass / msun')
	py.ylabel('|alpha0|')
	if SAVING:
		py.savefig('%s/absalpha0_vs_mbA_%i_beta0_%s.png' %(odir,beta0i,plottitle))

	#Plot of central pressure versus mbA.
	py.figure(3)
	py.clf()
	py.plot(mbA[sel], pcen[sel],'.', alpha=0.2)
	sel2=(abs(alpha0[sel])>1e-4)&(abs(alpha0[sel])<1)
	phi0_good=phi0[sel][sel2]
	phi0vec=np.logspace(np.log10(min(phi0_good)), np.log10(max(phi0_good)), phi0_bins)
	for phi0i in xrange(len(phi0vec)):
		phi0sel=(abs(phi0-phi0vec[phi0i])<0.01)
		py.plot(mbA[phi0sel], pcen[phi0sel], 'o', alpha=0.2, markersize=4)
		if len(mbA[phi0sel])>0:
			indimax=mbA[phi0sel].argmax()
			py.plot(mbA[phi0sel][indimax], pcen[phi0sel][indimax], '^', markersize=10, color='black')
	py.xlabel('Baryonic mass / msun')
	py.ylabel('Central pressure / Pa')
	py.yscale('log')
	py.xlim(0.,5.)
	if SAVING:
		py.savefig('%s/pcen_vs_mbA_%i_beta0_%s.png' %(odir,beta0i,plottitle))

	#Plot of central pressure versus mass.
	py.figure(4)
	py.clf()
	py.plot(mA[sel], pcen[sel],'.', alpha=0.2)
	for phi0i in xrange(len(phi0vec)):
		phi0sel=(abs(phi0-phi0vec[phi0i])<0.01)
		py.plot(mA[phi0sel], pcen[phi0sel], 'o', alpha=0.2, markersize=4)
		if len(mA[phi0sel])>0:
			indimax=mA[phi0sel].argmax()
			py.plot(mA[phi0sel][indimax], pcen[phi0sel][indimax], '^', markersize=10, color='black')
	py.xlabel('Mass / msun')
	py.ylabel('Central pressure / Pa')
	py.yscale('log')
	py.xlim(0.,5.)
	if SAVING:
		py.savefig('%s/pcen_vs_mA_%i_beta0_%s.png' %(odir,beta0i,plottitle))
	
	#import matplotlib.pyplot as plt
	#from mpl_toolkits.mplot3d import Axes3D
	#fig = plt.figure()
	#ax = fig.add_subplot(111, projection='3d')
	#ax.scatter3D(mbA[sel], np.log10(abs(alpha0[sel])), np.log10(pcen[sel]),color='green')
	#ax.scatter3D(mbA, np.log10(abs(alpha0)), np.log10(pcen), color='blue')
	#ax.set_xlim(0.,5.)

	if PLOTTING:
		raw_input('enter')
