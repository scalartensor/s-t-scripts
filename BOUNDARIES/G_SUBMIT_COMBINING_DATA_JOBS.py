#!/usr/local/python-2.7.2/bin/python -u
import numpy as np
import pylab as py
import os
from scipy import interpolate as ip
from time import sleep
import COMMON as CM
import Eq_solver.DEFAULT_PARAMETERS as DEF
from USEFUL import time_estimate
from RUNS import run_fun

##############################################
#INPUT PARAMETERS:
run_number=6

##############################################
run_dir='/lustre/projects/p002_swin/prosado/scalartensor/data/ANALYSIS/BOUNDARIES/RUN%.3i/RAW/' %int(run_number)
qsub_dir='/home/prosado/projects/scalartensor/scalar-tensor-scripts/ANALYSIS/BOUNDARIES/G_qsub_COMBINING_DATA_RUN%.3i/'  %int(run_number)
outputdir='/lustre/projects/p002_swin/prosado/scalartensor/data/ANALYSIS/BOUNDARIES/RUN%.3i/COMBINED/' %int(run_number)

unused, unused, unused, beta0_min, beta0_max, beta0_bins=run_fun(run_number)

#Create or relpace output directory.
CM.create_or_replace(qsub_dir, cluster=True)
CM.create_or_replace(outputdir, cluster=True)

#Locate files.
#inputfiles=np.sort([file for file in os.listdir(run_dir) if file[0:5]=='ofile'])

#Analyse files.

#for job_i in xrange(len(inputfiles)):
for job_i in xrange(beta0_bins):
	gfilename=qsub_dir+'job_%.3i.gstar' %(job_i+1)

	#Content of the gstar file:
	filetext=['#!/bin/csh \n\
#PBS -q sstar \n\
#PBS -l nodes=1:ppn=1 \n\
#PBS -l pmem=2000mb \n\
#PBS -l walltime=10:00:00 \n\
#PBS -N %sO_job_%s \n\
#PBS -o %sO_output_%s \n\
#PBS -e %sO_error_%s \n\
\n\
echo Deploying job to CPUs ... \n\
cat $PBS_NODEFILE \n\
\n\
python /home/prosado/projects/scalartensor/scalar-tensor-scripts/ANALYSIS/BOUNDARIES/G_COMBINING_DATA.py %s %s %i %i' %(qsub_dir, job_i+1, qsub_dir, job_i+1, qsub_dir, job_i+1, run_dir, outputdir, job_i, run_number)]

	np.savetxt(gfilename, filetext, fmt='%s', newline='\n')

	task='qsub '+gfilename
	print task
	os.system(task)

