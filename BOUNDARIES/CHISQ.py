#!/usr/bin/env python -u
import numpy as np
import pylab as py
import os, sys
import COMMON as CM
from scipy import interpolate as ip
from scipy.interpolate import LinearNDInterpolator as LNDI
from USEFUL import time_estimate

#INPUT PARAMETERS:
stdir=sys.argv[1] #Directory of scalar tensor data.
odir=sys.argv[2] #Output directory.
parfile=sys.argv[3] #Path to pulsar par file.
#stdir='../../../data/ANALYSIS/BOUNDARIES/RUN004/ANALYSIS/'
#odir='../../../data/ANALYSIS/BOUNDARIES/RUN004/CHISQ/'

###########################################

#Create or replace output directory.
CM.create_or_replace(odir)

#Observable pulsar data.
opd=CM.get_pulsar_data(parfile)

#Make list of scalar-tensor data.
all_files=np.sort([file for file in os.listdir(stdir) if file[0:4]!='.DS_'])

np.random.shuffle(all_files)

for fili in xrange(len(all_files)):
	filenum=all_files[fili].split('_')[1].split('.txt')[0]
	print 'File %s (%i / %i).' %(filenum, fili+1, len(all_files))

	#Load scalar tensor data.
	std=CM.getdata(stdir+all_files[fili])

	#Output chi square data to output file.
	outputfile=odir+'chisq_%s.txt' %filenum
	CM.chisq_analysis(std, opd, outputfile)






