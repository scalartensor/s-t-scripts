import numpy as np
from scipy.integrate import odeint
import input_parameters as ip

class scalar_tensor:
	'''Functions that depend on the minispace of considered scalar-tensor theories.'''

        def __init__(s, beta0):
                s.beta0=beta0

        def A(s, phi):
                '''Coupling function (dimensionless).'''
                return np.exp(1./2.*s.beta0*phi*phi)

        def alpha(s, phi):
                '''Linear coupling (dimensionless).'''
                return s.beta0*phi

class equation_of_state:
	'''Polytrope.'''

	def __init__(s, kns, Gamma, kp, kn):
		s.kns=kns
		s.Gamma=Gamma
		s.kp=kp
		s.kn=kn

	def epsN(s, pN):
		'''.'''
		return (pN*1./s.kns)**(1./s.Gamma)+pN*1./(s.kp*(s.Gamma-1.))

	def nN(s, pN):
		'''.'''
		return s.kn*(pN*1./s.kns)**(1./s.Gamma)

#It would be much better to rescale variables in a way that things do not depend on the polytrope parameters!!!

st=scalar_tensor(ip.beta0)
eos=equation_of_state(ip.kns, ip.Gamma, ip.kp, ip.kn)

class field_equations:
	def __init__(s, field_variables):
		s.MN, s.nu, s.phi, s.psiN, s.rhoN, s.MbN, s.om, s.ombN = field_variables

	def dpNdrhoN(s,pN):
		'''Define this function first, because it will be used in all others, to change from a variable rhoN to pN.'''
		return -ip.kp*(eos.epsN(pN)+pN/ip.kp)*((ip.kM/ip.kp)*(s.rhoN**2.*st.A(s.phi)**4.*pN/(s.rhoN-2.*s.MbN))+0.5*s.rhoN*s.psiN**2.+s.MN/(s.rhoN*(s.rhoN-2.*s.MN))+st.alpha(s.phi)*s.psiN)

	def dMNdpN(s, pN):
		dMNdrhoN=ip.kM*s.rhoN**2.*st.A(s.phi)**4.*eos.epsN(pN)+0.5*s.rhoN*(s.rhoN-2.*s.MN)*s.psiN**2. 
		return dMNdrhoN*s.dpNdrhoN(pN)**(-1.)

	def dnudpN(s, pN):
		'''.'''
		dnudrhoN=ip.knu*(s.rhoN**2.*st.A(s.phi)**4.*pN/(s.rhoN-2.*s.MN)+s.rhoN*s.psiN**2.+2.*s.MN/(s.rhoN*(s.rhoN-2.*s.MN)))
		return dnudrhoN*s.dpNdrhoN(pN)**(-1.)

	def dphidpN(s, pN):
		'''.'''
		dphidrhoN=s.psiN
		return dphidrhoN*s.dpNdrhoN(pN)**(-1.)

	def dpsiNdpN(s, pN):
		'''.'''
		dpsiNdrhoN=ip.kM*s.rhoN*st.A(s.phi)**4./(s.rhoN-2.*s.MN)*(st.alpha(s.phi)*(eos.epsN(pN)-3.*pN/ip.kp)+s.rhoN*s.psiN*(eos.epsN(pN)-pN/ip.kp))-2.*(s.rhoN-s.MN)/(s.rhoN*(s.rhoN-2.*s.MN))*s.psiN
		return dpsiNdrhoN*s.dpNdrhoN(pN)**(-1.)
	
	def drhoNdpN(s, pN):
		'''Define this function first, because it will be used in all others, to change from a variable rhoN to pN.'''
		return s.dpNdrhoN(pN)**(-1.)

	def dMbNdpN(s, pN):
		'''.'''
		dMbNdrhoN=eos.nN(pN)*st.A(s.phi)**3.*s.rhoN**2./np.sqrt(1.-2.*s.MN/s.rhoN)
		return dMbNdrhoN*s.dpNdrhoN(pN)**(-1.)

	def domdpN(s, pN):
		'''.'''
		domdrhoN=s.ombN
		return domdrhoN*s.dpNdrhoN(pN)**(-1.)

	def dombNdpN(s, pN):
		'''.'''
		dombNdrhoN=ip.kM*s.rhoN**2./(s.rhoN-2.*s.MN)*st.A(s.phi)**4.*(eos.epsN(pN)+pN/ip.kp)*(s.ombN+4.*s.om/s.rhoN)+(s.psiN**2.*s.rhoN-4./s.rhoN)*s.ombN
		return dombNdrhoN*s.dpNdrhoN(pN)**(-1.)
	
	def all_derivatives(s, pN):
		return [s.dMNdpN(pN), s.dnudpN(pN), s.dphidpN(pN), s.dpsiNdpN(pN), s.drhoNdpN(pN), s.dMbNdpN(pN), s.domdpN(pN), s.dombNdpN(pN)]

class initial_conditions:
	'''Initial conditions for the ODE system.'''

	def __init__(s, pNcen, phiNcen):
		s.pNcen=pNcen
		s.phiNcen=phiNcen

	def rhoN(s):
		'''Define this first for convenience.'''
		return ip.rhoNcenT
	
	def MN(s):
		return 0.

	def nu(s):
		return 0.

	def phi(s):
		return s.phiNcen

	def psiN(s):
		return ip.kM*1./3.*s.rhoN()*st.A(s.phi())**4.*st.alpha(s.phi())*(eos.epsN(s.pNcen)-3.*s.pNcen/ip.kp)

	def MbN(s):
		return 0.

	def om(s):
		return 1.

	def ombN(s):
		return ip.kM*4./5.*s.rhoN()*st.A(s.phi())**4.*(eos.epsN(s.pNcen)+s.pNcen/ip.kp)*s.om()

	def all_variables(s):
		return [s.MN(), s.nu(), s.phi(), s.psiN(), s.rhoN(), s.MbN(), s.om(), s.ombN()]

def ode_system(field_variables, pN):
	'''Adapts the ODE system in order to be used by scipy.integrate.odeint function.'''

	return field_equations(field_variables).all_derivatives(pN)


#Create initial conditions file.
numpoints=20 #Number of points to analyse.
pcen_min=1e33 #Minimum central pressure.
pcen_max=1e34 #Maximum central pressure.
phicen_min=1e-5 #Minimum central scalar field.
phicen_max=1e1 #Maximum central scalar field.
P_vec=10.**(np.random.uniform(np.log10(pcen_min*1./ip.sp), np.log10(pcen_max*1./ip.sp), numpoints))
Phi_vec=10.**(np.random.uniform(np.log10(phicen_min), np.log10(phicen_max), numpoints))

for pi in xrange(len(P_vec)):
	for phi in xrange(len(Phi_vec)):
		pNcen=P_vec[pi]
		phiNcen=Phi_vec[phi]
		print
		print pNcen
		print phiNcen
		ic=initial_conditions(pNcen, phiNcen).all_variables()
		#pN = np.linspace(0, 10, 101)
		#Array of normalised pressure.
		pN_a=np.logspace(ip.pNsurT, pNcen, 1000)
		#sol = odeint(pend, y0, t, args=(b, c))
		sol = odeint(ode_system, ic, pN_a)
		#Get arrays from solutions.
		MN_a, nu_a, phi_a, psiN_a, rhoN_a, MbN_a, om_a, omb_a=sol[:,0], sol[:,1], sol[:,2], sol[:,3], sol[:,4], sol[:,5], sol[:,6], sol[:,7]
		RNS=rhoN_a[0]*ip.sM #Radius of the NS in m.
		nusp=RNS*(psiN_a[0]/ip.sM)**2.+2.*(MN_a[0]*ip.sM)/(RNS*(RNS-2.*(MN_a[0]*ip.sM)))
		alphaA=2.*(psiN_a[0]/ip.sM)/nusp
		Q1=(1.+alphaA**2.)**(1./2.)
		Q2=(1.-2.*(MN_a[0]*ip.sM)/RNS)**(1./2.)
		nush=-2./Q1*np.arctanh(Q1/(1.+2./(RNS*nusp)))
		psi0=(psiN_a[0]/ip.sM)-1./2.*alphaA*nush
		mA=ip.light**2./(2.*ip.grav)*nusp*RNS**2.*Q2*np.exp(1./2.*nush)
		mbA=(MbN_a[0]*ip.sMb)
		JA=ip.light**2./(6.*ip.grav)*(omb_a[0]/ip.sM)*RNS**4.*Q2*np.exp(-0.5*nush)
		Om=om_a[0]-ip.light**4./ip.grav**2.*3.*JA/(4.*mA**3.*(3.-alphaA**2.))*(np.exp(2.*nush)-1.+4.*ip.grav*mA/(RNS*ip.light**2)*np.exp(nush)*(2.*ip.grav*mA/(RNS*ip.light**2)+np.exp(nush/2.)*np.cosh(1./2.*Q1*nush)))
		IA=JA/Om

		print RNS
		print alphaA
		print mA/ip.msun
		print mbA/ip.msun
		print IA
		raw_input('enter')
exit()
import matplotlib.pyplot as plt
plt.ion()
plt.plot(pNvec, sol[:, 0], 'b', label='theta(t)')
plt.plot(pNvec, sol[:, 1], 'g', label='omega(t)')
plt.plot(pNvec, sol[:, 2], 'b', label='omega(t)')
plt.legend(loc='best')
plt.xlabel('t')
plt.grid()
raw_input('enter')
#plt.show()

