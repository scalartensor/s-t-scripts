import numpy as np

grav=6.673e-11 #Newtons gravitational constant (in m^3 kg^{-1} s^{-2}).
light=2.99792458e8 #Speed of light in vacuum (in m/s).
msun=1.9891e30 #Solar mass (in kg).
rsun=6.955e8 #Solar radius (in m).

beta0=-3. #Cuadratic coupling (dimensionless). 

sp=1e36 #Typical maximum pressure at the centre of the NS (in Pa).
sM=1e4 #Typical radius of the NS (in m).
pNsurT=1e-5 #Pressure at the surface of the NS (in theory this should be zero, but that leads to singularities).
pNminT=0.0001 #Minimum pressure at the centre of the NS in rescaled units (so the real value is that multiplied by sp).
pNmaxT=10 #Maximum pressure at the centre of the NS in rescaled units (so the real value is that multiplied by sp).
phiNminT=0.001 #Minimum value of the scalar field at the centre of the NS.
phiNmaxT=20 #Maximum value of the scalar field at the centre of the NS.
rhoNcenT=1e-3 #Radial coordinate at the centre (in theory should be zero).

pbins=40 #Number of bins in values of pressure (log spaced).
phibins=40 #Number of bins in values of scalar field (log spaced).

#Polytrope parameters (from DEF96).
mtb=1.66e-27 #Typical baryon mass (in kg).
nt0=1e44 #Baryon number density (in m^{-3}).
Gamma=2.34 #Dimensionless constant.
kns=0.0195 #Dimensionless constant.

#Derived scaling variables.
seps=(nt0*mtb*light**2.)**(1.-1./Gamma)*sp**(1./Gamma)
kp=seps/sp
kn=seps/(nt0*mtb*light**2.)
kM=4.*np.pi*grav/(light**4.)*sM**2.*seps
knu=8.*np.pi*grav/(light**4.)*sM**2.*sp
sMb=4.*np.pi*mtb*nt0*sM**3.
