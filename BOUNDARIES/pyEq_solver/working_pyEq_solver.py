import numpy as np
from scipy.integrate import odeint
import input_parameters as ip

class scalar_tensor:
	'''Functions that depend on the minispace of considered scalar-tensor theories.'''

        def __init__(s, beta0):
                s.beta0=beta0

        def A(s, phi):
                '''Coupling function (dimensionless).'''
                return np.exp(1./2.*s.beta0*phi*phi)

        def alpha(s, phi):
                '''Linear coupling (dimensionless).'''
                return s.beta0*phi

class equation_of_state:
	'''Polytrope.'''

	def __init__(s, kns, Gamma, kp, kn):
		s.kns=kns
		s.Gamma=Gamma
		s.kp=kp
		s.kn=kn

	def epsN(s, pN):
		'''.'''
		return (pN*1./s.kns)**(1./s.Gamma)+pN*1./(s.kp*(s.Gamma-1.))

	def nN(s, pN):
		'''.'''
		return s.kn*(pN*1./s.kns)**(1./s.Gamma)

#It would be much better to rescale variables in a way that things do not depend on the polytrope parameters!!!

st=scalar_tensor(ip.beta0)
eos=equation_of_state(ip.kns, ip.Gamma, ip.kp, ip.kn)

b=0.25
c=5.

class field_equations:
	def __init__(s, field_variables):
		s.MN, s.nu, s.phi=field_variables
		#s.MN, s.nu, s.phi, s.psiN, s.rhoN, s.MbN, s.om, s.ombN = field_variables

	def dMNdpN(s,pN):
		return s.nu+pN

	def dnudpN(s,pN):
		'''.'''
		return -b*s.nu-c*np.sin(s.MN)

	def dphidpN(s,pN):
		'''.'''
		return 1.

	def dpsiNdpN(s,pN):
		'''.'''
		return 1.

	def drhoNdpN(s,pN):
		'''.'''
		return 1.

	def dMbNdpN(s,pN):
		'''.'''
		return 1.

	def domdpN(s,pN):
		'''.'''
		return 1.

	def dombNdpN(s,pN):
		'''.'''
		return 1.
	
	def all_derivatives(s,pN):
		return [s.dMNdpN(pN), s.dnudpN(pN), s.dphidpN(pN)]

class initial_conditions:
	'''Initial conditions for the ODE system.'''

	def __init__(s, pNcen, phiNcen):
		s.pNcen=pNcen
		s.phiNcen=phiNcen
	
	def MN(s):
		return np.pi - 0.1

	def nu(s):
		return 0.

	def phi(s):
		return 1.

	def all_variables(s):
		return [s.MN(), s.nu(), s.phi()]

def ode_system(field_variables, pN):
	'''Adapts the ODE system in order to be used by scipy.integrate.odeint function.'''

	return field_equations(field_variables).all_derivatives(pN)


pNcen=0.
phiNcen=0.
ic=initial_conditions(pNcen, phiNcen).all_variables()
pN = np.linspace(0, 10, 101)
#sol = odeint(pend, y0, t, args=(b, c))
sol = odeint(ode_system, ic, pN)

import matplotlib.pyplot as plt
plt.ion()
plt.plot(pN, sol[:, 0], 'b', label='theta(t)')
plt.plot(pN, sol[:, 1], 'g', label='omega(t)')
plt.plot(pN, sol[:, 2], 'b', label='omega(t)')
plt.legend(loc='best')
plt.xlabel('t')
plt.grid()
raw_input('enter')
#plt.show()

