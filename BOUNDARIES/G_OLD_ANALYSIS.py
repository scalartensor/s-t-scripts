#!/usr/local/python-2.7.2/bin/python -u
import numpy as np
import pylab as py
import os, sys
import Eq_solver.DEFAULT_PARAMETERS as DEF
from scipy import interpolate as ip
from scipy.interpolate import LinearNDInterpolator as LNDI
from time import sleep
from USEFUL import time_estimate
import COMMON as CM

#INPUT PARAMETERS:
lphi0bins=500 #Number of points in the vector of interpolated values of the scalar field at infinity, log10(phi0), in order to calculate alphaA, betaA, and kA.
mbAbins=500 #Number of points in the vector of baryonic mass.
inputdir=sys.argv[1]
outputdir=sys.argv[2]

#Physical conditions.
mbA_min=0.5 #Minimum value of NS baryonic mass (in msun).
mbA_max=3.5 #Maximum value of NS baryonic mass (in msun).
mA_min=0. #Minimum value of NS mass (in msun).
Rns_min=1000. #Minimum value of NS radius (in m).
abs_alpha0_min=1e-4 #Minimum value of alpha0.

###########################
#Create or relpace output directory.
CM.create_or_replace(outputdir, cluster=True)

##############
#Load data.
inputfiles=np.sort([file for file in os.listdir(inputdir) if file[0:5]=='comb_'])

t=time_estimate(len(inputfiles)) #A class that prints estimated computation time.

for fili in xrange(len(inputfiles)):
	t.display() #Shows the remaining computation time.
	t.increase() #Needed to calculate the remaining computation time.

	ifile=inputdir+inputfiles[fili]
	data=np.load(ifile)[()]

	beta0, sp, sM, mtb, nt0, Gamma, kns, o_data=CM.old_analysis(data, mbA_min=mbA_min, mbA_max=mbA_max, mA_min=mA_min, Rns_min=Rns_min, abs_alpha0_min=abs_alpha0_min, lphi0bins=lphi0bins, mbAbins=mbAbins)

	#Save data.
	ofile=outputdir+'old_%.3i.txt' %fili
	#np.savetxt(ofile, o_data, delimiter=', ', newline='\n', header=' Parameters: \n beta0=%e \n sp=%e \n sM=%e \n mtb=%e \n nt0=%e \n Gamma=%e \n kns=%e \n Columns: \n mA/solar_mass, alpha0, alphaA_true, alphaA_check, betaA, kA, pcen/Pa, phicen.' %(beta0, sp, sM, mtb, nt0, Gamma, kns))
	np.savetxt(ofile, o_data, delimiter=' ', newline='\n', header=' Parameters: \n beta0=%e \n sp=%e \n sM=%e \n mtb=%e \n nt0=%e \n Gamma=%e \n kns=%e \n Columns: \n mA/solar_mass, alpha0, alphaA_true, alphaA_check, betaA, kA, pcen/Pa, phicen.' %(beta0, sp, sM, mtb, nt0, Gamma, kns))


