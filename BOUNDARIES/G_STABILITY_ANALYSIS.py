#!/usr/local/python-2.7.2/bin/python -u
import numpy as np
import pylab as py
import os, sys
import Eq_solver.DEFAULT_PARAMETERS as DEF
from scipy import interpolate as ip
from scipy.interpolate import LinearNDInterpolator as LNDI
from time import sleep
from USEFUL import time_estimate
import COMMON as CM

#INPUT PARAMETERS:
run_number=4 #Number of the current run, from which data will be taken.

inputdir='/lustre/projects/p002_swin/prosado/scalartensor/data/ANALYSIS/BOUNDARIES/RUN004/ANALYSIS/'

###########################
#Create or relpace output directory.
#CM.create_or_replace(outputdir)

##############
#Load data.
inputfiles=np.sort([file for file in os.listdir(inputdir) if file[0:9]=='analysis_'])

py.ion()

t=time_estimate(len(inputfiles)) #A class that prints estimated computation time.

for fili in xrange(len(inputfiles)):
	t.display() #Shows the remaining computation time.
	t.increase() #Needed to calculate the remaining computation time.

	openfile=open(inputdir+inputfiles[fili])
	for line in openfile:
		if line.split('=')[0].split(' ')[2]=='beta0':
			beta0=float(line.split('=')[1])
			break
	openfile.close()

	if beta0<-3: #########################
		continue

	ifile=inputdir+inputfiles[fili]
	data=np.loadtxt(ifile, skiprows=10)

	mA=data[:,0]
	mbA=data[:,8]
	Rns=data[:,9]*1./1000 #Radius of the NS in km.
	pcen=data[:,6]
	aalpha0=abs(data[:,1])

	aalpha0_vec=np.unique(aalpha0)

	for ii in xrange(len(aalpha0_vec)-1):
		sel=(aalpha0>aalpha0_vec[ii])&(aalpha0<=aalpha0_vec[ii+1])

		py.figure(1)
		py.clf()
		#py.semilogy(mA[sel], pcen[sel],'.', color='blue')
		#py.semilogy(mbA[sel], pcen[sel],'.', color='red')
		py.plot(Rns[sel], mA[sel], '.', color='blue')
		py.plot(Rns[sel], mbA[sel], '.', color='red')
		py.title('beta0=%.3f, |alpha0|=%.3e' %(beta0, aalpha0_vec[ii]))
		py.draw()
		
		py.figure(2)
		py.clf()
		py.plot(pcen[sel], mA[sel], '.', color='blue')
		py.plot(pcen[sel], mbA[sel], '.', color='red')
		py.draw()
		py.pause(0.00001)



