#!/usr/bin/env python -u
import numpy as np
import pylab as py
import os
from scipy import interpolate as ip
from time import sleep
import COMMON as CM
import Eq_solver.DEFAULT_PARAMETERS as DEF
from USEFUL import time_estimate

##############################################
#INPUT PARAMETERS:
run_dir='../../../data/ANALYSIS/BOUNDARIES/RUN2/RAW/'

##############################################

#Locate files.
list_vec=os.listdir(run_dir)
parfile_vec,icfile_vec, ofile_vec=[],[],[]
for file in list_vec:
	if file[0:2]=='pa':
		parfile_vec.append(file)
	elif file[0:2]=='ic':
		icfile_vec.append(file)
	elif file[0:2]=='of':
		ofile_vec.append(file)

to_analyse=len(icfile_vec)-len(ofile_vec)

print 'There are %i files to be analysed.' %to_analyse

#Analyse random files.
np.random.shuffle(icfile_vec)

t=time_estimate(to_analyse) #A class that prints estimated computation time.

for file in xrange(len(icfile_vec)):
	t.display() #Shows the remaining computation time.
	t.increase() #Needed to calculate the remaining computation time.

	icfile=icfile_vec[file]
	par_i=icfile.split('.')[0].split('_')[1]
	pcen_i=icfile.split('.')[0].split('_')[2]
	parfile='parfile_%s.m' %par_i
	ofile='ofile_%s_%s.txt' %(par_i, pcen_i)
	if os.path.isfile(ofile):
		continue
	else:
		CM.eq_solver(run_dir+parfile, run_dir+icfile, run_dir+ofile)

