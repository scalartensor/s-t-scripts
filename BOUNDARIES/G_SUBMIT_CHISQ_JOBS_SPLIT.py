#!/usr/local/python-2.7.2/bin/python
import numpy as np
import pylab as py
import os, sys
import COMMON as CM

##############################################
#INPUT PARAMETERS:
run_number=6
split=5 #Number of times that each beta0 file will be split.
abs_alpha0_min=1e-4 #Minimum |alpha0| for all jobs.
abs_alpha0_max=1. #Maximum |alpha0|.

pulsar=sys.argv[1] #Either 1141 or 1738 or 1141_less.

#Files from ANALYSIS_+"idirtag" will be loaded.
#The output files will be called CHISQ_+"odirtag".

if pulsar=='1141':
	idirtag='_1141'
	odirtag='_1141_poster'
	parfile='/home/prosado/projects/scalartensor/scalar-tensor-scripts/ANALYSIS/BOUNDARIES/poster.par'
	qdestination='gstar'
elif pulsar=='1141_invariant':
	idirtag='_1141'
	odirtag='_1141_invariant'
	parfile='/home/prosado/projects/scalartensor/scalar-tensor-scripts/ANALYSIS/BOUNDARIES/invariant.par'
	qdestination='gstar'
elif pulsar=='1738':
	idirtag='_1738'
	odirtag='_1738_FEA12'
	parfile='/home/prosado/projects/scalartensor/scalar-tensor-scripts/ANALYSIS/BOUNDARIES/FEA12.par'
	qdestination='sstar'
elif pulsar=='1141_less':
	idirtag='_1141_less'
	odirtag='_1141_less_poster'
	parfile='/home/prosado/projects/scalartensor/scalar-tensor-scripts/ANALYSIS/BOUNDARIES/poster.par'
	qdestination='sstar'
elif pulsar=='1738_less':
	idirtag='_1738_less'
	odirtag='_1738_less_FEA12'
	parfile='/home/prosado/projects/scalartensor/scalar-tensor-scripts/ANALYSIS/BOUNDARIES/FEA12.par'
elif pulsar=='1141_bhat':
	idirtag='_1141'
	odirtag='_1141_bhat'
	parfile='/home/prosado/projects/scalartensor/scalar-tensor-scripts/ANALYSIS/BOUNDARIES/bhat.par'
	qdestination='gstar'
##############################################
run_dir='/lustre/projects/p002_swin/prosado/scalartensor/data/ANALYSIS/BOUNDARIES/RUN%.3i/ANALYSIS%s/' %(int(run_number), idirtag)
qsub_dir='/home/prosado/projects/scalartensor/scalar-tensor-scripts/ANALYSIS/BOUNDARIES/G_qsub_CHISQ_RUN%.3i%s/'  %(int(run_number), odirtag)
outputdir='/lustre/projects/p002_swin/prosado/scalartensor/data/ANALYSIS/BOUNDARIES/RUN%.3i/CHISQ%s/' %(int(run_number), odirtag)

aalpha0vec=np.logspace(np.log10(abs_alpha0_min), np.log10(abs_alpha0_max), split+1)

#Create or relpace output directory.
CM.create_or_replace(qsub_dir, cluster=True)
CM.create_or_replace(outputdir, cluster=True)

#Locate files.
inputfiles=np.sort([file for file in os.listdir(run_dir) if file[0:9]=='analysis_'])

np.random.shuffle(inputfiles) #To analyse files in a random order.

for file_i in xrange(len(inputfiles)):
	file_indi=int(inputfiles[file_i].split('_')[1].split('.')[0])
	analysis_filename=run_dir+inputfiles[file_i]
	for job_i in xrange(split):
		gfilename=qsub_dir+'job_%.3i_%.2i.gstar' %(file_indi,job_i)
		outputfile=outputdir+'chisq_%.3i_%.2i.txt' %(file_indi,job_i)

		#Content of the gstar file:
		filetext=['#!/bin/csh \n\
#PBS -q %s \n\
#PBS -l nodes=1:ppn=1 \n\
#PBS -l pmem=2000mb \n\
#PBS -l walltime=10:00:00 \n\
#PBS -N %sO_job_%.3i_%.2i \n\
#PBS -o %sO_output_%.3i_%.2i \n\
#PBS -e %sO_error_%.3i_%.2i \n\
	\n\
echo Deploying job to CPUs ... \n\
cat $PBS_NODEFILE \n\
	\n	' %(qdestination, qsub_dir, file_i, job_i, qsub_dir, file_i, job_i, qsub_dir, file_i, job_i)]
	
		filetext2=['python /home/prosado/projects/scalartensor/scalar-tensor-scripts/ANALYSIS/BOUNDARIES/G_CHISQ_SPLIT.py %s %s %s %e %e' %(analysis_filename, outputfile, parfile, aalpha0vec[job_i], aalpha0vec[job_i+1])]

		np.savetxt(gfilename, filetext+filetext2, fmt='%s', newline='\n')
	
		task='qsub '+gfilename
		print task
		os.system(task)
