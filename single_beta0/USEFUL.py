#!/usr/bin/python
import numpy as np
from time import time
import os, sys

def hms(sec):
	'''Takes the amount of seconds and returns a vector (h,m,s) which is the time in hours, minutes and seconds.'''
	seconds=int((sec%3600.)%60.)
	minutes=int((sec%3600.)//60.)
	hours=int(sec//3600.)
	return hours,minutes,seconds

class time_estimate:
	'''To be used to display the elapsed and remaining time of the computation.'''
	
	def __init__(self,iterations):
		self.iter=iterations
		self.maxti=1000
		self.timini=time()
		self.lasttimi=self.timini
		self.diffi=np.zeros(self.maxti)
		self.tim=0
		self.loopi=0
		#self.diffi[self.tim]=self.timi-self.lasttimi
		self.elap=hms(0)
		self.rema=hms(0)

	def increase(self):
		'''Updates the remaining time after one loop.'''
		self.timi=time()
		self.diffi[self.tim]=self.timi-self.lasttimi
		self.lasttimi=self.timi
		self.elap=hms(self.timi-self.timini)
		self.rema=hms(np.mean(self.diffi[self.diffi>0.])*(self.iter-self.loopi))
		self.tim=self.tim+1-max(0,self.tim-self.maxti+2)*self.maxti
		self.loopi+=1

	def display(self):
		'''Displays elapsed and remaining time.'''
		sys.stdout.write(' Elapsed: %3s:%2s:%2s . Remaining: ~ %3s:%2s:%2s .\r' %(self.elap[0],self.elap[1],self.elap[2],self.rema[0],self.rema[1],self.rema[2]))
		if self.loopi==self.iter-1:
			print
