#!/usr/bin/env python
import numpy as np
import pylab as py
import os
from scipy import interpolate as ip
from scipy.interpolate import LinearNDInterpolator as LNDI

#Define constants.
grav=6.673e-11
light=2.99792458e8
msun=1.9891e30
day=24.*3600.
yr=365.*day

#Define some functions useful for this analysis.


#Define some functions.

def Pbdot_f(mma, mmb, Pb, ecc, alphaA, alphaB, betaA, betaB):
	'''mma and mmb are in solar masses.'''
	ma=mma*msun #Mass A in kg.
	mb=mmb*msun #Mass B.
	M=(ma+mb) #Total mass.
	n=2.*np.pi*1./Pb
	XA=ma*1./M
	XB=1.-XA
	nu=XA*XB
	GABterm=(grav*(1.+alphaA*alphaB)*M*n/(light**3))
	monopole1=-3.*np.pi*nu*1./(1.+alphaA*alphaB)*GABterm**(5./3.)*ecc*ecc*(1.+ecc*ecc*1./4.)*1./(1.-ecc*ecc)**(7./2.)
	monopole2=(5./3.*(alphaA+alphaB)-2./3.*(alphaA*XA+alphaB*XB)+(betaA*alphaB+betaB*alphaA)*1./(1.+alphaA*alphaB))**2.
	monopole=monopole1*monopole2

	dipole1=-2.*np.pi*nu*1./(1.+alphaA*alphaB)*GABterm*(1.+ecc*ecc*1./2)/(1.-ecc*ecc)**(5./2.)*(alphaA-alphaB)**2.
	dipole2=-4.*np.pi/(1.+alphaA*alphaB)*nu*GABterm**(5./3.)*1./(1.-ecc*ecc)
	dipole3=8./5.*(1.+31.*ecc*ecc/8.+19.*ecc**4./32.)*(alphaA-alphaB)*(alphaA*XA+alphaB*XB)*(XA-XB)+(1.+3.*ecc*ecc+3.*ecc**4./8.)*(alphaA-alphaB)*(betaB*alphaA*XA-betaA*alphaB*XB)*1./(1.+alphaA*alphaB)
	dipole=dipole1-dipole2*dipole3

	quadrupole_g=-192.*np.pi*nu/(5.*(1.+alphaA*alphaB))*GABterm**(5./3.)*(1.+73.*ecc*ecc/24.+37.*ecc**4./96.)/(1.-ecc*ecc)**(7./2.)

	quadrupole_phi=quadrupole_g*1./6.*((alphaA+alphaB)-alphaA*XA+alphaB*XB)**2.
	return monopole, dipole, quadrupole_phi, quadrupole_g

def gamma_f(mma, mmb, Pb, ecc, alphaA, alphaB, kA):
	''''''
	ma=mma*msun #Mass A in kg.
	mb=mmb*msun #Mass B.
	M=(ma+mb) #Total mass.
	n=2.*np.pi*1./Pb
	XA=ma*1./M
	XB=1.-XA
	nu=XA*XB
	GABterm=(grav*(1.+alphaA*alphaB)*M*n/(light**3))
	return ecc*XB*1./(n*(1.+alphaA*alphaB))*GABterm**(2./3.)*(XB*(1.+alphaA*alphaB)+1.+kA*alphaB)

def omdot_f(mma, mmb, Pb, ecc, alphaA, alphaB, betaA, betaB):
	''''''
	ma=mma*msun #Mass A in kg.
	mb=mmb*msun #Mass B.
	M=(ma+mb) #Total mass.
	n=2.*np.pi*1./Pb
	XA=ma*1./M
	XB=1.-XA
	nu=XA*XB
	GABterm=(grav*(1.+alphaA*alphaB)*M*n/(light**3))
	return 3.*n*1./(1.-ecc*ecc)*GABterm**(2./3.)*((1.-1./3.*alphaA*alphaB)*1./(1.+alphaA*alphaB)-(XA*betaB*alphaA*alphaA+XB*betaA*alphaB*alphaB)*1./(6.*(1.+alphaA*alphaB)**2.))



