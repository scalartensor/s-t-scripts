#!/usr/bin/env python -u
import numpy as np
import pylab as py
import os
import COMMON as CM
from scipy import interpolate as ip
from scipy.interpolate import LinearNDInterpolator as LNDI
from USEFUL import time_estimate

#Input parameters.
ifile='combined_013.txt'
inputdir='../../../data/ANALYSIS/multi_beta0/run1/combined/'
outputdir='../../../data/ANAYLISIS/multi_beta0/run1/npy/'
maxnum=100 #Number of random points to take from every beta0 file.
mpmin, mpmax=0.1, 3. #Minimum and maximum pulsar mass.
mpbins=200 #Number of points.
mcmin, mcmax = 0.1, 3. #Minimum and maximum companion mass.
mcbins=200 #Number of points.
Pbdot_err_factor=10. #Factor by which the error of Pbdot is multiplied.
gamma_err_factor=10. #Same thing for gamma.
omdot_err_factor=1000. #Same thing for omegadot.

#Binary observable parameters, from '../../data/onezero8.par'.
Pb=0.19765096246205615405*CM.day
Pb_err=0.00000000002871417705*CM.day
Pbdot=-3.9011926801913395108e-13
Pbdot_err=4.2168816927995573978e-15
omdot=5.3102830722373918762*np.pi/180.*1./CM.yr
omdot_err=0.00008490666463720403*np.pi/180.*1./CM.yr
gamma=0.00073189513193250703368
gamma_err=0.00000273145999992813
ecc=0.17188259594416682194
ecc_err=0.00000104439455779383

#Load scalar tensor data.
npyfile=ifile[:-3]+'npy'
if not os.path.isfile(inputdir+npyfile): #If python file does not exist, load txt and save python file.
	#Load beta0 (and other parameters if necessary).
	f = open(inputdir+ifile,'r')
	for line in f.xreadlines():
		if line[3:8]=='beta0':
			beta0=eval(line[9:])
			break
	f.close()
	#Load data. Columns: mA/solar_mass, alpha0, alphaA_true, alphaA_check, betaA, kA
	data=np.loadtxt(inputdir+ifile, skiprows=12, delimiter=',')
	mA=data[:,0]
	alpha0=data[:,1]
	alphaA=data[:,2]
	alphaA_check=data[:,3]
	betaA=data[:,4]
	kA=data[:,5]
	dicti={'mA':mA, 'alpha0':alpha0, 'alphaA_true':alphaA, 'alphaA_check':alphaA_check, 'betaA':betaA, 'kA':kA, 'beta0':beta0}
	np.save(ifile[:-4], dicti)
else: #Directly load python file.
	data=np.load(inputdir+npyfile)[()]
	beta0=data['beta0']
	mA=data['mA']
	alpha0=data['alpha0']
	alphaA=data['alphaA_true']
	betaA=data['betaA']
	kA=data['kA']
alphaB=alpha0
betaB=beta0*np.ones(len(alpha0))

#Mass-mass diagram.
mpvec=np.linspace(mpmin, mpmax, mpbins)
mcvec=np.linspace(mcmin, mcmax, mcbins)
MP,MC=np.meshgrid(mpvec, mcvec)

py.ion()

alpha0_consistent=[]

indis=np.arange(len(alphaA))
np.random.shuffle(indis)
indis_sel=indis[0:maxnum]

#t=time_estimate(len(alphaA)) #A class that prints estimated computation time.
t=time_estimate(maxnum) #A class that prints estimated computation time.

#for i in xrange(len(alphaA)):
for i in indis_sel:
	t.display() #Shows the remaining computation time.
	t.increase() #Needed to calculate the remaining computation time.

	alphaA_i=alphaA[i]
	alphaB_i=alphaB[i]
	betaA_i=betaA[i]
	betaB_i=betaB[i]
	kA_i=kA[i]
	
	#print alphaA_i, alphaB_i, betaA_i, betaB_i, kA_i
	monopole, dipole, quadrupole_phi, quadrupole_g=CM.Pbdot_f(MP, MC, Pb, ecc, alphaA_i, alphaB_i, betaA_i, betaB_i)
	Pbdot_mat=monopole+dipole+quadrupole_phi+quadrupole_g
	gamma_mat=CM.gamma_f(MP, MC, Pb, ecc, alphaA_i, alphaB_i, kA_i)
	omdot_mat=CM.omdot_f(MP, MC, Pb, ecc, alphaA_i, alphaB_i, betaA_i, betaB_i)

	#Selections.
	Pbdot_sel=(abs(Pbdot_mat-Pbdot)<Pbdot_err*Pbdot_err_factor)
	gamma_sel=(abs(gamma_mat-gamma)<gamma_err*gamma_err_factor)
	omdot_sel=(abs(omdot_mat-omdot)<omdot_err*omdot_err_factor)
	all_sel=(Pbdot_sel&gamma_sel&omdot_sel)
	overlap=len(all_sel[all_sel])
	if overlap>0:
		#print 'CONSISTENT WITH OBSERVATIONS!'
		alpha0_consistent.append(alphaB_i)
	else:
		#print 'Not consistent!'
		pass
	#print

	#Plot.
	if True:
		numlevels=30
		levels=np.logspace(np.log10(np.amin(-Pbdot_mat)), np.log10(np.amax(-Pbdot_mat)), numlevels)

		Pbdot_yes=np.zeros(np.shape(Pbdot_mat))
		Pbdot_yes[Pbdot_sel]=1
		gamma_yes=np.zeros(np.shape(gamma_mat))
		gamma_yes[gamma_sel]=1
		omdot_yes=np.zeros(np.shape(omdot_mat))
		omdot_yes[omdot_sel]=1

		py.clf()
		py.contourf(MP, MC, Pbdot_yes, cmap='ocean_r', alpha=0.5)
		py.contourf(MP, MC, gamma_yes, cmap='ocean_r', alpha=0.5)
		py.contourf(MP, MC, omdot_yes, cmap='ocean_r', alpha=0.5)
		raw_input('enter')

print np.amax(alpha0_consistent)





