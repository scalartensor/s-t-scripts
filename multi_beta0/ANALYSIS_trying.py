#!/usr/bin/env python
##############
#This script should:
#(1) Load the data produced by mathematica and apply some physical checks on it.
#(2) Export data that can be used by Vivek's mass-mass script.
##############

import numpy as np
import pylab as py
import os
from scipy import interpolate as ip
from scipy.interpolate import LinearNDInterpolator as LNDI
from time import sleep

#INPUT PARAMETERS:
lphi0bins=400 #Number of points in the vector of interpolated values of the scalar field at infinity, log10(phi0), in order to calculate alphaA, betaA, and kA.
mbAbins=400 #Number of points in the vector of baryonic mass.
inputdir='../../../data/ANALYSIS/multi_beta0/run3/'
outputdir='../../../data/ANALYSIS/multi_beta0/run3/combined/'
print 'The output of this script has passed physical conditions (on maximum value of alpha0, certain range of radius and NS masses). But it will need further selections:'
print '(1) Take only physical solutions, i.e. those where mass (mA) decreases for increasing pressure (at the centre of the NS).'
print '(2) The theoretical alphaA and the derived one should coincide within 5 percent.'

###########################
#Define physical conditions.
absalpha0_max=1. #Maximum value of |alpha0|.
Rns_min=1000. #Minimum value of the radius of the NS in m.
Rns_max=20000. #Maximum value of the radius of the NS in m.
mbA_max=4. #Maximum value of the baryonic mass of the NS in msun.
mA_max=4. #Maximum value of the mass of the NS in msun.

##############
#Load data (solutions of the equations by Mathematica).
#Fixed parameters.
#AppendTo[params, {sp 1., sM 1., pNsurT 1., \[Rho]NcenT 1., mtb 1.,  nt0 1., \[CapitalGamma] 1., kns 1., \[Beta]0 1.}]

py.ion()################

param_files=np.sort([ fili for fili in os.listdir(inputdir) if fili[0:10]=='parameters'])

for fili in xrange(len(param_files)):
	print 'File %i / %i .' %(fili+1, len(param_files))
	indi=param_files[fili][-7:-4]
	params=np.loadtxt(inputdir+'parameters_%s.txt' %indi)
	sols=np.loadtxt(inputdir+'solutions_%s.txt' %indi)
	ofile=outputdir+'combined_%s.txt' %indi
	
	sp=params[0] #Typical maximum pressure at the centre of the NS in Pa.
	sM=params[1] #Typical radius of the NS in m.
	psur=params[2]*sp #Pressure at the surface of the NS (in theory should be zero, but that leads to singularities).
	rhocen=params[3]*sM #Radial coordinate at the centre (in theory should be zero).
	mtb=params[4] #Baryonic mass (kg).
	nt0=params[5] #Baryon number density (m^-3).
	Gamma=params[6] #Dimensionless parameter of the polytrope EoS.
	kns=params[7] #Dimensionless constant of the polytrope EoS.
	beta0=params[8] #Dimensionless constant : quadratic coupling of the scalar field.

	#Variable parameters.
	#AppendTo[sols, {pNcenT 1., \[CurlyPhi]NcenT 1., Rns 1., \[Alpha]A 1., \[CurlyPhi]0 1., mA 1./Msun, mbA 1./Msun, IA 1.}]
	pcen=sols[:,0]*sp #Pressure at the centre of the NS in Pa.
	phicen=sols[:,1] #Value of the scalar field at the centre of the NS.
	Rns=sols[:,2] #Radius of the NS in m.
	alphaA=sols[:,3] #Effective coupling (dimensionless).
	phi0=sols[:,4] #Scalar field at infinity (dimensionless).
	mA=sols[:,5] #Mass in solar masses.
	mbA=sols[:,6] #Mass (bar) in solar masses.
	IA=sols[:,7] #Moment of inertia in kg m^2.
	alpha0=sols[:,8]

	############################
	#Take only physical solutions.
	check=(mbA>0)&(mbA<mbA_max)&(mA>0)&(mA<mA_max)&(Rns>Rns_min)&(Rns<Rns_max)&(abs(alpha0)<absalpha0_max)
	pcen=pcen[check]
	phicen=phicen[check]
	Rns=Rns[check]
	alphaA=alphaA[check]
	phi0=phi0[check]
	mA=mA[check]
	mbA=mbA[check]
	IA=IA[check]
	alpha0=alpha0[check]

	#Interpolate all quantities on the log10(phi0)-mb plane.
	lphi0=np.log10(phi0)
	points=np.vstack((lphi0, mbA)).T
	mA_f=ip.LinearNDInterpolator(points, mA)
	alphaA_f=ip.LinearNDInterpolator(points, alphaA)
	IA_f=ip.LinearNDInterpolator(points, IA)
	alpha0_f=ip.LinearNDInterpolator(points, alpha0)
	pcen_f=ip.LinearNDInterpolator(points, pcen)#########

	lphi0vec=np.linspace(min(lphi0), max(lphi0), lphi0bins)
	mbAvec=np.linspace(min(mbA), max(mbA), mbAbins)
	alphaAcalc_mat=np.zeros((lphi0bins-1,mbAbins))
	alphaAtrue_mat=np.zeros(np.shape(alphaAcalc_mat))
	kA_mat=np.zeros(np.shape(alphaAcalc_mat))
	alpha0_mat=np.zeros(np.shape(alphaAcalc_mat))
	mA_mat=np.zeros(np.shape(alphaAcalc_mat))
	pcen_mat=np.zeros(np.shape(alphaAcalc_mat))########

	for mbi in xrange(len(mbAvec)):
		mAvec=mA_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi]) #Vector of mA as a function of phi0 along the stripe of constant mbA.
		mA_mat[:,mbi]=0.5*(mAvec[1:]+mAvec[:-1])

		alphaAcalc_mat[:,mbi]=np.diff(np.log(mAvec))*1./np.diff(10**(lphi0vec))
		alphaAtrue=alphaA_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi])
		alphaAtrue_mat[:,mbi]=0.5*(alphaAtrue[1:]+alphaAtrue[:-1])

		IAvec=IA_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi])
		kA_mat[:,mbi]=-np.diff(np.log(IAvec))*1./np.diff(10**(lphi0vec))

		alpha0true=alpha0_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi])
		alpha0_mat[:,mbi]=0.5*(alpha0true[1:]+alpha0true[:-1])
	
		pcenvec=pcen_f(lphi0vec, np.ones(lphi0bins)*mbAvec[mbi])########
		pcen_mat[:,mbi]=0.5*(pcenvec[1:]+pcenvec[:-1])######
		
		#if (len(abs(alpha0_mat[:,mbi])[np.isnan(alpha0_mat[:,mbi])==False])>0):
		#	print 'beta0=%.2f, mb=%.3f, min abs alpha0=%.5f ' %(beta0, mbAvec[mbi],np.min(abs(alpha0_mat[:,mbi])[np.isnan(alpha0_mat[:,mbi])==False]))
		#	py.figure(1)
		#	py.clf()
		#	selecti=(np.isnan(pcenvec)==False)&(np.isnan(mAvec)==False)
		#	py.plot(pcenvec[selecti], mAvec[selecti])
		#	py.draw()
		#	py.figure(2)
		#	py.clf()
		#	py.plot(pcenvec[selecti], alpha0true[selecti])
		#	py.draw()
		#	sleep(0.1)
		#	#raw_input('enter')

	if beta0>-3:
		#######################Plot pre-interpolation.
		py.clf()
		print 'beta0', beta0
		phi0lim=0.0024 #Limit on phi0 from Solar System experiments (from DEF96).
		sel=(abs(phi0-phi0lim)<0.0001)
		py.plot(mA[sel], abs(alphaA[sel]),'.', color='blue')
		################Plot post-interpolation.
		indi=abs(lphi0vec-np.log10(phi0lim)).argmin()
		mAveci=mA_mat[indi,:]
		#alpha0veci=alpha0_mat[i,:]
		alphaAveci=alphaAtrue_mat[indi,:]
		alphaAcheckveci=alphaAcalc_mat[indi,:]
		selecti=(np.isnan(mAveci)==False)
		py.plot(mAveci[selecti], abs(alphaAveci[selecti]),'.', color='red')
		py.plot(mAveci[selecti], abs(alphaAcheckveci[selecti]),'.', color='black')
		py.xlim(0.5,3.)
		py.ylim(1e-12,0.8)
		py.yscale('log')
		raw_input('enter')

	if False:
		for i in xrange(lphi0bins-1):
			#selecti=(np.isnan(pcen_mat)==False)
			py.clf()
			pcenveci=pcen_mat[i,:]
			mAveci=mA_mat[i,:]
			alpha0veci=alpha0_mat[i,:]
			alphaAveci=alphaAtrue_mat[i,:]
			alphaAcheckveci=alphaAcalc_mat[i,:]
			selecti=(np.isnan(pcenveci)==False)
			#py.plot(pcenveci[selecti], mAveci[selecti])
			#py.plot(pcenveci[selecti], mbAvec[selecti])
			#py.xlim(0, 0.3e36)
			#py.ylim(0.5,3)
			#py.plot(mAveci[selecti], abs(alphaAveci[selecti]))
			#py.plot(mbAvec[selecti], abs(alphaAveci[selecti]))
			py.plot(mbAvec[selecti], -alphaAveci[selecti],'.')
			py.plot(mbAvec[selecti], -alphaAcheckveci[selecti],'.')
			#py.yscale('log')
			py.xlim(0,4)
			py.ylim(0.,1.)
			py.draw()
			sleep(0.01)
			#raw_input('enter')

	#tol=5 #Tolerance: percentage to which the derived and "true" alphaA must coincide.

	alphaAtrue_m=0.5*(alphaAtrue_mat[1:,:]+alphaAtrue_mat[:-1,:])
	alphaAcalc_m=0.5*(alphaAcalc_mat[1:,:]+alphaAcalc_mat[:-1,:])
	lphi0vec_m=0.5*(lphi0vec[1:]+lphi0vec[:-1])
	betaA_mat=(np.diff(alphaAtrue_mat,axis=0).T*1./np.diff(lphi0vec_m)).T
	kA_m=0.5*(kA_mat[1:,:]+kA_mat[:-1,:])
	alpha0_m=0.5*(alpha0_mat[1:,:]+alpha0_mat[:-1,:])
	mA_m=0.5*(mA_mat[1:,:]+mA_mat[:-1,:])
	pcen_m=0.5*(pcen_mat[1:,:]+pcen_mat[:-1,:])############

	#Select points where alphaA calculated ant theoretical agree within 5%.
	#perc_mat=abs( (alphaAtrue_m-alphaAcalc_m)*100./alphaAtrue_m )
	#good_mat=np.zeros(np.shape(perc_mat))
	#perc_mat[np.isnan(perc_mat)]=100.
	#good_mat[perc_mat<tol]=1

	#Output a set of points that do not have nans.
	selecti=(-np.isnan(alphaAtrue_m))&(-np.isnan(betaA_mat))&(-np.isnan(kA_m))
	#o_data=np.vstack((mA_m[selecti], alpha0_m[selecti], alphaAtrue_m[selecti], alphaAcalc_m[selecti], betaA_mat[selecti], kA_m[selecti])).T
	o_data=np.vstack((mA_m[selecti], alpha0_m[selecti], alphaAtrue_m[selecti], alphaAcalc_m[selecti], betaA_mat[selecti], kA_m[selecti], pcen_m[selecti])).T

	continue#########################
	#Save data.
	np.savetxt(ofile, o_data, delimiter=', ', newline='\n', header=' Parameters: \n beta0=%e \n sp=%e \n sM=%e \n psur=%e \n rhocen=%e \n mtb=%e \n nt0=%e \n Gamma=%e \n kns=%e \n Columns: \n mA/solar_mass, alpha0, alphaA_true, alphaA_check, betaA, kA, pcen/Pa.' %(beta0, sp, sM, psur, rhocen, mtb, nt0, Gamma, kns))


