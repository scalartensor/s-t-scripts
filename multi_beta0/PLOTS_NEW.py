#!/usr/bin/env python -u
import numpy as np
import pylab as py
import os,sys
from time import sleep

#Input data.
flight=True #"True" to plot on the flight as points are created. "False" to plot a finalised file.
ploti='ip' #Either 'abc', alpha-beta curve, or 'ip', input parameters.

#Import data.
FEA_file1='../../../data/FEA12_alpha0beta0_1141.txt'
FEA_file2='../../../data/FEA12_alpha0beta0_1738.txt'
ifile='../../../data/ANALYSIS/alpha0beta0_run5.txt'
py.ion()
while True:
	if flight:
		data=np.genfromtxt(ifile, delimiter=',', skip_footer=1)
	else:
		data=np.loadtxt(ifile, delimiter=',')
	if len(data)==0:
		sys.stdout.write('No overlap found yet. \r')
		continue

	FEA1=np.loadtxt(FEA_file1)
	FEA2=np.loadtxt(FEA_file2)
	alpha0=data[:,0]
	beta0=data[:,1]
	perc=data[:,2]
	pcen=data[:,3]
	phicen=data[:,4]

	#Plot.
	selecti=(perc<5) #Select the ones where alphaA and alphaA_check coincide within 5 percent.
	#Find envelope.
	beta0_env=np.unique(beta0[selecti])
	alpha0_env=np.zeros(len(beta0_env))
	pcen_env=np.zeros(len(beta0_env))
	phicen_env=np.zeros(len(beta0_env))

	beta0_env_all=np.unique(beta0)
	alpha0_env_all=np.zeros(len(beta0_env_all))
	for i in xrange(len(beta0_env)):
		selecti2=(beta0[selecti]==beta0_env[i])
		indi_env=abs(alpha0[selecti][selecti2]).argmax()
		alpha0_env[i]=alpha0[selecti][selecti2][indi_env]
		pcen_env[i]=pcen[selecti][selecti2][indi_env]
		phicen_env[i]=phicen[selecti][selecti2][indi_env]
	for i in xrange(len(beta0_env_all)):
		alpha0_env_all[i]=max(abs(alpha0[beta0==beta0_env_all[i]]))

	if ploti=='abc':
		py.clf()
		ymin,ymax=1e-4, 1.
		xmin,xmax=-6., 6.
		py.plot(FEA1[:,0], FEA1[:,1], color='purple', linewidth=4)
		py.plot(FEA2[:,0], FEA2[:,1], color='blue', linewidth=4)
		py.plot(beta0, abs(alpha0), '.', color='red', alpha=0.1)
		py.plot(beta0[selecti], abs(alpha0[selecti]), '.', color='green', alpha=0.1)
		py.plot(beta0_env_all, abs(alpha0_env_all), color='red', linewidth=4)
		py.plot(beta0_env, abs(alpha0_env), color='green', linewidth=4)
		py.axvline(0, ymin, ymax, color='black')
		py.axhline(3.2e-3, xmin, xmax, color='black')
		py.yscale('log')
		py.xlim(xmin, xmax)
		py.ylim(ymin, ymax)
		py.ylabel('|alpha0|')
		py.xlabel('beta0')

		py.draw()
		if flight:
			sleep(5)
		else:
			raw_input('enter')

	elif ploti=='ip':
		py.clf()
		py.plot(pcen, phicen, '.', color='blue')
		py.plot(pcen_env, phicen_env, '.', color='red')
		py.xlim(1e32, 1e36)
		py.ylim(1e-5, 1e1)
		py.xscale('log')
		py.yscale('log')

		py.draw()
		if flight:
			sleep(5)
		else:
			raw_input('enter')
